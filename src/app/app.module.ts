import { BrowserModule } from '@angular/platform-browser';
import { LOCALE_ID, NgModule } from '@angular/core';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';


import { LoginModule } from './login/login.module';
import { APP_ROUTES } from './app.routes';
import { ComponentsModule } from './components/components.module';
import { HttpClientModule, HTTP_INTERCEPTORS} from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { PagesModule } from './pages/pages.module';
import { NgxPaginationModule } from 'ngx-pagination';
import { ChartsModule } from 'ng2-charts';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { SpinnerInterceptor } from './components/ui/interceptorSpinner/spinner.interceptor';
import { NgxSpinnerModule } from "ngx-spinner";

import localeEs from '@angular/common/locales/es-MX';
import { registerLocaleData } from '@angular/common';
registerLocaleData(localeEs, 'es-MX')
//import { NgStepperComponent } from 'angular-ng-stepper';

// import { Generos } from './pipes/generos.pipe.pipe';
// import { GenerosPipe } from './pipes/generos.pipe';


// import { PagoServiciosConComponent } from './components/pago-pacientes/pago-pacientes.component';


@NgModule({
  declarations: [
    AppComponent,
    // Generos.PipePipe,
    // GenerosPipe,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    LoginModule,
    APP_ROUTES,
    ComponentsModule,
    HttpClientModule,
    FormsModule,
    NgxPaginationModule,
    PagesModule,
    ChartsModule,
    BrowserAnimationsModule,
    NgxSpinnerModule
  ],
  exports: [
    NgxSpinnerModule
   //NgStepperComponent
  ],
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA
  ], 
  providers: [{provide: HTTP_INTERCEPTORS, useClass: SpinnerInterceptor, multi: true },
    {provide:LOCALE_ID, useValue:'es-MX'}],
  bootstrap: [AppComponent]
})
export class AppModule { }
