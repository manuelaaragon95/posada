import { gaurdarCotizacion, getDataCarrito } from 'src/app/functions/storage/storage.functions';
import { getCarritoStorage } from '../../../app/functions/storage/pacienteIntegrados';
import Swal from 'sweetalert2';

export default class CarritoPosada {
    public carrito = {
        total: 0,
        items: [],
    };

    constructor() {
        this.obtenerSotorageCarrito();
    }

    public obtenerSotorageCarrito(){

        this.carrito = getCarritoStorage();
  
        if ( this.carrito == null ){
            this.carrito = {
              total: 0,
              items: [],
            };
        }
        return this.carrito;
    }

    public agregarItem(item){
        const pedido = {
            nombreServicio: item.SERVICIO,
            precio:item.PRECIO_PUBLICO,
            cantidad:1,
            idServicio: item._id
        }

        pedido.precio = item.PRECIO_PUBLICO;
        this.cantidad(pedido.idServicio, pedido);
        let carritoString = JSON.stringify( this.carrito );
        gaurdarCotizacion( carritoString );
        this.carrito = getDataCarrito();
        this.alerta();
        return this.carrito;
    }

    public cantidad(id, pedido){
        this.obtenerSotorageCarrito();
        if(this.carrito.items.length == 0){
            this.sumarTotal( pedido.precio);
            this.carrito.items.push( pedido );
        }else{
            const found = this.carrito.items.findIndex(element => element.idServicio == id);
            if(found >= 0){
                 this.restarTotal(this.carrito.items[found].precio, this.carrito.items[found].cantidad);
                this.carrito.items[found].cantidad = this.carrito.items[found].cantidad + 1;
                this.sumarTotal((this.carrito.items[found].precio * this.carrito.items[found].cantidad));  
            }else{
                this.sumarTotal( pedido.precio);
                this.carrito.items.push( pedido );
            }
        }
    }

    public  sumarTotal( precio){
        this.carrito.total = this.carrito.total + precio;
    }

    public  restarTotal(precio, cantidad) {
        this.carrito.total = this.carrito.total - (precio * cantidad);
    }

    public restarUno(precio){
        this.carrito.total -= precio
    }   

    public  eliminar( id ) {
        this.carrito.items.forEach(  (item, index) => {
            if (index  === id ) {
                if(item.cantidad == 1){
                    this.carrito.items.splice( index, 1 )
                    this.restarTotal( item.precio, item.cantidad );
                }else{
                    item.cantidad -= 1;
                    this.restarUno (item.precio);
                }  
            }
        });
        let  carritoString = JSON.stringify( this.carrito  );
        gaurdarCotizacion(  carritoString );
    }

    public alerta(){
        Swal.fire({
            icon: 'success',
            title: '',
            text: 'Se agrego correctamente al carrito',
        })
    }

}