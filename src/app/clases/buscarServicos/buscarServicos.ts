export const filterServicios = (servicios = [], nombre) => {
    const servicosFilter = [];
    servicios.forEach((element:any)=>{
        if(element.idServicio != null){
            let est = element.idServicio.ESTUDIO.toLowerCase();
            if (est.includes(nombre.toLowerCase())) {
                servicosFilter.push(element)
            }  
        }
    })

    servicosFilter.sort(function(a, b){
        if(a.idServicio.ESTUDIO < b.idServicio.ESTUDIO) return -1; if(a.idServicio.ESTUDIO > b.idServicio.ESTUDIO) return 1; return 0;
    })

    return servicosFilter;
}