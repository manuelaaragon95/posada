// Sesison methods 
export const getUserNameSession = () => {
    const usuario = JSON.parse(localStorage.getItem('usuario')).nombre;
    return usuario
};

export const getUserIdSession = () => {
    const usuario = JSON.parse(localStorage.getItem('usuario'))._id;
    return usuario
    
};

// filters searchs

export const filterNameByPaciente = (pacientes = [], name) => {

    //recibe el array de los pacientes
    // recibe el parametro de busqueda

    const pacientesFilter = [];

    pacientes.forEach( (pacientes:any) => {
        
        var  nombrePaciente = ""
        var   apellidoPaterno = ""
        var    apellidoMaterno = ""

        if( pacientes.idPaciente == undefined ) {

            //validacion para los populates de la data
            nombrePaciente = pacientes.nombrePaciente
            apellidoPaterno = pacientes.apellidoPaterno
            apellidoMaterno  = pacientes.apellidoMaterno

        }else {

            // si la data del paciente no trae la data dentro del populate 
             nombrePaciente = pacientes.idPaciente.nombrePaciente
             apellidoPaterno = pacientes.idPaciente.apellidoPaterno
             apellidoMaterno  = pacientes.idPaciente.apellidoMaterno
        }

        nombrePaciente.trim() 
        apellidoPaterno.trim()
        // ponemos el trim de los nombres

        nombrePaciente.toUpperCase(); 
        apellidoPaterno.toUpperCase();

        // concatenamos los apellidos  al nombre
        nombrePaciente += " "+apellidoPaterno         
        nombrePaciente += " " +apellidoMaterno;


        if(nombrePaciente.includes(name.toUpperCase())){
            pacientesFilter.push( pacientes );
            //agregamos al array los pacientes
        }

    });

    //retorna los pacientes encontrados en el array
    return pacientesFilter;
}
