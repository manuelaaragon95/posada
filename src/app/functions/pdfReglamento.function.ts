import jsPDF from "jspdf";
import * as moment from "moment";


export function pdfReglamento() {

    const fecha = moment().format('LLLL');

    const doc:any = new jsPDF({
        unit: "cm",
        format: 'a4',
    });

    const imgPosada = '../../../../assets/images/logoPosada.png'

    doc.setTextColor(0, 0, 0);
    doc.setFontSize(12);
    doc.setFont('helvetica')
    for (let index = 0; index <= 5; index++) {
        doc.text('FICHA DE REGISTRO', 4, 2);
    }
    doc.addImage(imgPosada, 'PNG', 13, 0.7, 6, 2);
    doc.setFontSize(10);
    doc.text(fecha, 12.5, 3.1);
    
    for (let i = 0; i <=5; i++) {
        doc.setFontSize(10);
        doc.text('NOMBRE: Guillermo Urzúa Sánchez', 1, 4);
        doc.text('DOMICILIO: Av Morelos 1, Tlayca Jonacatepec Mor.', 1, 4.6)
        doc.text('FECHA INGRESO: 08/01/2022', 1, 5.2);
        doc.text('IDENTIFICACIÓN: IFE/INE', 1, 5.8);
        doc.text('AUTOMOVIL: Sentra Verde', 1, 6.4);
        doc.text('EMAIL: memo_urzua@hotmail.com', 12, 4);
        doc.text('FECHA DE SALIDA: 09/01/2022', 12, 4.6);
        doc.text('HORA DE INGRESO: 4:58 P.M.', 6.4, 5.2);
        doc.text('FORMA DE PAGO: TARJETA DÉBITO', 12, 5.2);
        doc.text('NO DE IDE: 0', 6.4, 5.8);
        doc.text('TERMINACIÓN: $1,000.00', 12, 5.8);
        doc.text('PLACAS: MCV 89-99', 6.4, 6.4);
        doc.text('HABITACIÓN: #3B - 2MAT HASTA 4 PERS.', 12, 6.4);
        doc.setFontSize(12);
        doc.text('REGLAMENTO', 8.5, 7.2)
    }
    doc.setFontSize(10);
    doc.text('1.- No se permite el ingreso ni consumo de bebidas alcohólicas durante su estancia en la Posada.', 1, 7.7);
    doc.text('2.- Por disposición oficial no se permite fumar dentro de las instalaciones de la Posada.', 1, 8.2);
    doc.text('3.- No se aceptan mascotas.', 1, 8.7);
    doc.text('4.- Por ser de autoservicio nuestro estacionamiento, no nos hacemos responsables de ningun daño a su automóvil.', 1, 9.2);
    doc.text('5.- En caso de extravio de la llave o del control remoto será pagado por el huésped, $60.00 o $100.00 segun sea el caso.', 1, 9.7);
    doc.text('6.- Cualquier daño ocasionado por el mal uso de las instalaciones será con cargo al huésped.', 1, 10.2);
    doc.text('7.- La habitación vence a las 12:00 hrs. Cualquier retraso aplica un cargo extra para el cliente.', 1, 10.7);

    // * Segunda Mita´
    doc.setTextColor(0, 0, 0);
    doc.setFontSize(12);
    doc.setFont('helvetica')
    for (let index = 0; index <= 5; index++) {
        doc.text('FICHA DE REGISTRO', 4, 14);
    }
    doc.addImage(imgPosada, 'PNG', 13.5, 13, 6, 2);
    doc.setFontSize(10);
    doc.text(fecha, 12.5, 15.3);
    
    for (let i = 0; i <=5; i++) {
        doc.setFontSize(10);
        doc.text('NOMBRE: Guillermo Urzúa Sánchez', 1, 16);
        doc.text('DOMICILIO: Av Morelos 1, Tlayca Jonacatepec Mor.', 1, 16.6)
        doc.text('FECHA INGRESO: 08/01/2022', 1, 17.2);
        doc.text('IDENTIFICACIÓN: IFE/INE', 1, 17.8);
        doc.text('AUTOMOVIL: Sentra Verde', 1, 18.4);
        doc.text('EMAIL: memo_urzua@hotmail.com', 12, 16);
        doc.text('FECHA DE SALIDA: 09/01/2022', 12, 16.6);
        doc.text('HORA DE INGRESO: 4:58 P.M.', 6.4, 17.2);
        doc.text('FORMA DE PAGO: TARJETA DÉBITO', 12, 17.2);
        doc.text('NO DE IDE: 0', 6.4, 17.8);
        doc.text('TERMINACIÓN: $1,000.00', 12, 17.8);
        doc.text('PLACAS: MCV 89-99', 6.4, 18.4);
        doc.text('HABITACIÓN: #3B - 2MAT HASTA 4 PERS.', 12, 18.4);
        doc.setFontSize(12);
        doc.text('REGLAMENTO', 8.5, 19.2)
    }
    doc.setFontSize(10);
    doc.text('1.- No se permite el ingreso ni consumo de bebidas alcohólicas durante su estancia en la Posada.', 1, 20.7);
    doc.text('2.- Por disposición oficial no se permite fumar dentro de las instalaciones de la Posada.', 1, 21.2)
    doc.text('3.- No se aceptan mascotas.', 1, 21.7);
    doc.text('4.- Por ser de autoservicio nuestro estacionamiento, no nos hacemos responsables de ningun daño a su automóvil.', 1, 22.2);
    doc.text('5.- En caso de extravio de la llave o del control remoto será pagado por el huésped, $60.00 o $100.00 segun sea el caso.', 1, 22.7);
    doc.text('6.- Cualquier daño ocasionado por el mal uso de las instalaciones será con cargo al huésped.', 1, 23.2);
    doc.text('7.- La habitación vence a las 12:00 hrs. Cualquier retraso aplica un cargo extra para el cliente.', 1, 23.7)
    doc.text('Enterado y conforme: _________________________.', 10, 26.2);









    // doc.save('asdasd.pdf');
    window.open(doc.output('bloburl', '_blank'));



}