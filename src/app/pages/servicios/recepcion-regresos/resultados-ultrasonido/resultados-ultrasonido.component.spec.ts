import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ResultadosUltrasonidoComponent } from './resultados-ultrasonido.component';

describe('ResultadosUltrasonidoComponent', () => {
  let component: ResultadosUltrasonidoComponent;
  let fixture: ComponentFixture<ResultadosUltrasonidoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ResultadosUltrasonidoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ResultadosUltrasonidoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
