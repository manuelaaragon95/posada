import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BitacoraRayosXComponent } from './bitacora-rayos-x.component';

describe('BitacoraRayosXComponent', () => {
  let component: BitacoraRayosXComponent;
  let fixture: ComponentFixture<BitacoraRayosXComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BitacoraRayosXComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BitacoraRayosXComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
