import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import jsPDF from 'jspdf';
import { ConsultaService } from 'src/app/services/consultas/consulta.service';
import { LaboratorioService } from 'src/app/services/consultasLab/laboratorio.service';

@Component({
  selector: 'app-resultados-finales',
  templateUrl: './resultados-finales.component.html',
  styleUrls: ['./resultados-finales.component.css']
})
export class ResultadosFinalesComponent implements OnInit {

  public paciente = {

    apellidoMaterno: '',
    apellidoPaterno: '',
    callePaciente: '',
    consultas:'' ,
    contactoEmergencia1ApellidoMaterno: '',
    contactoEmergencia1ApellidoPaterno: '',
    contactoEmergencia1Curp: "",
    contactoEmergencia1Edad: "",
    contactoEmergencia1Genero: "",
    contactoEmergencia1Nombre: "",
    contactoEmergencia1Telefono: "",
    contactoEmergencia2ApellidoMaterno: "",
    contactoEmergencia2ApellidoPaterno: "",
    contactoEmergencia2Curp: "",
    contactoEmergencia2Edad: "",
    contactoEmergencia2Genero: "",
    contactoEmergencia2Nombre: "",
    contactoEmergencia2Telefono: "",
    correoPaciente: "",
    correoRazonSocial2: "",
    cpPaciente: '',
    cpRazonSocial: "",
    cpRazonSocial2: "",
    curp: "",
    edad: '',
    estadoPaciente: "",
    familia: [],
    fechaNacimientoPaciente: "",
    fechaRegistro: "",
    genero: "",
    membresiaActiva: '',
    nombrePaciente: "",
    nomenclaturaRegistro: "",
    paisNacimiento: "",
    paisPaciente: "",
    paquetes: [],
    paquetesPacientes: [],
    razonSocial1: "",
    razonSocial1Calle: "",
    razonSocial1Estado: "",
    razonSocial1Municipio: "",
    razonSocial2: "",
    razonSocial2Calle: "",
    razonSocial2Estado: "",
    razonSocial2Municipio: "",
    razoncocial1RFC: "",
    razoncocial2RFC: "",
    status: "",
    telefono: '',
    _id: ''
  }
  



  public resultado =[{
    idEstudio:{
      ESTUDIO:"",
      ELEMENTOS:[{
        elementos:"",
        referencia:"",
        tipo:""
      }
      ]
    },
    obtenidos:{},
    idPaciente:[],
    idPedido:{
      _id:''
    },
    quimico:""
  }]
  
  public obtenido=[];
  public tipos = [];
  public resultadoF=[]
  public quimicoRes=[{ quimico:""}];

  public id;
  
  constructor(

    private _laboratorio: LaboratorioService,
    private _route: ActivatedRoute,
    private _consultaService: ConsultaService 

  ) { }

  ngOnInit(): void {

    this.id = this._route.snapshot.paramMap.get('id');
    this.obtenerResultados();

  } 


  obtenerResultados(){

    const idPedido = localStorage.getItem('idPedido');
    // console.log( idPedido );

    this.paciente=JSON.parse(localStorage.getItem('idPaciente'))
    this._laboratorio.obtenerLaboratorioEvolucion(this.paciente._id , idPedido)
      .subscribe(( data ) =>{
        // console.log(data)
        this.setEstudio( data['data'])
      });   
  }

 setEstudio(estudio:any){
  // console.log(estudio);
//seteamos los elementos
 this.resultado = estudio

 this.resultado.forEach( (element) => {

  // console.log(element);
  if (element.idPedido != null  ) {
     if(element.idEstudio.ESTUDIO === localStorage.getItem('nombreEstidio')){
       
        this.resultadoF.push(element);    
     }
  }

  this.resultadoF.reverse();
 });


 this.quimicoRes = this.resultado
// iteramos los resultados
for(let item of this.resultadoF){
  for(let items of item.idEstudio.ELEMENTOS){
      // se hace el push al elemento 0 del arreglo 
    if(this.tipos.length == 0){
        this.tipos.push(items.tipo);  
     }else{
       // busca los elementos dentro del arreglo

       const encontrar= this.tipos.find(element => element == items.tipo)
       if(encontrar){
          this.tipos.push("")
          // se hace un push de elementos vacios
       }else{
         // un push del elemento nuevo
         this.tipos.push(items.tipo)
       }
     }   
  }
}

 for(let item in this.resultadoF[0].obtenidos[0]){
   this.obtenido.push(this.resultadoF[0].obtenidos[0][item])
   
 }
 
 
 }


 printComponent( tablaData ){

  
  const img_header = document.querySelector('#img_content');
  const img_footer = document.querySelector('#img_footer');

  img_header.classList.remove('hide-display');
  img_header.classList.add('show-to-pdf');
  
  // img_footer.classList.remove('hide-display');
  // img_footer.classList.add('show-footer');

  const printComponents = document.getElementById(tablaData).innerHTML;
  //const header_img = document.querySelector('#header-text');

  const originalComponents = document.body.innerHTML;

  // document.body.innerHTML = printComponents;

  
  window.print();
  document.body.innerHTML = originalComponents;
  window.location.reload();

 } 

 imprimirPDFLabs() {
   let imgLabs = '../../../../../assets/images/bs-1-hero.png';
   let fooLabs = '../../../../../assets/images/lineas-bs.png';
    const doc = new jsPDF({
    // orientation: "landscape",
    unit: "cm",
    format: 'a4',
    });

      let pageHeight = doc.internal.pageSize.height;
      console.log('Tamaño del pdf',pageHeight);
      

      doc.addImage(imgLabs, 'PNG', 1, 0.5, 4, 2)
      doc.setTextColor(0, 0, 255);
      doc.setFontSize(10);
      doc.setFont('helvetica')
      // doc.setFontType('bold')
      doc.text('Nombre Completo:', 1, 3)
      doc.text('Juan Patron Guerrero', 4.5, 3)
      doc.text('Edad: ', 11.5, 3);
      doc.text('26', 12.5, 3);
      doc.text('Género: ', 15, 3);
      doc.text('Masculino', 17, 3);
      doc.text('Domicilio:', 1, 3.8)
      doc.setTextColor(100);
      doc.text('EMILIANO ZAPATA MORELOS MEXICO', 3, 3.8);
      doc.setTextColor(0, 0, 255);
      doc.text('Fecha de Nacimiento: ', 14.5, 3.8);
      doc.setTextColor(100);
      doc.text('16-07-1995', 18, 3.8);
      doc.setTextColor(0, 0, 255);
      doc.text('Lugar de Nacimiento:', 1, 4.6)
      doc.setTextColor(100);
      doc.text('EMILIANO ZAPATA MORELOS MEXICO', 4.5, 4.6);
      doc.setTextColor(0, 0, 255);
      doc.text('CURP:', 13, 4.6)
      doc.setTextColor(100);
      doc.text('PAGJHMSTRON04MSTR01', 14.5, 4.6);
      doc.setTextColor(0, 0, 255);
      doc.text('Teléfono:', 1, 5.4);
      doc.setTextColor(100);
      doc.text('7351181377', 2.8, 5.4);
      doc.setTextColor(0, 0, 255);
      doc.text('ID:', 13, 5.4)
      doc.setTextColor(100);
      doc.text('5f7f4b4b3974c43ba0fb21a9', 13.8, 5.4)

      /*  AQUI VA EL PUERCO DE LA HOJA XDDDDD */
      doc.setFontSize(11);
      doc.setTextColor(0);
      doc.text('Tipo: ', 1, 7);
      doc.text('Elementos: ', 4, 7);
      doc.text('Valores Obtenidos: ', 9, 7);
      doc.text('Valores de Referencia: ', 15, 7);

      let coordX = 10;
      let coordY;

      for (let index = 0; index < coordX; index++) {
        console.log("asdasda", coordX);
        
        
      }



      doc.addImage(fooLabs, 'PNG', 1, 28.5, 19.5, 0.5)
      // doc.text("otra hojaaa", 1, 28.5);
      // doc.addPage();
      
      // doc.insertPage;
      let y = 30;
      doc.text("otra hojaaaaaa", 1, 35);
      if (y>=pageHeight)
        {
        doc.addPage();
        doc.text("otra hojaaaaaa", 1, 3);
        doc.text("otra dsfsdfsd", 1, 3.5);

        doc.addImage(fooLabs, 'PNG', 1, 28.5, 19.5, 0.5)

        }

      doc.save("LabsTestttttt xD");
      return;
 }

 otraPage() {
  const doc = new jsPDF({
       // orientation: "landscape",
       unit: "cm",
       format: 'a4',
      });

  if (this.imprimirPDFLabs >= doc.internal.pageSize.height.valueOf) {
    doc.addPage();
  }

  //  let imgLabs = '../../../../../assets/images/bs-1-hero.png';
  //  let fooLabs = '../../../../../assets/images/lineas-bs.png';
  //  const doc = new jsPDF({
  //    // orientation: "landscape",
  //    unit: "cm",
  //    format: 'a4',
  //   });
    
  //   let pageHeight = doc.internal.pageSize.height;
    

  //    doc.addImage(imgLabs, 'PNG', 1, 0.5, 4, 2)
  //    doc.setTextColor(0, 0, 255);
  //    doc.setFontSize(10);
  //    doc.setFont('helvetica')
  //    // doc.setFontType('bold')
  //    doc.text('Nombre Completo:', 1, 3)
  //    doc.text('Juan Patron Guerrero', 4.5, 3)
  //    doc.text('Edad: ', 11.5, 3);
  //    doc.text('26', 12.5, 3);
 }
}
