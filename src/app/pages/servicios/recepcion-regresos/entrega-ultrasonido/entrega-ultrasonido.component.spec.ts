import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EntregaUltrasonidoComponent } from './entrega-ultrasonido.component';

describe('EntregaUltrasonidoComponent', () => {
  let component: EntregaUltrasonidoComponent;
  let fixture: ComponentFixture<EntregaUltrasonidoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EntregaUltrasonidoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EntregaUltrasonidoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
