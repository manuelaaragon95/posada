import { Component, OnInit } from '@angular/core';
import { ConsultaService } from 'src/app/services/consultas/consulta.service';
import { PacientesService } from 'src/app/services/pacientes/pacientes.service';
import jsPDF from 'jspdf';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-regresos-lab',
  templateUrl: './regresos-lab.component.html',
  styleUrls: ['./regresos-lab.component.css']
})
export class RegresosLabComponent implements OnInit {

  public totalpaciente:string;
  public pagina = 0;
  public consultas:any=[];
  noSe: any;
  public servicio='';

  constructor(public _consultaService: ConsultaService,
              public _consultaPaciente: PacientesService,
              private activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.servicio = this.activatedRoute.snapshot.paramMap.get('servicio');
    this.obtenerCosnultaLab();
  }

  obtenerCosnultaLab(){
    this._consultaService.verListaLaboratorio(this.servicio).subscribe((resp:any)=>{
      console.log(resp);
      
    })
    /* this._consultaService.verListaLaboratorio().subscribe( (data) =>   {
        this.consultas = data['data'].reverse();
        this.totalpaciente = data['data'].results;
    }); */
  }

  imp(){
    let values: any;
    values = this.noSe.map((elemento) => Object.values(elemento));
    const doc:any = new jsPDF();
    doc.text(12, 9, "BITÁCORA DE RAYOS X");
    doc.autoTable({
      head: [['#', 'Fecha', 'Nombre', 'Edad', 'Sexo', 'Sede', 'Estudio']],
      body: values
    })
    doc.save('Bitácora_De_Rayos_X.pdf')
  }
 
}
