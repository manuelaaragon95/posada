import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RegresosLabComponent } from './regresos-lab.component';

describe('RegresosLabComponent', () => {
  let component: RegresosLabComponent;
  let fixture: ComponentFixture<RegresosLabComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RegresosLabComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RegresosLabComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
