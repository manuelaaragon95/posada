import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RegresosComponent } from './regresos.component';

describe('RegresosComponent', () => {
  let component: RegresosComponent;
  let fixture: ComponentFixture<RegresosComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RegresosComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RegresosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
