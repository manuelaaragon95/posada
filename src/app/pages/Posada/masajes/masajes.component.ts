import { Component, OnInit } from '@angular/core';
import { PosadaServiceService } from './../../../services/posada-service/posada-service.service';

@Component({
  selector: 'app-masajes',
  templateUrl: './masajes.component.html',
  styleUrls: ['./masajes.component.css']
})
export class MasajesComponent implements OnInit {
  public masajes:any [] = [] 

  constructor(private _masajes:PosadaServiceService) { }

  ngOnInit(): void {
    this.obtenerMasajes()
  }
  obtenerMasajes(){
    this._masajes.getHospedaje('Masajes').subscribe((data:any)=>{
      this.masajes = data.data
    })
  }
}
