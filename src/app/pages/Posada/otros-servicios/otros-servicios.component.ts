import { Component, OnInit } from '@angular/core';
import { PosadaServiceService } from './../../../services/posada-service/posada-service.service';

@Component({
  selector: 'app-otros-servicios',
  templateUrl: './otros-servicios.component.html',
  styleUrls: ['./otros-servicios.component.css']
})
export class OtrosServiciosComponent implements OnInit {

  public servicios:any [] = []  

  constructor(private _otroservicios:PosadaServiceService) { }

  ngOnInit(): void {
    this.obtenerOtrosServicios();
  }
  obtenerOtrosServicios(){
    this._otroservicios.getHospedaje('Otros servicios').subscribe((data:any)=>{
      this.servicios = data.data
    })
  }
}
