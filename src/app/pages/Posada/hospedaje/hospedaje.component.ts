import { Component, OnInit } from '@angular/core';
import { PosadaServiceService } from './../../../services/posada-service/posada-service.service';

@Component({
  selector: 'app-hospedaje',
  templateUrl: './hospedaje.component.html',
  styleUrls: ['./hospedaje.component.css']
})
export class HospedajeComponent implements OnInit {

  public habitaciones:any [] = [] 
  public totalhabitaciones:string;

  constructor(private _habitaciones:PosadaServiceService) { }

  ngOnInit(): void {
    this.obtenerHabitaciones();
  }

  obtenerHabitaciones(){
    this._habitaciones.getHospedaje('Habitaciones').subscribe((data:any)=>{
      this.habitaciones = data.data
    })
    //this.habitaciones = this._habitaciones.getHospedaje();
    /* this.totalhabitaciones = this._habitaciones.getHospedaje().results; */
  }

}
