import { Component, OnInit } from '@angular/core';
import { NgxSpinnerService } from 'ngx-spinner';
import { PosadaService } from './../../../services/posada/posada.service';
import { pdfReglamento } from 'src/app/functions/pdfReglamento.function';


@Component({
  selector: 'app-huespedes',
  templateUrl: './huespedes.component.html',
  styleUrls: ['./huespedes.component.css']
})
export class HuespedesComponent implements OnInit {

  public huesped:any [] = [];
  public totalhuspedes:string;
  public pagina = 0;

  constructor(private _huespedes:PosadaService,
              private spinner: NgxSpinnerService) { }

  ngOnInit(): void {
    this.spinner.show();
    this.huespedes();
    // this.imprimir();
  }

  huespedes(){
    this._huespedes.obtenerHuespedes().subscribe((resp:any)=>{
      if (resp.ok) {
        this.huesped = resp['data'].reverse();
        this.totalhuspedes = resp['data'].results;
        this.spinner.hide();
      }
    })
  }

  imprimir() {
    pdfReglamento();
  }

}
