import { Component, OnDestroy, OnInit } from '@angular/core';
import { Observable, Subject, Subscription } from 'rxjs';
import Pacientes from 'src/app/interfaces/pacientes.interface';
import { PacientesService  } from '../../../services/pacientes/pacientes.service';
import Swal from 'sweetalert2';
import { SpinnerService } from 'src/app/services/spinner/spinner.service';
import { NgxSpinnerService } from "ngx-spinner";
 
/* import { SpinnerService } from 'src/app/services/spinner/spinner.service'; */

@Component({
  selector: 'app-pacientes',
  templateUrl: './pacientes.component.html',
  styleUrls: ['./pacientes.component.css']
})
export class PacientesComponent implements OnInit, OnDestroy {

  public pacientes2:any[] = [];
  public totalpaciente:string;
  public pagina = 0;
  public  filtropacientes = '';
  public filterPost = '';

  private unsubcribe$ = new Subject<void>();

  constructor(public _pacienteService: PacientesService,
              private _spinnerService:SpinnerService,
              private spinner: NgxSpinnerService) {}

  ngOnInit(): void {
    this.obtenerPacientes();
    this.focusSearchPatients();
    this.spinner.show();
  }

  obtenerPacientes() {
    this._pacienteService.getPacientesAll()
    .subscribe( (data: any) => {
      if( data['message']   === 'No hay pacientes' ) {
         Swal.fire({
              icon: 'success',
              title: '',
              text: 'NO SE ENCUENTRA NINGUN PACIENTE',
            })
        return;
      }
      this.pacientes2 = data.data.reverse();
      this.totalpaciente = data.data.results;
      this.spinner.hide();
    });
  }

  typeSearchPatients(){
    if( this.filtropacientes.length > 3 ){

      this._pacienteService.getPacientePorNombre( this.filtropacientes )
      .subscribe(  data => {
        this.pacientes2 = data['data'];
      });
    }else if(this.filtropacientes == ''){
      this.obtenerPacientes();
      this.focusSearchPatients();
    }
  }

  focusSearchPatients(){
    const input: HTMLInputElement = document.querySelector('#busquedaPaciente');
    input.select();
  }

  paginaAnterior(){
    this.pagina = this.pagina - 8;
    this._pacienteService.getPacientes( this.pagina).subscribe( (data: any) => {
      this.pacientes2 = data.users;
    })
  }

  siguentePagina(){
    this.pagina += 8;
    this._pacienteService.getPacientes( this.pagina).subscribe( (data: any) => {
      this.pacientes2= data.users;
    })
  }

  ngOnDestroy(){
    this.unsubcribe$.next();
    this.unsubcribe$.complete();
  }
}
