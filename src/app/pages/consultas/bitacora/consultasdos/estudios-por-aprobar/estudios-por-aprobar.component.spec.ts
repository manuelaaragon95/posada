import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EstudiosPorAprobarComponent } from './estudios-por-aprobar.component';

describe('EstudiosPorAprobarComponent', () => {
  let component: EstudiosPorAprobarComponent;
  let fixture: ComponentFixture<EstudiosPorAprobarComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EstudiosPorAprobarComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EstudiosPorAprobarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
