import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ConsultasdosComponent } from './consultasdos.component';

describe('ConsultasdosComponent', () => {
  let component: ConsultasdosComponent;
  let fixture: ComponentFixture<ConsultasdosComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ConsultasdosComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ConsultasdosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
