import { TestBed } from '@angular/core/testing';

import { PosadaService } from './posada.service';

describe('PosadaService', () => {
  let service: PosadaService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PosadaService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
