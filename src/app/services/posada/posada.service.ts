import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders} from '@angular/common/http';
import { Observable, Observer } from 'rxjs';
import { URL } from 'src/app/config/conf';

@Injectable({
  providedIn: 'root'
})
export class PosadaService {

  public url: string;

  constructor(private _http:HttpClient) { 
    this.url = URL;
  }

  registrarHuesped(body){
    return this._http.post( this.url +'/huesped', body );
  }

  obtenerHuespedes(){
    return this._http.get( this.url +'/obtener/huesped');
  }

}
