import { TestBed } from '@angular/core/testing';

import { PosadaServiceService } from './posada-service.service';

describe('PosadaServiceService', () => {
  let service: PosadaServiceService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PosadaServiceService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
