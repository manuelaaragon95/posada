import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders} from '@angular/common/http';
import { Observable, Observer } from 'rxjs';
import { URL } from 'src/app/config/conf';

@Injectable({
  providedIn: 'root'
})
export class PosadaServiceService {

  public habitaciones:any [] = [
    {
      SERVICIO:'HABITACION FAMILIAR 1',
      precio:1000,
      _id:'ashdgadshhagdjasgdhgahjd'
    },
    {
      SERVICIO:'HABITACION FAMILIAR 2',
      precio:1000,
      _id:'laklskdlkasdklakdlaksldka'
    },
    {
      SERVICIO:'HABITACION FAMILIAR 3',
      precio:1000,
      _id:'qweqweqweqweqweqweqweqweqw'
    },
    {
      SERVICIO:'HABITACION FAMILIAR 6',
      precio:1000,
      _id:'utututyutyututuyutyutyutyu'
    },
    {
      SERVICIO:'HABITACION FAMILIAR 7',
      precio:1000,
      _id:'zxczxczxczxczxczxczxczxczx'
    }
  ]; 
  url: string;

  constructor(private _http:HttpClient){

    this.url = URL;
  }

  getHospedaje(servicio){
    return this._http.get(this.url +'/ver/servicios/posada/'+servicio);
  }

}
