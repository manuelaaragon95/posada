import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ServicosPosadaComponent } from './servicos-posada.component';

describe('ServicosPosadaComponent', () => {
  let component: ServicosPosadaComponent;
  let fixture: ComponentFixture<ServicosPosadaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ServicosPosadaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ServicosPosadaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
