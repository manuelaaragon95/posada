import { Component, Input, OnInit } from '@angular/core';
import  CarritoPosada  from "../../clases/carrito/carrito-posada";

@Component({
  selector: 'app-servicos-posada',
  templateUrl: './servicos-posada.component.html',
  styleUrls: ['./servicos-posada.component.css']
})
export class ServicosPosadaComponent implements OnInit {

  @Input() servicio = ''
  @Input() serviciosPosada:any[]=[];
  @Input() totalpaciente:string;
  public pagina = 0;
  public carrito = {
    total: 0,
    items: []
  };

  constructor() { }

  ngOnInit(): void {
    this.obtenerCarritoStorage();
  }

  agregarCarrito(item:any){
    let carrito = new CarritoPosada();
    this.carrito = carrito.agregarItem( item );
    this.validarCarrito();
  }

  eliminar(index){
    let carrito = new CarritoPosada();
    carrito.eliminar( index );
    this.obtenerCarritoStorage();
    this.validarCarrito();
  }

  validarCarrito(){
    this.obtenerCarritoStorage();
    if(this.carrito.items.length == 0){
      return true;
    }else{
      return false;
    }
  }

  obtenerCarritoStorage(){
    const storageCarrito = new CarritoPosada();
    this.carrito = storageCarrito.obtenerSotorageCarrito();
  }

}
