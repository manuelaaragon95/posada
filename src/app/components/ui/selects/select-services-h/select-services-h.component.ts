import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-select-services-h',
  templateUrl: './select-services-h.component.html',
  styleUrls: ['./select-services-h.component.css']
})
export class SelectServicesHComponent implements OnInit {

  constructor(
    public _router: Router
  ) { }

  ngOnInit(): void {
  }
  irAUnServicio(  servicio ){
    this.setRouteService( servicio );
  }

  setRouteService(servicio){
    this._router.navigate([ `/serviciosInt/${servicio}`]);
  }
}
