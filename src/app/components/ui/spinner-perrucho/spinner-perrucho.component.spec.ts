import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SpinnerPerruchoComponent } from './spinner-perrucho.component';

describe('SpinnerPerruchoComponent', () => {
  let component: SpinnerPerruchoComponent;
  let fixture: ComponentFixture<SpinnerPerruchoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SpinnerPerruchoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SpinnerPerruchoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
