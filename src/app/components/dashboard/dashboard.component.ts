import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
    this.eliminarPaciente();
  }

  eliminarPaciente(){
    localStorage.removeItem('sedePaciente')
    /* localStorage.removeItem('carrito') */
    localStorage.removeItem('idPedidoSede')
    localStorage.removeItem('idPedidoXray')
    localStorage.removeItem('idPedidoUltra')
    localStorage.removeItem('pedidoUsg')
    localStorage.removeItem('ultrasonido')
    localStorage.removeItem('fechaUsg')
    localStorage.removeItem('fechaXray')
    localStorage.removeItem('xray')
    localStorage.removeItem('paciente');
    localStorage.removeItem('nombreEstidio');
    localStorage.removeItem('idPedido');
    localStorage.removeItem('estudio')
    localStorage.removeItem('paciente');
    /* localStorage.removeItem('carrito'); */
    localStorage.removeItem('idPaciente')
  }

}
