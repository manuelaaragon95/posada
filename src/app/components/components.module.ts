import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DashboardComponent } from './dashboard/dashboard.component';
import { CardComponent } from './card/card.component';
import { BrowserModule } from '@angular/platform-browser';
import { ChatComponent } from './chat/chat/chat.component';
import { NavBarComponent } from './nav-bar/nav-bar/nav-bar.component';
import { FormsModule, ReactiveFormsModule} from '@angular/forms';
import { RouterModule } from '@angular/router';
import { AmbulanciaComponent } from './servicios/serv/ambulancia/ambulancia.component';
import { NgxPaginationModule } from 'ngx-pagination';

import { TablaServiceComponent } from './servicios/serv/tabla-service/tabla-service.component';
import { TodosServComponent } from './servicios/serv/todos-serv/todos-serv.component';
import { PagoServiciosComponent } from './servicios/pago-servicios/pago-servicios.component';
import { FichaIdentificacionComponent } from './ficha-identificacion/ficha-identificacion.component';
// import { HistoriaClinicaComponent } from './historiaClinica/historia-clinica/historia-clinica.component';
import { ChartsModule } from 'ng2-charts';
import { HistoriaClinicaComponent } from './historiaClinica/historia-clinica/historia-clinica.component';
import { GraficasNinosComponent } from './graficas-ninos/graficas-ninos.component';
import { GraficasPediatriaComponent } from './graficas-pediatria/graficas-pediatria.component';
import { HeaderRecetaComponent } from './header-receta/header-receta.component';
import { FichaInfoUserComponent } from './ficha-identificacion/ficha-info-user/ficha-info-user.component';
import { StepperComponent } from './registro/stepper/stepper.component';
import { MatStepperModule, MatStepper } from '@angular/material/stepper';
import { TablasConsultasComponent } from './tablas/tablas-consultas/tablas-consultas.component';
import { TitulosComponent } from './ficha-identificacion/titulos/titulos.component';
import { PaquetesComponent } from './paquetes/paquetes.component';
import { PagosPaquetesComponent } from './paquetes/pagos-paquetes/pagos-paquetes.component';
import { PrenatalComponent } from './paquetes/paciente-paquete/prenatal/prenatal.component';
import { NeonatalComponent } from './paquetes/paciente-paquete/neonatal/neonatal.component';
import { PediatricoComponent } from './paquetes/paciente-paquete/pediatrico/pediatrico.component';
import { MembresiaComponent } from './paquetes/paciente-paquete/membresia/membresia.component';
import { MedicoLaboralComponent } from './paquetes/paciente-paquete/medico-laboral/medico-laboral.component';
import { PrenatalRiesgoComponent } from './paquetes/paciente-paquete/prenatal-riesgo/prenatal-riesgo.component';
import { VidaPlenaComponent } from './paquetes/paciente-paquete/vida-plena/vida-plena.component';

//HISOTRIA CLINICA
import { SignosVitalesComponent } from './signos-vitales/signos-vitales.component';
import { HistoriaClinicaHorizonteComponent } from './historia-clinica-horizonte/historia-clinica-horizonte/historia-clinica-horizonte.component';
import { AntecedentesHorizonteComponent } from './historia-clinica-horizonte/antecedentes-horizonte/antecedentes-horizonte.component';
import { SignosVitalesHorizonteComponent } from './historia-clinica-horizonte/signos-vitales-horizonte/signos-vitales-horizonte.component';
import { GinecoPorVisitaComponent } from './historia-clinica-horizonte/gineco-por-visita/gineco-por-visita.component';
import { MedicinaPrevComponent } from './historia-clinica-horizonte/medicina-prev/medicina-prev.component';
import { EsquemaVacunacionComponent } from './historia-clinica-horizonte/esquema-vacunacion/esquema-vacunacion.component';
import { NutricionComponent } from './historia-clinica-horizonte/nutricion/nutricion.component';
import { AntecedentesHFComponent } from './historia-clinica-horizonte/antecedentes-hf/antecedentes-hf.component';
import { AntecedentesPNPComponent } from './historia-clinica-horizonte/antecedentes-pnp/antecedentes-pnp.component';
import { AntecedentesPpComponent } from './historia-clinica-horizonte/antecedentes-pp/antecedentes-pp.component';
import { InterrogacionPAYSComponent } from './historia-clinica-horizonte/interrogacion-pays/interrogacion-pays.component';
import { AntecedentesGinecoObsComponent } from './historia-clinica-horizonte/antecedentes-gineco-obs/antecedentes-gineco-obs.component';
import { RegistroSedesComponent } from './registro-sedes/registro-sedes.component';
import { SpinnerPerruchoComponent } from './ui/spinner-perrucho/spinner-perrucho.component';
import { NgxSpinnerModule } from "ngx-spinner";
import { ServicosPosadaComponent } from './servicos-posada/servicos-posada.component';
import { RegistroPosadaComponent } from './registro-posada/registro-posada.component';
// import { PagosServiciosComponent } from './pago-pacientes/pagos-servicios/pagos-servicios.component';

@NgModule({
  declarations: [
    DashboardComponent,
     CardComponent,
     ChatComponent,
     NavBarComponent,
     AmbulanciaComponent,
     TablaServiceComponent,
     TodosServComponent,
     PagoServiciosComponent,
     FichaIdentificacionComponent,
     HistoriaClinicaComponent,
     GraficasNinosComponent,
     GraficasPediatriaComponent,
     HeaderRecetaComponent,
     FichaInfoUserComponent,
     StepperComponent,
     TablasConsultasComponent,
     TitulosComponent,
     PaquetesComponent,
     PagosPaquetesComponent,
     PrenatalComponent,
     NeonatalComponent,
     PediatricoComponent,
     MembresiaComponent,
     MedicoLaboralComponent,
     PrenatalRiesgoComponent,
     VidaPlenaComponent,
     SignosVitalesComponent,
      HistoriaClinicaHorizonteComponent,
      AntecedentesHorizonteComponent,
      SignosVitalesHorizonteComponent,
      GinecoPorVisitaComponent,
      MedicinaPrevComponent,
      EsquemaVacunacionComponent,
      NutricionComponent,
      AntecedentesHFComponent,
      AntecedentesPNPComponent,
      AntecedentesPpComponent,
      InterrogacionPAYSComponent,
      AntecedentesGinecoObsComponent,
      RegistroSedesComponent,
      SpinnerPerruchoComponent,
      ServicosPosadaComponent,
      RegistroPosadaComponent
    //  PagosServiciosComponent
  ],
  imports: [
    CommonModule,
    BrowserModule,
    RouterModule,
    FormsModule,
    ReactiveFormsModule,
    ChartsModule,
    MatStepperModule,
    NgxPaginationModule,
    NgxSpinnerModule
  ],
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA
   ],
  exports:[
    CardComponent,
    DashboardComponent,
    ChatComponent,
    NavBarComponent,
    AmbulanciaComponent,
    TodosServComponent,
    PagoServiciosComponent,
    FichaIdentificacionComponent,
    HistoriaClinicaComponent,
    GraficasNinosComponent,
    GraficasPediatriaComponent,
    HeaderRecetaComponent,
    FichaInfoUserComponent,
    StepperComponent,
    TablasConsultasComponent,
    TitulosComponent,
    PaquetesComponent,
    PagosPaquetesComponent,
    PrenatalComponent,
    NeonatalComponent,
    PediatricoComponent,
    MembresiaComponent,
    MedicoLaboralComponent,
    PrenatalRiesgoComponent,
    VidaPlenaComponent,
    SignosVitalesComponent,
    HistoriaClinicaHorizonteComponent,
    AntecedentesHorizonteComponent,
    SignosVitalesHorizonteComponent,
    GinecoPorVisitaComponent,
    MedicinaPrevComponent,
    EsquemaVacunacionComponent,
    NutricionComponent,
    AntecedentesHFComponent,
    AntecedentesPNPComponent,
    AntecedentesPpComponent,
    InterrogacionPAYSComponent,
    AntecedentesGinecoObsComponent,
    RegistroSedesComponent,
    ServicosPosadaComponent,
    RegistroPosadaComponent
    // MatStepperModule
  ]
})
export class ComponentsModule { }
