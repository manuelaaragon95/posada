import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { PaquetesService } from '../../../../services/paquetes/paquetes.service';
import * as moment from 'moment';
import Swal from 'sweetalert2'
import { PagosService } from '../../../../services/pagos/pagos.service';
import Dates from '../../../../clases/dates/dates';
import { getDataStorage } from 'src/app/functions/storage/storage.functions';
/* import { CEDE } from 'src/app/classes/cedes/cedes.class'; */
import PacienteStorage from 'src/app/clases/pacientes/pacientes.class';
import { eliminarStorage } from 'src/app/functions/storage/pacienteIntegrados';
import Tickets from '../../../../clases/tickets/tickets.class';

@Component({
  selector: 'app-prenatal-riesgo',
  templateUrl: './prenatal-riesgo.component.html',
  styleUrls: ['./prenatal-riesgo.component.css']
})
export class PrenatalRiesgoComponent implements OnInit {

  @Input() id: String;
  consultas:any = { tipo: '', consulta: '', fecha: '', medico: '', firma: '' }
  item:any ={ nombreEstudio:'', precioCon:0, precioSin:0, _id:''}
  concepto:any[] = []
  medicos:any[] = []
  paquete:any[] = []
  citas:any[] = []
  examenes:any[] = []
  public contadorestudios=0;
  public contadorcitas=0;
  public egoCount = 0;
  public ego2Count = 0;
  public ult1Count = 0;
  public ult2Count = 0;
  public ult3Count = 0;
  public ult4Count = 0;
  public ult5Count = 0;
  public lab1Count = 0;
  public lab2Count = 0;
  public lab3Count = 0;
  public lab4Count = 0;
  public lab5Count = 0;
  public lab6Count = 0;
  public lab7Count = 0;
  public lab8Count = 0;
  public lab9Count = 0;
  public lab10Count = 0;
  public lab11Count = 0;
  public lab12Count = 0;
  public lab13Count = 0;
  public lab14Count = 0;
    public hospitalizacion = {fecha:"", hora:"" , consulta:"", medico: "", firma:"" };
    public consultaDespuesParto = {fecha:"", hora:"" , consulta:"", medico: "", firma:"" };
    // public paqueteExamenesInto = [];
    public fechaConsulta = moment().format('l');
    public horaIngreso = moment().format('hh:mm');
    public btnAgregaConsultas = false;
    public pacienteInfo={
      nombrePaciente: "",
      apellidoPaterno: "",
      apellidoMaterno: "",
      curp: "",
      edad: 0,
      genero: "",
      id:"",
      callePaciente: "",
      fechaNacimientoPaciente:"",
      estadoPaciente: "",
      paisPaciente: "",
      telefono: "",
      _id:""
    };
    public infoVenta = {  

      paciente:"",
      nombrePaciente:"",
      vendedor:"",
      fecha:"",
      hora:"",
      estudios:[],
      efectivo:false,
      doctorQueSolicito:"",
      transferencia: false,
      tarjetCredito:false,
      tarjetaDebito:false,
      montoEfectivo:0,
      montoTarjteCredito:0,
      montoTarjetaDebito:0,
      montoTranferencia: 0,
      sede:"",
      totalCompra:0,
      prioridad:"",
      compraConMembresia:true,
      status:'Pagado'
    }

    public pedidosLaboratorios = { 
      estudios:[],
      idPaciente:"",
      fecha:"",
      hora:"",
      medicoQueSolicita:"",
      sede:"",
      prioridad:"Programado",
      estado:""
    }

    public pedidosUltrasonido = {
      idPaciente:"", 
      estudios:[],
      fecha:"",
      hora:"",
      medicoQueSolicita:"",
      sede:"",
      prioridad:"Programado"
    }
  
    public carrito = {
      totalSin: 0,
      totalCon: 0,
      items: []
    };
  fecha: string;

  public diasRestantes:Number;
  public fechaRegistro: string;
  public nombrePaquete='';
  public sede='';

  constructor(public _router: ActivatedRoute, 
              public _paquete:PaquetesService, 
              public _route:Router, 
              private _pagoServicios: PagosService) {
                this.obtenerSede();
              }

  ngOnInit(): void {
    this.obtenerMedicos();
    this.obtenerPaquete();
    this.consultas.firma = JSON.parse(localStorage.getItem('usuario')).nombre;
  }

  obtenerSede(){
    this.sede = localStorage.getItem('cede')
  }

  obtenerPaquete(){
    this._paquete.obtenerPaquete(this.id)
    .subscribe(  (data:any) =>  {
      this.pacienteInfo = data['paquetes'][0]['paciente'];
      this.paquete.push(data['paquetes']);
      this.citas = this.paquete[0].visitas
      this.examenes = this.paquete[0].examenesLab;
      this.fechaRegistro = this.paquete[0].fecha;
      this.nombrePaquete = this.paquete[0].paquete.nombrePaquete;
      this.verCosnultashechas();
      this.getDiasRestantes();
    });
  }

  getDiasRestantes(){

    const dias = new Dates()
    this.diasRestantes  =  dias.getDateRest( this.fechaRegistro ); 
    /* console.log( this.diasRestantes ); */ 

    if( this.diasRestantes <= 0 || this.diasRestantes == NaN ){
      
      let estado = {
        membresiaActiva: false
      }
      this._paquete.actualizarEstadoMembresia( this.pacienteInfo._id, estado )
      .subscribe( data => {/* console.log(data) */});

    }
  }

  obtenerMedicos(){
    this._paquete.getMedicos()
    .subscribe( (data) => {
      this.medicos = data['medicos']
    });
  }

  irAUnServicio(  servicio ){
    this.setRouteService( servicio );
  }

  setRouteService(servicio){
    this._route.navigate([ `/serviciosInt/${servicio}`]);
  }

  verCosnultashechas(){
    /* console.log(this.citas); */
    this.egoCount= 0;
    this.ego2Count= 0;
    this.citas.forEach(element => {
      // console.log( element );
      if( element.consulta == "7 Consultas programadas con el Especialista" ){
        // console.log("Se lo lee");
        this.egoCount += 1;
        let cardEspecialista = document.querySelector('#cardEspecialista');
        let badgeEspecialista = document.querySelector('#badgeEspecialista');
        this.addClassesCss( cardEspecialista, badgeEspecialista);
        //this.hospitalizacion = element ;
      } 
      if( element.consulta == '3 consultas de Nutrición'){
        this.ego2Count += 1;
        let cardNutricion = document.querySelector('#cardNutricion');
        let badgeNutricion = document.querySelector('#badgeNutricion');
        this.addClassesCss( cardNutricion, badgeNutricion);
        //this.consultaDespuesParto = element;
      }
    });
    // termian el codigo que valida las consultas 
    this.verEstudios();
  }

  verEstudios(){
    this.ult1Count = 0;
    this.ult2Count = 0;
    this.ult3Count = 0;
    this.ult4Count = 0;
    this.ult5Count = 0;
    this.lab1Count = 0;
    this.lab2Count = 0;
    this.lab3Count = 0;
    this.lab4Count = 0;
    this.lab5Count = 0;
    this.lab6Count = 0;
    this.lab7Count = 0;
    this.lab8Count = 0;
    this.lab9Count = 0;
    this.lab10Count = 0;
    this.lab11Count = 0;
    this.lab12Count = 0;
    this.lab13Count = 0;
    this.lab14Count = 0;
    // badgeQuimica
   //   // quimicaSanguineaCard
   /* console.log(this.examenes); */
   
    this.examenes.forEach( (examen:consultas) => {
      if( examen.consulta === "1 higado y Via Biliar"  ){
        this.ult1Count += 1;
        let cardHigado = document.querySelector('#HigadoCard');
        let badgeHigado = document.querySelector("#badgeHigado");
        this.addClassesCss( cardHigado, badgeHigado );
      }
      if(examen.consulta === "4 Obstétricos"){
        this.ult2Count += 1;
        let cardObstetricos = document.querySelector('#obstetricosCard');
        let badgeObstetricos = document.querySelector("#badegeObstetricos");
        this.addClassesCss( cardObstetricos, badgeObstetricos );
      }
      if(examen.consulta === "1 Genetico (11-13.6 SDG)"){
        this.ult3Count += 1;
        let cardGenetico = document.querySelector('#GeneticoCard');
        let badgeGenetico = document.querySelector('#badgeGenetico');
        this.addClassesCss( cardGenetico, badgeGenetico);
      }
      if( examen.consulta === "1 Estructural (  18- 24 SDG)"){
        this.ult4Count += 1;
        let cardEstructural = document.querySelector("#EstructuralCard");
        let badgeEstructural = document.querySelector("#badgeEstructural");
        this.addClassesCss(cardEstructural, badgeEstructural)
      }
      if( examen.consulta === "1 Perfil biofisico (Ultimo trimestre)"){
        this.ult5Count += 1;
        let cardBiofisico = document.querySelector("#BiofisicoCard");
        let badgeBiofisico = document.querySelector("#badgeBiofisico");
        this.addClassesCss(cardBiofisico, badgeBiofisico)
      }
      //labs
      if( examen.consulta === "1 Biometría Hemática Completa "){
        this.lab1Count += 1;
        let cardBHC = document.querySelector("#BHCCard");
        let badgeBHC = document.querySelector("#badgeBHC");
        this.addClassesCss(cardBHC, badgeBHC)
      }
      if( examen.consulta === "1 QS 6 (Glucosa, Urea, Creatinina, Ácido Úrico, Colesterol y Triglicéridos)"){
        this.lab2Count += 1;
        let cardQS = document.querySelector("#QSCard");
        let badgeQS = document.querySelector("#badgeQS");
        this.addClassesCss(cardQS, badgeQS)
      }
      if( examen.consulta === "3 Exámenes Generales de Orina"){
        this.lab3Count += 1;
        let cardEGO = document.querySelector("#EGOCard");
        let badgeEGO = document.querySelector("#badgeEGO");
        this.addClassesCss(cardEGO, badgeEGO)
      }
      if( examen.consulta === "1 V.D.R.L."){
        this.lab4Count += 1;
        let cardVDRL = document.querySelector("#VDRLCard");
        let badgeVDRL = document.querySelector("#badgeVDRL");
        this.addClassesCss(cardVDRL, badgeVDRL)
      }
      if( examen.consulta === "1 V.I.H."){
        this.lab5Count += 1;
        let cardVIH = document.querySelector("#VIHCard");
        let badgeVIH = document.querySelector("#badgeVIH");
        this.addClassesCss(cardVIH, badgeVIH)
      }
      if( examen.consulta === "2 Grupos Sanguíneos (Ambos padres)"){
        this.lab6Count += 1;
        let cardGSA = document.querySelector("#GSACard");
        let badgeGSA = document.querySelector("#badgeGSA");
        this.addClassesCss(cardGSA, badgeGSA)
      }
      if( examen.consulta === "1 Tiempos de Coagulación"){
        this.lab7Count += 1;
        let cardTC = document.querySelector("#TCCard");
        let badgeTC = document.querySelector("#badgeTC");
        this.addClassesCss(cardTC, badgeTC)
      }
      if( examen.consulta === "1 Curva de tolerancia a la glucosa"){
        this.lab8Count += 1;
        let cardCTG = document.querySelector("#CTGCard");
        let badgeCTG = document.querySelector("#badgeCTG");
        this.addClassesCss(cardCTG, badgeCTG)
      }
      if( examen.consulta === "1 Pruebas de funcionamiento hepático"){
        this.lab9Count += 1;
        let cardPFH = document.querySelector("#PFHCard");
        let badgePFH = document.querySelector("#badgePFH");
        this.addClassesCss(cardPFH, badgePFH)
      }
      if( examen.consulta === "1 Depuración de proteínas en orina de 24 horas"){
        this.lab10Count += 1;
        let cardDPO = document.querySelector("#DPOCard");
        let badgeDPO = document.querySelector("#badgeDPO");
        this.addClassesCss(cardDPO, badgeDPO)
      }
      if( examen.consulta === "1 Triple "){
        this.lab11Count += 1;
        let cardTriple = document.querySelector("#TripleCard");
        let badgeTriple = document.querySelector("#badgeTriple");
        this.addClassesCss(cardTriple, badgeTriple)
      }
      if( examen.consulta === "1 Perfil tiroideo"){
        this.lab12Count += 1;
        let cardPT = document.querySelector("#PTCard");
        let badgePT = document.querySelector("#badgePT");
        this.addClassesCss(cardPT, badgePT)
      }
      if( examen.consulta === "1 Urocultivo"){
        this.lab13Count += 1;
        let cardUrocultivo = document.querySelector("#UrocultivoCard");
        let badgeUrocultivo = document.querySelector("#badgeUrocultivo");
        this.addClassesCss(cardUrocultivo, badgeUrocultivo)
      }
      if( examen.consulta === "1 Exudado vaginal"){
        this.lab14Count += 1;
        let cardEV = document.querySelector("#EVCard");
        let badgeEV = document.querySelector("#badgeEV");
        this.addClassesCss(cardEV, badgeEV)
      }
    });
   //   //  termina el codigo que valida los examenes 
  }

  showMessage(examen: string){
    this.examenes.forEach(  (estudio:consultas, index) => {
      // console.log( estudio.consulta  === examen  );
      // console.log( estudio  );
        if( estudio.consulta ===  examen  ){
          // console.log(estudio.consulta, index);
           Swal.fire({
              icon: 'success',
              title: '',
              text: `Tipo:  ${this.examenes[index].consulta}\n  Fecha: ${this.examenes[index].fecha} \n Hora: ${this.examenes[index].hora}\n   Médico ${this.examenes[index].medico}\n  Atendió: ${this.examenes[index].firma}`,
            })
        }else{
          this.citas.forEach((cita:consultas, index)=>{
            if(cita.consulta === examen){
               Swal.fire({
              icon: 'success',
              title: '',
              text: `Tipo:  ${this.citas[index].consulta}\n  Fecha: ${this.citas[index].fecha} \n Hora: ${this.citas[index].hora}\n   Médico ${this.citas[index].medico}\n  Atendió: ${this.citas[index].firma}`,
            })
            }
          })
        }
    });
  }

  addClassesCss( card: Element, badge:Element ){
    card.classList.add('border-primary');
    //citas
    if( badge.id == "badgeEspecialista"){
      badge.innerHTML = this.egoCount.toString(); 
    }else if(badge.id == 'badgeNutricion'){
      badge.innerHTML = this.ego2Count.toString();
    }else if( badge.id == "badgeHigado"){
      badge.innerHTML = this.ult1Count.toString(); 
    }else if(badge.id == 'badegeObstetricos'){
      badge.innerHTML = this.ult2Count.toString();
    }else if(badge.id == 'badgeGenetico'){
      badge.innerHTML = this.ult3Count.toString();
    }else if(badge.id == 'badgeEstructural'){
      badge.innerHTML = this.ult4Count.toString();
    }else if(badge.id == 'badgeBiofisico'){
      badge.innerHTML = this.ult5Count.toString();
    }else if(badge.id == 'badgeBHC'){
      badge.innerHTML = this.lab1Count.toString();
    }else if(badge.id == 'badgeQS'){
      badge.innerHTML = this.lab2Count.toString();
    }else if(badge.id == 'badgeEGO'){
      badge.innerHTML = this.lab3Count.toString();
    }else if(badge.id == 'badgeVDRL'){
      badge.innerHTML = this.lab4Count.toString();
    }else if(badge.id == 'badgeVIH'){
      badge.innerHTML = this.lab5Count.toString();
    }else if(badge.id == 'badgeGSA'){
      badge.innerHTML = this.lab6Count.toString();
    }else if(badge.id == 'badgeTC'){
      badge.innerHTML = this.lab7Count.toString();
    }else if(badge.id == 'badgeCTG'){
      badge.innerHTML = this.lab8Count.toString();
    }else if(badge.id == 'badgePFH'){
      badge.innerHTML = this.lab9Count.toString();
    }else if(badge.id == 'badgeDPO'){
      badge.innerHTML = this.lab10Count.toString();
    }else if(badge.id == 'badgeTriple'){
      badge.innerHTML = this.lab11Count.toString();
    }else if(badge.id == 'badgePT'){
      badge.innerHTML = this.lab12Count.toString();
    }else if(badge.id == 'badgeUrocultivo'){
      badge.innerHTML = this.lab13Count.toString();
    }else if(badge.id == 'badgeEV'){
      badge.innerHTML = this.lab14Count.toString();
    }
    badge.classList.add('badge-primary');
  }

  // Nueva programacion de insercion
  seleccion($event, value){
    switch (value) {
      case 'visitas':
        this.concepto=[]
        for(let item of this.paquete ){
          for(let item2 of item.paquete.CitasIncluidas){
            this.concepto.push(item2)
          }
          /* console.log(this.concepto) */
        }  
        this.consultas.consulta = ''
      break;

      case 'examenesLab':
        this.concepto=[]
        for(let item of this.paquete ){
          for(let item2 of item.paquete.examenesLaboratorio){
            this.concepto.push(item2)
          }
          /* console.log(this.concepto) */
        }  
        this.consultas.consulta = ''
      break;
      
      case 'examenesUlt':
        this.concepto=[]
        for(let item of this.paquete ){
          for(let item2 of item.paquete.ultrasonidos){
            this.concepto.push(item2)
          }
          /* console.log(this.concepto) */
        }
        this.consultas.consulta = ''
        
      break;

      default:
        break;
    }
  }

  agregarConsulta(){
    this.consultas.fecha = this.fechaConsulta;
    this.consultas.hora = this.horaIngreso;
    /* console.log(this.consultas) */
    this.consultas.firma = JSON.parse(localStorage.getItem('usuario')).nombre;

    if(this.consultas.tipo == '' || this.consultas.consulta == '' || this.consultas.medico == '' || this.consultas.firma == ''){
       Swal.fire({
              icon: 'error',
              title: '',
              text: 'LOS DATOS SON REQUERIDOS',
            })
    }else{
      if(this.consultas.tipo == 'visitas'){
        let val = true;
        val = this.validacioncitas();
        if(val){
          this._paquete.agregarConsulta(this.consultas,this.consultas.tipo,this.id).subscribe(
            (data)=>{
             this.mostrarConsultas();
              this.setDatesVenta(this.consultas.medico);
              this._pagoServicios.agregarPedido( this.infoVenta )
              .subscribe( (data) => {
                // console.log( data );
                if(  data['ok'] ){
                  
                  this.generarTicket(data['folio']);
                    // se crea la tabla de las ventas 
                    Swal.fire({
                      icon: 'success',
                      title: '',
                      text: 'LA CONSULTA SE AGREGO EXITOSAMENTE',
                    })
                    // ESTA INFORMCAIÓN SE ENVIA A LOS DIFERENTES SERVICIOS 
                    // seteamos las fechas 
                    eliminarStorage();
                    
                    
                    const eliminarPaciente = new PacienteStorage();
                    eliminarPaciente.removePacienteStorage();
                    this.consultas = { tipo: '', consulta: '', fecha: '', medico: '', firma:'' }
                    this._route.navigateByUrl('consulta');   
                  }
                });
              })
        }else{
                    Swal.fire({
                      icon: 'error',
                      title: '',
                      text: 'LAS CONSULTAS DEL PAQUETE SE TERMINARON',
                    })
          this.consultas = { tipo: '', consulta: '', fecha: '', medico: '', firma:'' }
        }
      }else{
        let val = true;
        val = this.validaciones();
        if(val){
          this.consultas.tipo = 'examenesLab'
          this._paquete.agregarConsulta(this.consultas,this.consultas.tipo,this.id).subscribe(
            (data)=>{
              /* console.log(data); */
              
              this.setDatesVenta(this.consultas.medico); 
              this.pagar_consulta(this.consultas.consulta, this.consultas.medico);         
              this.consultas = { tipo: '', consulta: '', fecha: '', medico: '', firma:'' }
            })
        }else{
                              Swal.fire({
                      icon: 'success',
                      title: '',
                      text: 'LOS LABORATORIOS DEL PAQUETE SE TERMINARON',
                    })
          this.consultas = { tipo: '', consulta: '', fecha: '', medico: '', firma:'' }
        }
      }
    }
  }

  validaciones(){
    this.contadorestudios=0
    switch(this.consultas.consulta){      
      case '1 Biometría Hemática Completa ':
        for (const iterator of this.examenes) {
          if(iterator.consulta == '1 Biometría Hemática Completa '){
            this.contadorestudios+= 1
          }
        }
        if(this.contadorestudios < 1){
          return true;
        }else{
          return false;
        }
        break;
      case '1 QS 6 (Glucosa, Urea, Creatinina, Ácido Úrico, Colesterol y Triglicéridos)':
          for (const iterator of this.examenes) {
            if(iterator.consulta == '1 QS 6 (Glucosa, Urea, Creatinina, Ácido Úrico, Colesterol y Triglicéridos)'){
              this.contadorestudios+= 1
            }
          }
          /* console.log('asghasgdh'); */
          
          if(this.contadorestudios < 1){
            return true;
          }else{
            return false;
          }
        break;
      case '3 Exámenes Generales de Orina':
          for (const iterator of this.examenes) {
            if(iterator.consulta == '3 Exámenes Generales de Orina'){
              this.contadorestudios+= 1
            }
          }
          /* console.log(this.contadorestudios); */
          
          if(this.contadorestudios < 3){
            return true;
          }else{
            return false;
          }
        break;
      case '1 V.D.R.L.':
          for (const iterator of this.examenes) {
            if(iterator.consulta == '1 V.D.R.L.'){
              this.contadorestudios+= 1
            }
          }
          if(this.contadorestudios < 1){
            return true;
          }else{
            return false;
          }
        break;
      case '1 V.I.H.':
          for (const iterator of this.examenes) {
            if(iterator.consulta == '1 V.I.H.'){
              this.contadorestudios+= 1
            }
          }
          if(this.contadorestudios < 1){
            return true;
          }else{
            return false;
          }
        break;
      case '2 Grupos Sanguíneos (Ambos padres)':
          for (const iterator of this.examenes) {
            if(iterator.consulta == '2 Grupos Sanguíneos (Ambos padres)'){
              this.contadorestudios+= 1
            }
          }
          if(this.contadorestudios < 2){
            return true;
          }else{
            return false;
          }
        break;
      case '1 Tiempos de Coagulación':
          for (const iterator of this.examenes) {
            if(iterator.consulta == '1 Tiempos de Coagulación'){
              this.contadorestudios+= 1
            }
          }
          if(this.contadorestudios < 1){
            return true;
          }else{
            return false;
          }
        break;
        case '1 Curva de tolerancia a la glucosa':
          for (const iterator of this.examenes) {
            if(iterator.consulta == '1 Curva de tolerancia a la glucosa'){
              this.contadorestudios+= 1
            }
          }
          if(this.contadorestudios < 1){
            return true;
          }else{
            return false;
          }
        break;
        case '1 Pruebas de funcionamiento hepático':
          for (const iterator of this.examenes) {
            if(iterator.consulta == '1 Pruebas de funcionamiento hepático'){
              this.contadorestudios+= 1
            }
          }
          if(this.contadorestudios < 1){
            return true;
          }else{
            return false;
          }
        break;
        case '1 Depuración de proteínas en orina de 24 horas':
          for (const iterator of this.examenes) {
            if(iterator.consulta == '1 Depuración de proteínas en orina de 24 horas'){
              this.contadorestudios+= 1
            }
          }
          if(this.contadorestudios < 1){
            return true;
          }else{
            return false;
          }
        break;
        case '1 Triple ':
          for (const iterator of this.examenes) {
            if(iterator.consulta == '1 Triple '){
              this.contadorestudios+= 1
            }
          }
          if(this.contadorestudios < 1){
            return true;
          }else{
            return false;
          }
        break;
        case '1 Perfil tiroideo':
          for (const iterator of this.examenes) {
            if(iterator.consulta == '1 Perfil tiroideo'){
              this.contadorestudios+= 1
            }
          }
          if(this.contadorestudios < 1){
            return true;
          }else{
            return false;
          }
        break;
        case '1 Urocultivo':
          for (const iterator of this.examenes) {
            if(iterator.consulta == '1 Urocultivo'){
              this.contadorestudios+= 1
            }
          }
          if(this.contadorestudios < 1){
            return true;
          }else{
            return false;
          }
        break;
        case '1 Exudado vaginal':
          for (const iterator of this.examenes) {
            if(iterator.consulta == '1 Exudado vaginal'){
              this.contadorestudios+= 1
            }
          }
          if(this.contadorestudios < 1){
            return true;
          }else{
            return false;
          }
        break;
        case '1 higado y Via Biliar':
          for (const iterator of this.examenes) {
            if(iterator.consulta == '1 higado y Via Biliar'){
              this.contadorestudios+= 1
            }
          }
          if(this.contadorestudios < 1){
            return true;
          }else{
            return false;
          }
        break;
        case '4 Obstétricos':
          for (const iterator of this.examenes) {
            if(iterator.consulta == '4 Obstétricos'){
              this.contadorestudios+= 1
            }
          }
          if(this.contadorestudios < 4){
            return true;
          }else{
            return false;
          }
        break;
        case '1 Genetico (11-13.6 SDG)':
          for (const iterator of this.examenes) {
            if(iterator.consulta == '1 Genetico (11-13.6 SDG)'){
              this.contadorestudios+= 1
            }
          }
          if(this.contadorestudios < 1){
            return true;
          }else{
            return false;
          }
        break;
        case '1 Estructural (  18- 24 SDG)':
          for (const iterator of this.examenes) {
            if(iterator.consulta == '1 Estructural (  18- 24 SDG)'){
              this.contadorestudios+= 1
            }
          }
          if(this.contadorestudios < 1){
            return true;
          }else{
            return false;
          }
        break;
        case '1 Perfil biofisico (Ultimo trimestre)':
          for (const iterator of this.examenes) {
            if(iterator.consulta == '1 Perfil biofisico (Ultimo trimestre)'){
              this.contadorestudios+= 1
            }
          }
          if(this.contadorestudios < 1){
            return true;
          }else{
            return false;
          }
        break;
    }
  }

  validacioncitas(){
    this.contadorcitas=0
    switch(this.consultas.consulta){      
      case '7 Consultas programadas con el Especialista':
        for (const iterator of this.citas) {
          if(iterator.consulta == '7 Consultas programadas con el Especialista'){
            this.contadorcitas+= 1
          }
        }
        if(this.contadorcitas < 7){
          return true;
        }else{
          return false;
        }
        break;
      case '3 consultas de Nutrición':
          for (const iterator of this.citas) {
            if(iterator.consulta == '3 consultas de Nutrición'){
              this.contadorcitas+= 1
            }
          }
          if(this.contadorcitas < 3){
            return true;
          }else{
            return false;
          }
        break;
    }
  }

  //metodo 
  setDatesVenta(medico){
    if(this.consultas.consulta == '7 Consultas programadas con el Especialista'){
      const item = {
        name:this.nombrePaquete,
        nombreEstudio: "CONSULTA DE MEDICO GENERAL",
        precioCon:0,
        precioSin:0,
        _id:'5fd3ebca08ccbb0017712f0d'
      }
      this.carrito.items.push(item);
    }else{
      const item = {
        name:this.nombrePaquete,
        nombreEstudio: "CONSULTA NUTRICION",
        precioCon:0,
        precioSin:0,
        _id:'5fdd2a7fa71eca0017a8e930'
      }
      this.carrito.items.push(item);
    }
  
    const dates = new Dates();
    //this.infoVenta.totalCompra = this.carrito.totalSin;
    this.fecha = moment().format('l');    
    this.infoVenta.hora = moment().format('LT');
    this.infoVenta.vendedor = getDataStorage()._id;
    if(this.pacienteInfo.id == undefined){
      this.infoVenta.paciente = this.pacienteInfo._id;
    }else{
      this.infoVenta.paciente = this.pacienteInfo.id;
    }
    this.infoVenta.sede = this.sede;
    this.infoVenta.prioridad = "Programado"
    this.infoVenta.fecha = dates.getDate();
    this.infoVenta.doctorQueSolicito = medico;
    this.infoVenta.estudios= this.carrito.items
    this.infoVenta.totalCompra = 0;
        
  }
  
  pagar_consulta(nombre,medico){
    this.carrito={
      totalSin: 0,
      totalCon: 0,
      items: []
    };
    const dates = new Dates(); 
    /* console.log(nombre); */
    //19
    switch (nombre) {
      case '1 Biometría Hemática Completa ':
            this.item.nombreEstudio = "BIOMETRIA HEMATICA COMPLETA";
            this.item._id = '5fd284b11cb0ea0017f57bd8';
            this.fecha = moment().format('l');    
            this.infoVenta.hora = moment().format('LT');
            this.infoVenta.vendedor = getDataStorage()._id;
            if(this.pacienteInfo.id == undefined){
              this.infoVenta.paciente = this.pacienteInfo._id;
            }else{
              this.infoVenta.paciente = this.pacienteInfo.id;
            }
            this.infoVenta.nombrePaciente = this.pacienteInfo.nombrePaciente+ ' ' + this.pacienteInfo.apellidoPaterno+ ' ' + this.pacienteInfo.apellidoMaterno
            this.infoVenta.sede = this.sede;
            this.infoVenta.prioridad = "Programado"
            this.infoVenta.fecha = dates.getDate();
            this.infoVenta.doctorQueSolicito = medico;
            /* this.carrito.items.push(this.item); */
            this.infoVenta.estudios= this.carrito.items
            this.infoVenta.totalCompra = 0;
            const item = {
              name:this.nombrePaquete,
              nombreEstudio: "BIOMETRIA HEMATICA COMPLETA",
              precioCon:0,
              precioSin:0,
              idEstudio:'5fd284b11cb0ea0017f57bd8'
            }
            this.carrito.items.push(item);
            this._pagoServicios.agregarPedido( this.infoVenta )
            .subscribe( (data) => {
              // console.log( data );
              if(  data['ok'] ){
                
                this.generarTicket(data['folio']);
                  // se crea la tabla de las ventas 
                  this.setDatesPedidos();
                  this.pedidosLaboratorios.estudios.push(item);
                  /* console.log(this.pedidosLaboratorios); */
                  
                  this._pagoServicios.pedidosLaboratorio( this.pedidosLaboratorios ).subscribe( data => {/* console.log( data ) */});
                                      Swal.fire({
                      icon: 'success',
                      title: '',
                      text: 'LA CONSULTA SE AGREGO EXITOSAMENTE',
                    })
                  this.pedidosLaboratorios = { 
                    estudios:[],
                    idPaciente:"",
                    fecha:"",
                    hora:"",
                    medicoQueSolicita:"",
                    sede:"",
                    prioridad:"Programado",
                    estado:""
                  }
                  // ESTA INFORMCAIÓN SE ENVIA A LOS DIFERENTES SERVICIOS 
                  // seteamos las fechas 
                    eliminarStorage();
                    
                    
                    const eliminarPaciente = new PacienteStorage();
                    eliminarPaciente.removePacienteStorage();  
                }
            });
            this.mostrarConsultas();
        break;
      case '1 QS 6 (Glucosa, Urea, Creatinina, Ácido Úrico, Colesterol y Triglicéridos)':
        this.item.nombreEstudio = "QUIMICA SANGUINEA 6 ";
        this.item._id = '5fd2a4171cb0ea0017f57ddb';
        this.fecha = moment().format('l');    
        this.infoVenta.hora = moment().format('LT');
        this.infoVenta.vendedor = getDataStorage()._id;
        if(this.pacienteInfo.id == undefined){
          this.infoVenta.paciente = this.pacienteInfo._id;
        }else{
          this.infoVenta.paciente = this.pacienteInfo.id;
        }
        this.infoVenta.nombrePaciente = this.pacienteInfo.nombrePaciente+ ' ' + this.pacienteInfo.apellidoPaterno+ ' ' + this.pacienteInfo.apellidoMaterno
        this.infoVenta.sede = this.sede;
        this.infoVenta.prioridad = "Programado"
        this.infoVenta.fecha = dates.getDate();
        this.infoVenta.doctorQueSolicito = medico;
        /* this.carrito.items.push(this.item); */
        this.infoVenta.estudios= this.carrito.items
        this.infoVenta.totalCompra = 0;
        const item2 = {
          name:this.nombrePaquete,
          nombreEstudio: "QUIMICA SANGUINEA 6 ",
          precioCon:0,
          precioSin:0,
          idEstudio:'5fd2a4171cb0ea0017f57ddb'
        }
        this.carrito.items.push(item2);
        this._pagoServicios.agregarPedido( this.infoVenta )
        .subscribe( (data) => {
          // console.log( data );
          if(  data['ok'] ){
            
            this.generarTicket(data['folio']);
              // se crea la tabla de las ventas 
              this.setDatesPedidos();
              this.pedidosLaboratorios.estudios.push(item2);
              /* console.log(this.pedidosLaboratorios); */
                  
              this._pagoServicios.pedidosLaboratorio( this.pedidosLaboratorios ).subscribe( data => {/* console.log( data ) */});
                                  Swal.fire({
                      icon: 'success',
                      title: '',
                      text: 'LA CONSULTA SE AGREGO EXITOSAMENTE',
                    })
                    this.pedidosLaboratorios = { 
                estudios:[],
                idPaciente:"",
                fecha:"",
                hora:"",
                medicoQueSolicita:"",
                sede:"",
                prioridad:"Programado",
                estado:""
              }
              // ESTA INFORMCAIÓN SE ENVIA A LOS DIFERENTES SERVICIOS 
              // seteamos las fechas 
                eliminarStorage();
                
                
                const eliminarPaciente = new PacienteStorage();
                eliminarPaciente.removePacienteStorage();  
            }
        });
        this.mostrarConsultas();
        break;
      case '3 Exámenes Generales de Orina':
        this.item.nombreEstudio = "EXAMEN GENERAL DE ORINA ";
        this.item._id = '5fd28c2f1cb0ea0017f57c58';
        this.fecha = moment().format('l');    
        this.infoVenta.hora = moment().format('LT');
        this.infoVenta.vendedor = getDataStorage()._id;
        if(this.pacienteInfo.id == undefined){
          this.infoVenta.paciente = this.pacienteInfo._id;
        }else{
          this.infoVenta.paciente = this.pacienteInfo.id;
        }
        this.infoVenta.nombrePaciente = this.pacienteInfo.nombrePaciente+ ' ' + this.pacienteInfo.apellidoPaterno+ ' ' + this.pacienteInfo.apellidoMaterno
        this.infoVenta.sede = this.sede;
        this.infoVenta.prioridad = "Programado"
        this.infoVenta.fecha = dates.getDate();
        this.infoVenta.doctorQueSolicito = medico;
        /* this.carrito.items.push(this.item); */
        this.infoVenta.estudios= this.carrito.items
        this.infoVenta.totalCompra = 0;
        const item3 = {
          name:this.nombrePaquete,
          nombreEstudio: "EXAMEN GENERAL DE ORINA ",
          precioCon:0,
          precioSin:0,
          idEstudio:'5fd28c2f1cb0ea0017f57c58'
        }
        this.carrito.items.push(item3);
        this._pagoServicios.agregarPedido( this.infoVenta )
        .subscribe( (data) => {
          // console.log( data );
          if(  data['ok'] ){
            
            this.generarTicket(data['folio']);
              // se crea la tabla de las ventas 
              this.setDatesPedidos();
              this.pedidosLaboratorios.estudios.push(item3);
              /* console.log(this.pedidosLaboratorios); */
                  
              this._pagoServicios.pedidosLaboratorio( this.pedidosLaboratorios ).subscribe( data => {/* console.log( data ) */});
                      Swal.fire({
                      icon: 'success',
                      title: '',
                      text: 'LA CONSULTA SE AGREGO EXITOSAMENTE',
                    })
                this.pedidosLaboratorios = { 
                estudios:[],
                idPaciente:"",
                fecha:"",
                hora:"",
                medicoQueSolicita:"",
                sede:"",
                prioridad:"Programado",
                estado:""
              }
              // ESTA INFORMCAIÓN SE ENVIA A LOS DIFERENTES SERVICIOS 
              // seteamos las fechas 
                eliminarStorage();
                
                
                const eliminarPaciente = new PacienteStorage();
                eliminarPaciente.removePacienteStorage();  
            }
        });
        this.mostrarConsultas();
        break;
      case '1 V.D.R.L.':
        this.item.nombreEstudio = "V.D.R.L.";
        this.item._id = '5fd29b611cb0ea0017f57d51';
        this.fecha = moment().format('l');    
        this.infoVenta.hora = moment().format('LT');
        this.infoVenta.vendedor = getDataStorage()._id;
        if(this.pacienteInfo.id == undefined){
          this.infoVenta.paciente = this.pacienteInfo._id;
        }else{
          this.infoVenta.paciente = this.pacienteInfo.id;
        }
        this.infoVenta.nombrePaciente = this.pacienteInfo.nombrePaciente+ ' ' + this.pacienteInfo.apellidoPaterno+ ' ' + this.pacienteInfo.apellidoMaterno
        this.infoVenta.sede = this.sede;
        this.infoVenta.prioridad = "Programado"
        this.infoVenta.fecha = dates.getDate();
        this.infoVenta.doctorQueSolicito = medico;
        /* this.carrito.items.push(this.item); */
        this.infoVenta.estudios= this.carrito.items
        this.infoVenta.totalCompra = 0;
        const item4 = {
          name:this.nombrePaquete,
          nombreEstudio: "V.D.R.L.",
          precioCon:0,
          precioSin:0,
          idEstudio:'5fd29b611cb0ea0017f57d51'
        }
        this.carrito.items.push(item4);
        this._pagoServicios.agregarPedido( this.infoVenta )
        .subscribe( (data) => {
          // console.log( data );
          if(  data['ok'] ){
            
            this.generarTicket(data['folio']);
              // se crea la tabla de las ventas
              this.setDatesPedidos();
              this.pedidosLaboratorios.estudios.push(item4);
              /* console.log(this.pedidosLaboratorios); */
                  
              this._pagoServicios.pedidosLaboratorio( this.pedidosLaboratorios ).subscribe( data => {/* console.log( data ) */}); 
                    Swal.fire({
                      icon: 'success',
                      title: '',
                      text: 'LA CONSULTA SE AGREGO EXITOSAMENTE',
                    })
                    this.pedidosLaboratorios = { 
                estudios:[],
                idPaciente:"",
                fecha:"",
                hora:"",
                medicoQueSolicita:"",
                sede:"",
                prioridad:"Programado",
                estado:""
              }
              // ESTA INFORMCAIÓN SE ENVIA A LOS DIFERENTES SERVICIOS 
              // seteamos las fechas 
                eliminarStorage();
                
                
                const eliminarPaciente = new PacienteStorage();
                eliminarPaciente.removePacienteStorage();  
            }
        });
        this.mostrarConsultas();
        break;
      case '1 V.I.H.':
        this.item.nombreEstudio = "H.I.V. (PRUEBA PRESUNTIVA)";
        this.item._id = '5fd293501cb0ea0017f57caa';
        this.fecha = moment().format('l');    
        this.infoVenta.hora = moment().format('LT');
        this.infoVenta.vendedor = getDataStorage()._id;
        if(this.pacienteInfo.id == undefined){
          this.infoVenta.paciente = this.pacienteInfo._id;
        }else{
          this.infoVenta.paciente = this.pacienteInfo.id;
        }
        this.infoVenta.nombrePaciente = this.pacienteInfo.nombrePaciente+ ' ' + this.pacienteInfo.apellidoPaterno+ ' ' + this.pacienteInfo.apellidoMaterno
        this.infoVenta.sede = this.sede;
        this.infoVenta.prioridad = "Programado"
        this.infoVenta.fecha = dates.getDate();
        this.infoVenta.doctorQueSolicito = medico;
        /* this.carrito.items.push(this.item); */
        this.infoVenta.estudios= this.carrito.items
        this.infoVenta.totalCompra = 0;
        const item5 = {
          name:this.nombrePaquete,
          nombreEstudio: "H.I.V. (PRUEBA PRESUNTIVA)",
          precioCon:0,
          precioSin:0,
          idEstudio:'5fd293501cb0ea0017f57caa'
        }
        this.carrito.items.push(item5);
        this._pagoServicios.agregarPedido( this.infoVenta )
        .subscribe( (data) => {
          // console.log( data );
          if(  data['ok'] ){
            
            this.generarTicket(data['folio']);
              // se crea la tabla de las ventas 
              this.setDatesPedidos();
              this.pedidosLaboratorios.estudios.push(item5);
              /* console.log(this.pedidosLaboratorios); */
                  
              this._pagoServicios.pedidosLaboratorio( this.pedidosLaboratorios ).subscribe( data => {/* console.log( data ) */});
                                  Swal.fire({
                      icon: 'success',
                      title: '',
                      text: 'LA CONSULTA SE AGREGO EXITOSAMENTE',
                    })
                    this.pedidosLaboratorios = { 
                estudios:[],
                idPaciente:"",
                fecha:"",
                hora:"",
                medicoQueSolicita:"",
                sede:"",
                prioridad:"Programado",
                estado:""
              }
              // ESTA INFORMCAIÓN SE ENVIA A LOS DIFERENTES SERVICIOS 
              // seteamos las fechas 
                eliminarStorage();
                
                
                const eliminarPaciente = new PacienteStorage();
                eliminarPaciente.removePacienteStorage();  
            }
        });
        this.mostrarConsultas();
        break;
      case '2 Grupos Sanguíneos (Ambos padres)':
        this.item.nombreEstudio = "GRUPO Y FACTOR RH";
        this.item._id = '5fd292b31cb0ea0017f57c9e';
        this.fecha = moment().format('l');    
        this.infoVenta.hora = moment().format('LT');
        this.infoVenta.vendedor = getDataStorage()._id;
        if(this.pacienteInfo.id == undefined){
          this.infoVenta.paciente = this.pacienteInfo._id;
        }else{
          this.infoVenta.paciente = this.pacienteInfo.id;
        }
        this.infoVenta.nombrePaciente = this.pacienteInfo.nombrePaciente+ ' ' + this.pacienteInfo.apellidoPaterno+ ' ' + this.pacienteInfo.apellidoMaterno
        this.infoVenta.sede = this.sede;
        this.infoVenta.prioridad = "Programado"
        this.infoVenta.fecha = dates.getDate();
        this.infoVenta.doctorQueSolicito = medico;
        /* this.carrito.items.push(this.item); */
        this.infoVenta.estudios= this.carrito.items
        this.infoVenta.totalCompra = 0;
        const item6 = {
          name:this.nombrePaquete,
          nombreEstudio: "GRUPO Y FACTOR RH",
          precioCon:0,
          precioSin:0,
          idEstudio:'5fd292b31cb0ea0017f57c9e'
        }
        this.carrito.items.push(item6);
        this._pagoServicios.agregarPedido( this.infoVenta )
        .subscribe( (data) => {
          // console.log( data );
          if(  data['ok'] ){
            
            this.generarTicket(data['folio']);
              // se crea la tabla de las ventas 
              this.setDatesPedidos();
              this.pedidosLaboratorios.estudios.push(item6);
              /* console.log(this.pedidosLaboratorios); */
                  
              this._pagoServicios.pedidosLaboratorio( this.pedidosLaboratorios ).subscribe( data => {/* console.log( data ) */});
                                  Swal.fire({
                      icon: 'success',
                      title: '',
                      text: 'LA CONSULTA SE AGREGO EXITOSAMENTE',
                    })
                    this.pedidosLaboratorios = { 
                estudios:[],
                idPaciente:"",
                fecha:"",
                hora:"",
                medicoQueSolicita:"",
                sede:"",
                prioridad:"Programado",
                estado:""
              }
              // ESTA INFORMCAIÓN SE ENVIA A LOS DIFERENTES SERVICIOS 
              // seteamos las fechas 
                eliminarStorage();
                
                
                const eliminarPaciente = new PacienteStorage();
                eliminarPaciente.removePacienteStorage();  
            }
        });
        this.mostrarConsultas();
        break;
      case '1 Tiempos de Coagulación':
        this.item.nombreEstudio = "TIEMPO DE COAGULACION  ";
        this.item._id = '5fd29a991cb0ea0017f57d41';
        this.fecha = moment().format('l');    
        this.infoVenta.hora = moment().format('LT');
        this.infoVenta.vendedor = getDataStorage()._id;
        if(this.pacienteInfo.id == undefined){
          this.infoVenta.paciente = this.pacienteInfo._id;
        }else{
          this.infoVenta.paciente = this.pacienteInfo.id;
        }
        this.infoVenta.nombrePaciente = this.pacienteInfo.nombrePaciente+ ' ' + this.pacienteInfo.apellidoPaterno+ ' ' + this.pacienteInfo.apellidoMaterno
        this.infoVenta.sede = this.sede;
        this.infoVenta.prioridad = "Programado"
        this.infoVenta.fecha = dates.getDate();
        this.infoVenta.doctorQueSolicito = medico;
        /* this.carrito.items.push(this.item); */
        this.infoVenta.estudios= this.carrito.items
        this.infoVenta.totalCompra = 0;
        const item7 = {
          name:this.nombrePaquete,
          nombreEstudio: "TIEMPO DE COAGULACION  ",
          precioCon:0,
          precioSin:0,
          idEstudio:'5fd29a991cb0ea0017f57d41'
        }
        this.carrito.items.push(item7);
        this._pagoServicios.agregarPedido( this.infoVenta )
        .subscribe( (data) => {
          // console.log( data );
          if(  data['ok'] ){
            this.generarTicket(data['folio']);

              // se crea la tabla de las ventas
              this.setDatesPedidos();
              this.pedidosLaboratorios.estudios.push(item7);
              /* console.log(this.pedidosLaboratorios); */
                  
              this._pagoServicios.pedidosLaboratorio( this.pedidosLaboratorios ).subscribe( data => {/* console.log( data ) */}); 
                                  Swal.fire({
                      icon: 'success',
                      title: '',
                      text: 'LA CONSULTA SE AGREGO EXITOSAMENTE',
                    })
                    this.pedidosLaboratorios = { 
                estudios:[],
                idPaciente:"",
                fecha:"",
                hora:"",
                medicoQueSolicita:"",
                sede:"",
                prioridad:"Programado",
                estado:""
              }
              // ESTA INFORMCAIÓN SE ENVIA A LOS DIFERENTES SERVICIOS 
              // seteamos las fechas 
                eliminarStorage();
                
                
                const eliminarPaciente = new PacienteStorage();
                eliminarPaciente.removePacienteStorage();  
            }
        });
        this.mostrarConsultas();
        break;
      case '1 Curva de tolerancia a la glucosa':
        this.item.nombreEstudio = "CURVA  DE   TOLERANCIA  A  LA  GLUCOSA  2 HRS";
        this.item._id = '5fd2a0051cb0ea0017f57d7a';
        this.fecha = moment().format('l');    
        this.infoVenta.hora = moment().format('LT');
        this.infoVenta.vendedor = getDataStorage()._id;
        if(this.pacienteInfo.id == undefined){
          this.infoVenta.paciente = this.pacienteInfo._id;
        }else{
          this.infoVenta.paciente = this.pacienteInfo.id;
        }
        this.infoVenta.nombrePaciente = this.pacienteInfo.nombrePaciente+ ' ' + this.pacienteInfo.apellidoPaterno+ ' ' + this.pacienteInfo.apellidoMaterno
        this.infoVenta.sede = this.sede;
        this.infoVenta.prioridad = "Programado"
        this.infoVenta.fecha = dates.getDate();
        this.infoVenta.doctorQueSolicito = medico;
        /* this.carrito.items.push(this.item); */
        this.infoVenta.estudios= this.carrito.items
        this.infoVenta.totalCompra = 0;
        const item8 = {
          name:this.nombrePaquete,
          nombreEstudio: "CURVA  DE   TOLERANCIA  A  LA  GLUCOSA  2 HRS",
          precioCon:0,
          precioSin:0,
          idEstudio:'5fd2a0051cb0ea0017f57d7a'
        }
        this.carrito.items.push(item8);
        this._pagoServicios.agregarPedido( this.infoVenta )
        .subscribe( (data) => {
          // console.log( data );
          if(  data['ok'] ){
            
            this.generarTicket(data['folio']);
              // se crea la tabla de las ventas 
              this.setDatesPedidos();
              this.pedidosLaboratorios.estudios.push(item8);
              /* console.log(this.pedidosLaboratorios); */
                  
              this._pagoServicios.pedidosLaboratorio( this.pedidosLaboratorios ).subscribe( data => {/* console.log( data ) */});
                                  Swal.fire({
                      icon: 'success',
                      title: '',
                      text: 'LA CONSULTA SE AGREGO EXITOSAMENTE',
                    })
                    this.pedidosLaboratorios = { 
                estudios:[],
                idPaciente:"",
                fecha:"",
                hora:"",
                medicoQueSolicita:"",
                sede:"",
                prioridad:"Programado",
                estado:""
              }
              // ESTA INFORMCAIÓN SE ENVIA A LOS DIFERENTES SERVICIOS 
              // seteamos las fechas 
                eliminarStorage();
                
                
                const eliminarPaciente = new PacienteStorage();
                eliminarPaciente.removePacienteStorage();  
            }
        });
        this.mostrarConsultas();
        break;
      case '1 Pruebas de funcionamiento hepático':
        this.item.nombreEstudio = "PRUEBAS DE FUNCION HEPATICO (BT,BD,BI,TGO,TGP,ALK)";
        this.item._id = '5fd2992b1cb0ea0017f57d1e';
        this.fecha = moment().format('l');    
        this.infoVenta.hora = moment().format('LT');
        this.infoVenta.vendedor = getDataStorage()._id;
        if(this.pacienteInfo.id == undefined){
          this.infoVenta.paciente = this.pacienteInfo._id;
        }else{
          this.infoVenta.paciente = this.pacienteInfo.id;
        }
        this.infoVenta.nombrePaciente = this.pacienteInfo.nombrePaciente+ ' ' + this.pacienteInfo.apellidoPaterno+ ' ' + this.pacienteInfo.apellidoMaterno
        this.infoVenta.sede = this.sede;
        this.infoVenta.prioridad = "Programado"
        this.infoVenta.fecha = dates.getDate();
        this.infoVenta.doctorQueSolicito = medico;
        /* this.carrito.items.push(this.item); */
        this.infoVenta.estudios= this.carrito.items
        this.infoVenta.totalCompra = 0;
        const item9 = {
          name:this.nombrePaquete,
          nombreEstudio: "PRUEBAS DE FUNCION HEPATICO (BT,BD,BI,TGO,TGP,ALK)",
          precioCon:0,
          precioSin:0,
          idEstudio:'5fd2992b1cb0ea0017f57d1e'
        }
        this.carrito.items.push(item9);
        this._pagoServicios.agregarPedido( this.infoVenta )
        .subscribe( (data) => {
          // console.log( data );
          if(  data['ok'] ){
            
            this.generarTicket(data['folio']);
              // se crea la tabla de las ventas 
              this.setDatesPedidos();
              this.pedidosLaboratorios.estudios.push(item9);
              /* console.log(this.pedidosLaboratorios); */
                  
              this._pagoServicios.pedidosLaboratorio( this.pedidosLaboratorios ).subscribe( data => {/* console.log( data ) */});
                                  Swal.fire({
                      icon: 'success',
                      title: '',
                      text: 'LA CONSULTA SE AGREGO EXITOSAMENTE',
                    })
                    this.pedidosLaboratorios = { 
                estudios:[],
                idPaciente:"",
                fecha:"",
                hora:"",
                medicoQueSolicita:"",
                sede:"",
                prioridad:"Programado",
                estado:""
              }
              // ESTA INFORMCAIÓN SE ENVIA A LOS DIFERENTES SERVICIOS 
              // seteamos las fechas 
                eliminarStorage();
                
                
                const eliminarPaciente = new PacienteStorage();
                eliminarPaciente.removePacienteStorage();  
            }
        });
        this.mostrarConsultas();
        break;
      case '1 Depuración de proteínas en orina de 24 horas':
        this.item.nombreEstudio = "PROTEINAS EN ORINA DE 24 HRS.";
        this.item._id = '5fd2984d1cb0ea0017f57d0f';
        this.fecha = moment().format('l');    
        this.infoVenta.hora = moment().format('LT');
        this.infoVenta.vendedor = getDataStorage()._id;
        if(this.pacienteInfo.id == undefined){
          this.infoVenta.paciente = this.pacienteInfo._id;
        }else{
          this.infoVenta.paciente = this.pacienteInfo.id;
        }
        this.infoVenta.nombrePaciente = this.pacienteInfo.nombrePaciente+ ' ' + this.pacienteInfo.apellidoPaterno+ ' ' + this.pacienteInfo.apellidoMaterno
        this.infoVenta.sede = this.sede;
        this.infoVenta.prioridad = "Programado"
        this.infoVenta.fecha = dates.getDate();
        this.infoVenta.doctorQueSolicito = medico;
        /* this.carrito.items.push(this.item); */
        this.infoVenta.estudios= this.carrito.items
        this.infoVenta.totalCompra = 0;
        const item10 = {
          name:this.nombrePaquete,
          nombreEstudio: "PROTEINAS EN ORINA DE 24 HRS.",
          precioCon:0,
          precioSin:0,
          idEstudio:'5fd2984d1cb0ea0017f57d0f'
        }
        this.carrito.items.push(item10);
        this._pagoServicios.agregarPedido( this.infoVenta )
        .subscribe( (data) => {
          // console.log( data );
          if(  data['ok'] ){
            
            this.generarTicket(data['folio']);
              // se crea la tabla de las ventas 
              this.setDatesPedidos();
              this.pedidosLaboratorios.estudios.push(item10);
              /* console.log(this.pedidosLaboratorios); */
                  
              this._pagoServicios.pedidosLaboratorio( this.pedidosLaboratorios ).subscribe( data => {/* console.log( data ) */});
                                  Swal.fire({
                      icon: 'success',
                      title: '',
                      text: 'LA CONSULTA SE AGREGO EXITOSAMENTE',
                    })
                    this.pedidosLaboratorios = { 
                estudios:[],
                idPaciente:"",
                fecha:"",
                hora:"",
                medicoQueSolicita:"",
                sede:"",
                prioridad:"Programado",
                estado:""
              }
              // ESTA INFORMCAIÓN SE ENVIA A LOS DIFERENTES SERVICIOS 
              // seteamos las fechas 
                eliminarStorage();
                
                
                const eliminarPaciente = new PacienteStorage();
                eliminarPaciente.removePacienteStorage();  
            }
        });
        this.mostrarConsultas();
        break;
      case '1 Triple ':
        this.item.nombreEstudio = "TRIPLE MARCADO CON INTERPRETACION GRAFICA";
        this.item._id = '5fd2a4821cb0ea0017f57de2';
        this.fecha = moment().format('l');    
        this.infoVenta.hora = moment().format('LT');
        this.infoVenta.vendedor = getDataStorage()._id;
        if(this.pacienteInfo.id == undefined){
          this.infoVenta.paciente = this.pacienteInfo._id;
        }else{
          this.infoVenta.paciente = this.pacienteInfo.id;
        }
        this.infoVenta.nombrePaciente = this.pacienteInfo.nombrePaciente+ ' ' + this.pacienteInfo.apellidoPaterno+ ' ' + this.pacienteInfo.apellidoMaterno
        this.infoVenta.sede = this.sede;
        this.infoVenta.prioridad = "Programado"
        this.infoVenta.fecha = dates.getDate();
        this.infoVenta.doctorQueSolicito = medico;
        /* this.carrito.items.push(this.item); */
        this.infoVenta.estudios= this.carrito.items
        this.infoVenta.totalCompra = 0;
        const item11 = {
          name:this.nombrePaquete,
          nombreEstudio: "TRIPLE MARCADO CON INTERPRETACION GRAFICA",
          precioCon:0,
          precioSin:0,
          idEstudio:'5fd2a4821cb0ea0017f57de2'
        }
        this.carrito.items.push(item11);
        this._pagoServicios.agregarPedido( this.infoVenta )
        .subscribe( (data) => {
          // console.log( data );
          if(  data['ok'] ){
            
            this.generarTicket(data['folio']);
              // se crea la tabla de las ventas
              this.setDatesPedidos();
              this.pedidosLaboratorios.estudios.push(item11);
              /* console.log(this.pedidosLaboratorios); */
                  
              this._pagoServicios.pedidosLaboratorio( this.pedidosLaboratorios ).subscribe( data => {/* console.log( data ) */}); 
                                  Swal.fire({
                      icon: 'success',
                      title: '',
                      text: 'LA CONSULTA SE AGREGO EXITOSAMENTE',
                    })
                    this.pedidosLaboratorios = { 
                estudios:[],
                idPaciente:"",
                fecha:"",
                hora:"",
                medicoQueSolicita:"",
                sede:"",
                prioridad:"Programado",
                estado:""
              }
              // ESTA INFORMCAIÓN SE ENVIA A LOS DIFERENTES SERVICIOS 
              // seteamos las fechas 
                eliminarStorage();
                
                
                const eliminarPaciente = new PacienteStorage();
                eliminarPaciente.removePacienteStorage();  
            }
        });
        this.mostrarConsultas();
        break;
      case '1 Perfil tiroideo':
        this.item.nombreEstudio = "PERFIL TIROIDEO I";
        this.item._id = '5fd2a3771cb0ea0017f57dcf';
        this.fecha = moment().format('l');    
        this.infoVenta.hora = moment().format('LT');
        this.infoVenta.vendedor = getDataStorage()._id;
        if(this.pacienteInfo.id == undefined){
          this.infoVenta.paciente = this.pacienteInfo._id;
        }else{
          this.infoVenta.paciente = this.pacienteInfo.id;
        }
        this.infoVenta.nombrePaciente = this.pacienteInfo.nombrePaciente+ ' ' + this.pacienteInfo.apellidoPaterno+ ' ' + this.pacienteInfo.apellidoMaterno
        this.infoVenta.sede = this.sede;
        this.infoVenta.prioridad = "Programado"
        this.infoVenta.fecha = dates.getDate();
        this.infoVenta.doctorQueSolicito = medico;
        /* this.carrito.items.push(this.item); */
        this.infoVenta.estudios= this.carrito.items
        this.infoVenta.totalCompra = 0;
        const item12 = {
          name:this.nombrePaquete,
          nombreEstudio: "PERFIL TIROIDEO I",
          precioCon:0,
          precioSin:0,
          idEstudio:'5fd2a3771cb0ea0017f57dcf'
        }
        this.carrito.items.push(item12);
        this._pagoServicios.agregarPedido( this.infoVenta )
        .subscribe( (data) => {
          // console.log( data );
          if(  data['ok'] ){
            
            this.generarTicket(data['folio']);
              // se crea la tabla de las ventas 
              this.setDatesPedidos();
              this.pedidosLaboratorios.estudios.push(item12);
              /* console.log(this.pedidosLaboratorios); */
                  
              this._pagoServicios.pedidosLaboratorio( this.pedidosLaboratorios ).subscribe( data => {/* console.log( data ) */});
                                  Swal.fire({
                      icon: 'success',
                      title: '',
                      text: 'LA CONSULTA SE AGREGO EXITOSAMENTE',
                    });
                this.pedidosLaboratorios = { 
                estudios:[],
                idPaciente:"",
                fecha:"",
                hora:"",
                medicoQueSolicita:"",
                sede:"",
                prioridad:"Programado",
                estado:""
              }
              // ESTA INFORMCAIÓN SE ENVIA A LOS DIFERENTES SERVICIOS 
              // seteamos las fechas 
                eliminarStorage();
                
                
                const eliminarPaciente = new PacienteStorage();
                eliminarPaciente.removePacienteStorage();  
            }
        });
        this.mostrarConsultas();
        break;
      case '1 Urocultivo':
        this.item.nombreEstudio = "CULTIVO DE ORINA (UROCULTIVO)";
        this.item._id = '5fd28a2b1cb0ea0017f57c32';
        this.fecha = moment().format('l');    
        this.infoVenta.hora = moment().format('LT');
        this.infoVenta.vendedor = getDataStorage()._id;
        if(this.pacienteInfo.id == undefined){
          this.infoVenta.paciente = this.pacienteInfo._id;
        }else{
          this.infoVenta.paciente = this.pacienteInfo.id;
        }
        this.infoVenta.nombrePaciente = this.pacienteInfo.nombrePaciente+ ' ' + this.pacienteInfo.apellidoPaterno+ ' ' + this.pacienteInfo.apellidoMaterno
        this.infoVenta.sede = this.sede;
        this.infoVenta.prioridad = "Programado"
        this.infoVenta.fecha = dates.getDate();
        this.infoVenta.doctorQueSolicito = medico;
        /* this.carrito.items.push(this.item); */
        this.infoVenta.estudios= this.carrito.items
        this.infoVenta.totalCompra = 0;
        const item13 = {
          name:this.nombrePaquete,
          nombreEstudio: "CULTIVO DE ORINA (UROCULTIVO)",
          precioCon:0,
          precioSin:0,
          idEstudio:'5fd28a2b1cb0ea0017f57c32'
        }
        this.carrito.items.push(item13);
        this._pagoServicios.agregarPedido( this.infoVenta )
        .subscribe( (data) => {
          // console.log( data );
          if(  data['ok'] ){
            
            this.generarTicket(data['folio']);
              // se crea la tabla de las ventas 
              this.setDatesPedidos();
              this.pedidosLaboratorios.estudios.push(item13);
              /* console.log(this.pedidosLaboratorios); */
                  
              this._pagoServicios.pedidosLaboratorio( this.pedidosLaboratorios ).subscribe( data => {/* console.log( data ) */});
                                  Swal.fire({
                      icon: 'success',
                      title: '',
                      text: 'LA CONSULTA SE AGREGO EXITOSAMENTE',
                    })
                    this.pedidosLaboratorios = { 
                estudios:[],
                idPaciente:"",
                fecha:"",
                hora:"",
                medicoQueSolicita:"",
                sede:"",
                prioridad:"Programado",
                estado:""
              }
              // ESTA INFORMCAIÓN SE ENVIA A LOS DIFERENTES SERVICIOS 
              // seteamos las fechas 
                eliminarStorage();
                
                
                const eliminarPaciente = new PacienteStorage();
                eliminarPaciente.removePacienteStorage();  
            }
        });
        this.mostrarConsultas();
        break;
      case '1 Exudado vaginal':
        this.item.nombreEstudio = "CULTIVO CERVICO VAGINAL";
        this.item._id = '5fd289731cb0ea0017f57c24';
        this.fecha = moment().format('l');    
        this.infoVenta.hora = moment().format('LT');
        this.infoVenta.vendedor = getDataStorage()._id;
        if(this.pacienteInfo.id == undefined){
          this.infoVenta.paciente = this.pacienteInfo._id;
        }else{
          this.infoVenta.paciente = this.pacienteInfo.id;
        }
        this.infoVenta.nombrePaciente = this.pacienteInfo.nombrePaciente+ ' ' + this.pacienteInfo.apellidoPaterno+ ' ' + this.pacienteInfo.apellidoMaterno
        this.infoVenta.sede = this.sede;
        this.infoVenta.prioridad = "Programado"
        this.infoVenta.fecha = dates.getDate();
        this.infoVenta.doctorQueSolicito = medico;
        /* this.carrito.items.push(this.item); */
        this.infoVenta.estudios= this.carrito.items
        this.infoVenta.totalCompra = 0;
        const item14 = {
          name:this.nombrePaquete,
          nombreEstudio: "CULTIVO CERVICO VAGINAL",
          precioCon:0,
          precioSin:0,
          idEstudio:'5fd289731cb0ea0017f57c24'
        }
        this.carrito.items.push(item14);
        this._pagoServicios.agregarPedido( this.infoVenta )
        .subscribe( (data) => {
          // console.log( data );
          if(  data['ok'] ){
            
            this.generarTicket(data['folio']);
              // se crea la tabla de las ventas 
              this.setDatesPedidos();
              this.pedidosLaboratorios.estudios.push(item14);
              /* console.log(this.pedidosLaboratorios); */
                  
              this._pagoServicios.pedidosLaboratorio( this.pedidosLaboratorios ).subscribe( data => {/* console.log( data ) */});
                                  Swal.fire({
                      icon: 'success',
                      title: '',
                      text: 'LA CONSULTA SE AGREGO EXITOSAMENTE',
                    })
                    this.pedidosLaboratorios = { 
                estudios:[],
                idPaciente:"",
                fecha:"",
                hora:"",
                medicoQueSolicita:"",
                sede:"",
                prioridad:"Programado",
                estado:""
              }
              // ESTA INFORMCAIÓN SE ENVIA A LOS DIFERENTES SERVICIOS 
              // seteamos las fechas 
                eliminarStorage();
                
                
                const eliminarPaciente = new PacienteStorage();
                eliminarPaciente.removePacienteStorage();  
            }
        });
        this.mostrarConsultas();
        break;
      case '1 Perfil biofisico (Ultimo trimestre)':
        this.item.nombreEstudio = "ULTRASONIDO PERFIL BIOFISICO";
        this.item._id = '602edde98a29f000156dbc2d';
        this.fecha = moment().format('l');    
        this.infoVenta.hora = moment().format('LT');
        this.infoVenta.vendedor = getDataStorage()._id;
        if(this.pacienteInfo.id == undefined){
          this.infoVenta.paciente = this.pacienteInfo._id;
        }else{
          this.infoVenta.paciente = this.pacienteInfo.id;
        }
        this.infoVenta.nombrePaciente = this.pacienteInfo.nombrePaciente+ ' ' + this.pacienteInfo.apellidoPaterno+ ' ' + this.pacienteInfo.apellidoMaterno
        this.infoVenta.sede = this.sede;
        this.infoVenta.prioridad = "Programado"
        this.infoVenta.fecha = dates.getDate();
        this.infoVenta.doctorQueSolicito = medico;
        /* this.carrito.items.push(this.item); */
        this.infoVenta.estudios= this.carrito.items
        this.infoVenta.totalCompra = 0;
        const item15 = {
          name:this.nombrePaquete,
          nombreEstudio: "ULTRASONIDO PERFIL BIOFISICO",
          precioCon:0,
          precioSin:0,
          idEstudio:'602edde98a29f000156dbc2d'
        }
        this.carrito.items.push(item15);
        this._pagoServicios.agregarPedido( this.infoVenta )
        .subscribe( (data) => {
          // console.log( data );
          if(  data['ok'] ){
            
            this.generarTicket(data['folio']);
              // se crea la tabla de las ventas 
              this.setDatesPedidos();
              this.pedidosUltrasonido.estudios.push(item15)
              this._pagoServicios.postPedidosUltra( this.pedidosUltrasonido ).subscribe( data => {/* console.log( data ) */});
                                  Swal.fire({
                      icon: 'success',
                      title: '',
                      text: 'LA CONSULTA SE AGREGO EXITOSAMENTE',
                    })
                    this.pedidosUltrasonido = {
                idPaciente:"", 
                estudios:[],
                fecha:"",
                hora:"",
                medicoQueSolicita:"",
                sede:"",
                prioridad:"Programado"
              }
              // ESTA INFORMCAIÓN SE ENVIA A LOS DIFERENTES SERVICIOS 
              // seteamos las fechas 
                eliminarStorage();
                
                
                const eliminarPaciente = new PacienteStorage();
                eliminarPaciente.removePacienteStorage();  
            }
        });
        this.mostrarConsultas();
        break;
      case '1 Estructural (  18- 24 SDG)':
        this.item.nombreEstudio = "ULTRASONIDO OBSTETRICO ESTRUCTURAL DEL SEGUNDO TRIMESTRE ";
        this.item._id = '5fd280c41cb0ea0017f57b91';
        this.fecha = moment().format('l');    
        this.infoVenta.hora = moment().format('LT');
        this.infoVenta.vendedor = getDataStorage()._id;
        if(this.pacienteInfo.id == undefined){
          this.infoVenta.paciente = this.pacienteInfo._id;
        }else{
          this.infoVenta.paciente = this.pacienteInfo.id;
        }
        this.infoVenta.nombrePaciente = this.pacienteInfo.nombrePaciente+ ' ' + this.pacienteInfo.apellidoPaterno+ ' ' + this.pacienteInfo.apellidoMaterno
        this.infoVenta.sede = this.sede;
        this.infoVenta.prioridad = "Programado"
        this.infoVenta.fecha = dates.getDate();
        this.infoVenta.doctorQueSolicito = medico;
        /* this.carrito.items.push(this.item); */
        this.infoVenta.estudios= this.carrito.items
        this.infoVenta.totalCompra = 0;
        const item16 = {
          name:this.nombrePaquete,
          nombreEstudio: "ULTRASONIDO OBSTETRICO ESTRUCTURAL DEL SEGUNDO TRIMESTRE ",
          precioCon:0,
          precioSin:0,
          idEstudio:'5fd280c41cb0ea0017f57b91'
        }
        this.carrito.items.push(item16);
        this._pagoServicios.agregarPedido( this.infoVenta )
        .subscribe( (data) => {
          // console.log( data );
          if(  data['ok'] ){
            
            this.generarTicket(data['folio']);
              // se crea la tabla de las ventas 
              this.setDatesPedidos();
              this.pedidosUltrasonido.estudios.push(item16)
              this._pagoServicios.postPedidosUltra( this.pedidosUltrasonido ).subscribe( data => {/* console.log( data ) */});
                                  Swal.fire({
                      icon: 'success',
                      title: '',
                      text: 'LA CONSULTA SE AGREGO EXITOSAMENTE',
                    })
                    this.pedidosUltrasonido = {
                idPaciente:"", 
                estudios:[],
                fecha:"",
                hora:"",
                medicoQueSolicita:"",
                sede:"",
                prioridad:"Programado"
              }
              // ESTA INFORMCAIÓN SE ENVIA A LOS DIFERENTES SERVICIOS 
              // seteamos las fechas 
                eliminarStorage();
                
                
                const eliminarPaciente = new PacienteStorage();
                eliminarPaciente.removePacienteStorage();  
            }
        });
        this.mostrarConsultas();
        break;
      case '1 Genetico (11-13.6 SDG)':
        this.item.nombreEstudio = "ULTRASONIDO OBSTETRICO ESTRUCTURAL DEL PRIMER TRISMESTRE";
        this.item._id = '5fd2810b1cb0ea0017f57b96';
        this.fecha = moment().format('l');    
        this.infoVenta.hora = moment().format('LT');
        this.infoVenta.vendedor = getDataStorage()._id;
        if(this.pacienteInfo.id == undefined){
          this.infoVenta.paciente = this.pacienteInfo._id;
        }else{
          this.infoVenta.paciente = this.pacienteInfo.id;
        }
        this.infoVenta.nombrePaciente = this.pacienteInfo.nombrePaciente+ ' ' + this.pacienteInfo.apellidoPaterno+ ' ' + this.pacienteInfo.apellidoMaterno
        this.infoVenta.sede = this.sede;
        this.infoVenta.prioridad = "Programado"
        this.infoVenta.fecha = dates.getDate();
        this.infoVenta.doctorQueSolicito = medico;
        /* this.carrito.items.push(this.item); */
        this.infoVenta.estudios= this.carrito.items
        this.infoVenta.totalCompra = 0;
        const item17 = {
          name:this.nombrePaquete,
          nombreEstudio: "ULTRASONIDO OBSTETRICO ESTRUCTURAL DEL PRIMER TRISMESTRE",
          precioCon:0,
          precioSin:0,
          idEstudio:'5fd2810b1cb0ea0017f57b96'
        }
        this.carrito.items.push(item17);
        this._pagoServicios.agregarPedido( this.infoVenta )
        .subscribe( (data) => {
          // console.log( data );
          if(  data['ok'] ){
            
            this.generarTicket(data['folio']);
              // se crea la tabla de las ventas
              this.setDatesPedidos();
              this.pedidosUltrasonido.estudios.push(item17)
              this._pagoServicios.postPedidosUltra( this.pedidosUltrasonido ).subscribe( data => {/* console.log( data ) */}); 
                                  Swal.fire({
                      icon: 'success',
                      title: '',
                      text: 'LA CONSULTA SE AGREGO EXITOSAMENTE',
                    })
                    this.pedidosUltrasonido = {
                idPaciente:"", 
                estudios:[],
                fecha:"",
                hora:"",
                medicoQueSolicita:"",
                sede:"",
                prioridad:"Programado"
              }
              // ESTA INFORMCAIÓN SE ENVIA A LOS DIFERENTES SERVICIOS 
              // seteamos las fechas 
                eliminarStorage();
                
                
                const eliminarPaciente = new PacienteStorage();
                eliminarPaciente.removePacienteStorage();  
            }
        });
        this.mostrarConsultas();
        break;
      case '4 Obstétricos':
        this.item.nombreEstudio = "ULTRASONIDO OBSTETRICO CONVENCIONAL";
        this.item._id = '5fd280031cb0ea0017f57b87';
        this.fecha = moment().format('l');    
        this.infoVenta.hora = moment().format('LT');
        this.infoVenta.vendedor = getDataStorage()._id;
        if(this.pacienteInfo.id == undefined){
          this.infoVenta.paciente = this.pacienteInfo._id;
        }else{
          this.infoVenta.paciente = this.pacienteInfo.id;
        }
        this.infoVenta.nombrePaciente = this.pacienteInfo.nombrePaciente+ ' ' + this.pacienteInfo.apellidoPaterno+ ' ' + this.pacienteInfo.apellidoMaterno
        this.infoVenta.sede = this.sede;
        this.infoVenta.prioridad = "Programado"
        this.infoVenta.fecha = dates.getDate();
        this.infoVenta.doctorQueSolicito = medico;
        /* this.carrito.items.push(this.item); */
        this.infoVenta.estudios= this.carrito.items
        this.infoVenta.totalCompra = 0;
        const item18 = {
          name:this.nombrePaquete,
          nombreEstudio: "ULTRASONIDO OBSTETRICO CONVENCIONAL",
          precioCon:0,
          precioSin:0,
          idEstudio:'5fd280031cb0ea0017f57b87'
        }
        this.carrito.items.push(item18);
        this._pagoServicios.agregarPedido( this.infoVenta )
        .subscribe( (data) => {
          // console.log( data );
          if(  data['ok'] ){
            
            this.generarTicket(data['folio']);
              // se crea la tabla de las ventas 
              this.setDatesPedidos();
              this.pedidosUltrasonido.estudios.push(item18)
              this._pagoServicios.postPedidosUltra( this.pedidosUltrasonido ).subscribe( data => {/* console.log( data ) */});
                                  Swal.fire({
                      icon: 'success',
                      title: '',
                      text: 'LA CONSULTA SE AGREGO EXITOSAMENTE',
                    })
                    this.pedidosUltrasonido = {
                idPaciente:"", 
                estudios:[],
                fecha:"",
                hora:"",
                medicoQueSolicita:"",
                sede:"",
                prioridad:"Programado"
              }
              // ESTA INFORMCAIÓN SE ENVIA A LOS DIFERENTES SERVICIOS 
              // seteamos las fechas 
                eliminarStorage();
                
                
                const eliminarPaciente = new PacienteStorage();
                eliminarPaciente.removePacienteStorage();  
            }
        });
        this.mostrarConsultas();
        break;
      case '1 higado y Via Biliar':
        this.item.nombreEstudio = "ULTRASONIDO DE HIGADO Y VIAS BILIARES";
        this.item._id = '5fd27ce51cb0ea0017f57b5a';
        this.fecha = moment().format('l');    
        this.infoVenta.hora = moment().format('LT');
        this.infoVenta.vendedor = getDataStorage()._id;
        if(this.pacienteInfo.id == undefined){
          this.infoVenta.paciente = this.pacienteInfo._id;
        }else{
          this.infoVenta.paciente = this.pacienteInfo.id;
        }
        this.infoVenta.nombrePaciente = this.pacienteInfo.nombrePaciente+ ' ' + this.pacienteInfo.apellidoPaterno+ ' ' + this.pacienteInfo.apellidoMaterno
        this.infoVenta.sede = this.sede;
        this.infoVenta.prioridad = "Programado"
        this.infoVenta.fecha = dates.getDate();
        this.infoVenta.doctorQueSolicito = medico;
        /* this.carrito.items.push(this.item); */
        this.infoVenta.estudios= this.carrito.items
        this.infoVenta.totalCompra = 0;
        const item19 = {
          name:this.nombrePaquete,
          nombreEstudio: "ULTRASONIDO DE HIGADO Y VIAS BILIARES",
          precioCon:0,
          precioSin:0,
          idEstudio:'5fd27ce51cb0ea0017f57b5a'
        }
        this.carrito.items.push(item19);
        this._pagoServicios.agregarPedido( this.infoVenta )
        .subscribe( (data) => {
          // console.log( data );
          if(  data['ok'] ){
            
            this.generarTicket(data['folio']);
              // se crea la tabla de las ventas 
              this.setDatesPedidos();
              this.pedidosUltrasonido.estudios.push(item19)
              this._pagoServicios.postPedidosUltra( this.pedidosUltrasonido ).subscribe( data => {/* console.log( data ) */});
                                  Swal.fire({
                      icon: 'success',
                      title: '',
                      text: 'LA CONSULTA SE AGREGO EXITOSAMENTE',
                    })
                    this.pedidosUltrasonido = {
                idPaciente:"", 
                estudios:[],
                fecha:"",
                hora:"",
                medicoQueSolicita:"",
                sede:"",
                prioridad:"Programado"
              }
              // ESTA INFORMCAIÓN SE ENVIA A LOS DIFERENTES SERVICIOS 
              // seteamos las fechas 
                eliminarStorage();
                
                
                const eliminarPaciente = new PacienteStorage();
                eliminarPaciente.removePacienteStorage();  
            }
        });
        this.mostrarConsultas();
        break;
      default:
        break;
    }
  }

  setDatesPedidos (){
    // this.pedidosLaboratorios.fecha = moment().format('l');
    const datesPedidoLab = new Dates();
    // configuracion de los pedidos de laboratorio
    this.pedidosLaboratorios.fecha = datesPedidoLab.getDate();
    this.pedidosLaboratorios.hora = moment().format('LT');
    this.pedidosLaboratorios.medicoQueSolicita = this.infoVenta.doctorQueSolicito;
    this.pedidosLaboratorios.sede = this.sede;
    this.pedidosLaboratorios.idPaciente = this.infoVenta.paciente;
    this.pedidosLaboratorios.idPaciente = this.infoVenta.paciente;
    // configuracion de los pedidos de ultrasonido
    this.pedidosUltrasonido.idPaciente = this.infoVenta.paciente;
    this.pedidosUltrasonido.fecha = datesPedidoLab.getDate();
    this.pedidosUltrasonido.sede = this.sede;
  }

  mostrarConsultas(){
    this.obtenerPaquete();
  }

  generarTicket(folio){

    const tickets = new Tickets();
    tickets.printTicketSinMembresia( this.pacienteInfo, this.carrito ,  this.infoVenta, folio);
  
  }
}

class consultas  {

  tipo:  string;
  consulta:  string;
  fecha:  string;
  firma:  string;
  hora:  string;
  medico:  string;

}
