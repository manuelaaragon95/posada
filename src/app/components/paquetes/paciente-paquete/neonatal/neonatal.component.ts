import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { PaquetesService } from '../../../../services/paquetes/paquetes.service';
import * as moment from 'moment';
import Swal from 'sweetalert2'
import Dates from '../../../../clases/dates/dates';
import { getDataStorage } from 'src/app/functions/storage/storage.functions';
/* import { CEDE } from 'src/app/classes/cedes/cedes.class'; */
import Tickets from '../../../../clases/tickets/tickets.class';
import { PagosService } from '../../../../services/pagos/pagos.service';
import { eliminarStorage } from 'src/app/functions/storage/pacienteIntegrados';
import PacienteStorage from 'src/app/clases/pacientes/pacientes.class';


@Component({
  selector: 'app-neonatal',
  templateUrl: './neonatal.component.html',
  styleUrls: ['./neonatal.component.css']
})
export class NeonatalComponent implements OnInit {

  @Input() id: String;
  consultas:any = { tipo: '', consulta: '', fecha: '', medico: '', firma: '' }
  item:any ={ nombreEstudio:'', precioCon:0, precioSin:0, _id:''}
  medicos:any[] = []
  paquete:any[] = []
  concepto:any[] = []
  citas:any[] = []
  examenes:any[] = []
  public egoCount = 0;
  public ego2Count = 0;
  public ego3Count = 0;
  public obsCount = 0;
  public ultCount=0;
  public labCount=0;
  public GOCount=0;
  public GSCount=0;
  public contador=0;
    public hospitalizacion = {fecha:"", hora:"" , consulta:"", medico: "", firma:"" };
    public consultaDespuesParto = {fecha:"", hora:"" , consulta:"", medico: "", firma:"" };
    // public paqueteExamenesInto = [];
    public fechaConsulta = moment().format('l');
    public horaIngreso = moment().format('hh:mm');
    public btnAgregaConsultas = false;
    public pacienteInfo={
      nombrePaciente: "",
      apellidoPaterno: "",
      apellidoMaterno: "",
      curp: "",
      edad: 0,
      genero: "",
      id:"",
      callePaciente: "",
      fechaNacimientoPaciente:"",
      estadoPaciente: "",
      paisPaciente: "",
      telefono: "",
      _id:""
    };
    public infoVenta = {  

      paciente:"",
      nombrePaciente:"",
      vendedor:"",
      fecha:"",
      hora:"",
      estudios:[],
      efectivo:false,
      doctorQueSolicito:"",
      transferencia: false,
      tarjetCredito:false,
      tarjetaDebito:false,
      montoEfectivo:0,
      montoTarjteCredito:0,
      montoTarjetaDebito:0,
      montoTranferencia: 0,
      sede:"",
      totalCompra:0,
      prioridad:"",
      compraConMembresia:true,
      status:'Pagado'
    }
  
    public carrito = {
      totalSin: 0,
      totalCon: 0,
      items: []
    };
  fecha: string;

  public pedidosLaboratorios = { 
    estudios:[],
    idPaciente:"",
    fecha:"",
    hora:"",
    medicoQueSolicita:"",
    sede:"",
    prioridad:"Programado",
    estado:""
  }

  public contadorcitas = 0;

  public diasRestantes:Number;
  public fechaRegistro: string;
  public nombrePaquete='';
  public sede='';

  constructor(public _router: ActivatedRoute, 
              public _paquete:PaquetesService, 
              public _route:Router, 
              private _pagoServicios: PagosService) {
                this.obtenerSede();
              }

  ngOnInit(): void {
    this.obtenerMedicos();
    this.obtenerPaquete();
    this.consultas.firma = JSON.parse(localStorage.getItem('usuario')).nombre;
  }

  obtenerSede(){
    this.sede = localStorage.getItem('cede')
  }

  obtenerPaquete(){
    this._paquete.obtenerPaquete(this.id)
    .subscribe(  (data:any) =>  {
      this.pacienteInfo = data['paquetes'][0]['paciente'];
      this.paquete.push(data['paquetes']);
      this.citas = this.paquete[0].visitas
      this.examenes = this.paquete[0].examenesLab;
      this.fechaRegistro = this.paquete[0].fecha;
      this.nombrePaquete = this.paquete[0].paquete.nombrePaquete;
      this.verCosnultashechas();
      this.getDiasRestantes();
    });
  }

  getDiasRestantes(){
    const dias = new Dates()
    this.diasRestantes  =  dias.getDateRest( this.fechaRegistro ); 
    if( this.diasRestantes <= 0 || this.diasRestantes == NaN ){
      let estado = {
        membresiaActiva: false
      }
      this._paquete.actualizarEstadoMembresia( this.pacienteInfo._id, estado )
      .subscribe( data =>{});
    }
  }

  obtenerMedicos(){
    this._paquete.getMedicos()
    .subscribe( (data) => {
      this.medicos = data['medicos']
    });
  }

  irAUnServicio(  servicio ){
    this.setRouteService( servicio );
  }

  setRouteService(servicio){
    this._route.navigate([ `/serviciosInt/${servicio}`]);
  }

  verCosnultashechas(){
    this.egoCount = 0;
    this.ego2Count = 0;
    this.ego3Count = 0;
    this.citas.forEach(element => {
      // console.log( element );
      if( element.consulta == "12 Consultas con Especialista en Pediatría" ){
        // console.log("Se lo lee");
        this.egoCount += 1;
        let cardCEP = document.querySelector('#cardCEP');
        let badgeCEP = document.querySelector('#badgeCEP');
        this.addClassesCss( cardCEP, badgeCEP);
        //this.hospitalizacion = element ;
      } 
      if( element.consulta === 'Cita abierta a Urgencias con Medico General las 24 Horas'){
        this.ego2Count += 1;
        let cardCAUMG = document.querySelector('#cardCAUMG');
        let badgeCAUMG = document.querySelector('#badgeCAUMG');
        this.addClassesCss( cardCAUMG, badgeCAUMG);
        //this.consultaDespuesParto = element;
      }
      if( element.consulta === '2 Estancias cortas de hasta 8 Horas en urgencias'){
        this.ego3Count += 1;
        let cardEC = document.querySelector('#cardEC');
        let badgeEC = document.querySelector('#badgeEC');
        this.addClassesCss( cardEC, badgeEC);
        //this.consultaDespuesParto = element;
      }
    });
    // termian el codigo que valida las consultas 
    this.verEstudios();
  }

  verEstudios(){
    this.GOCount=0;
    this.GSCount=0;
    // badgeQuimica
   //   // quimicaSanguineaCard
    this.examenes.forEach( (examen:consultas) => {
      if( examen.consulta === "1 Tamiz ampliado (Dentro de los primeros 10 días de nacido)" ){
        this.GOCount += 1
        let cardTA = document.querySelector('#TACard');
        let badgeTA = document.querySelector('#badegeTA');
        this.addClassesCss( cardTA, badgeTA);
      }
      if( examen.consulta === "1 Grupo Sanguineo y Factor RH"  ){
        this.GSCount +=1
        let cardGSFH = document.querySelector('#GSFHCard');
        let badgeGSFH = document.querySelector("#badgeGSFH");
        this.addClassesCss( cardGSFH, badgeGSFH );
      }
    });
   //   //  termina el codigo que valida los examenes 
  }

  showMessage(examen: string){
    this.examenes.forEach(  (estudio:consultas, index) => {
      // console.log( estudio.consulta  === examen  );
      // console.log( estudio  );
        if( estudio.consulta ===  examen  ){
          // console.log(estudio.consulta, index);
        Swal.fire({
          icon: 'success',
          title: '',
          text: `Tipo:  ${this.examenes[index].consulta}\n  Fecha: ${this.examenes[index].fecha} \n Hora: ${this.examenes[index].hora}\n   Médico ${this.examenes[index].medico}\n  Atendió: ${this.examenes[index].firma}`,
        })
        }else{
          this.citas.forEach((cita:consultas, index)=>{
            if(cita.consulta === examen){

              Swal.fire({
                icon: 'success',
                title: '',
                text: `Tipo:  ${this.citas[index].consulta}\n  Fecha: ${this.citas[index].fecha} \n Hora: ${this.citas[index].hora}\n   Médico ${this.citas[index].medico}\n  Atendió: ${this.citas[index].firma}` ,
              })
            }
          })
        }
    });
  }

  addClassesCss( card: Element, badge:Element ){
    card.classList.add('border-primary');
    if( badge.id == "badgeCEP"){
      badge.innerHTML = this.egoCount.toString(); 
    } else if(badge.id == "badgeCAUMG"){
      badge.innerHTML = this.ego2Count.toString();
    } else if(badge.id == "badgeEC"){
      badge.innerHTML = this.ego3Count.toString();
    }else if(badge.id == "badegeTA"){
      badge.innerHTML = this.GOCount.toString();
    }else if(badge.id == "badgeGSFH"){
      badge.innerHTML = this.GSCount.toString();
    }
    badge.classList.add('badge-primary');
  }

  //insercion
  seleccion($event, value){
    switch (value) {
      case 'visitas':
        this.concepto=[]
        for(let item of this.paquete ){
          for(let item2 of item.paquete.CitasIncluidas){
            this.concepto.push(item2)
          }
          /* console.log(this.concepto) */
        }  
        this.consultas.consulta = ''
      break;

      case 'examenesLab':
        this.concepto=[]
        for(let item of this.paquete ){
          for(let item2 of item.paquete.examenesLaboratorio){
            this.concepto.push(item2)
          }
          /* console.log(this.concepto) */
        }  
        this.consultas.consulta = ''
      break;

      default:
        break;
    }
  }


  agregarConsulta(){
    this.consultas.fecha = new Date()
    this.consultas.hora = this.horaIngreso;
    this.consultas.firma = JSON.parse(localStorage.getItem('usuario')).nombre;
    /* console.log(this.consultas) */
    if(this.consultas.tipo == '' || this.consultas.consulta == '' || this.consultas.medico == '' || this.consultas.firma == ''){
        Swal.fire({
          icon: 'error',
          title: '',
          text: 'INGRESA LOS DATOS REQUERIDOS',
        })
    }else{
      if(this.consultas.tipo == 'visitas'){
        if(this.consultas.consulta == '12 Consultas con Especialista en Pediatría'){
          let vali = true;
          vali = this.validacionesVisitas();
          if(vali){
            this._paquete.agregarConsulta(this.consultas,this.consultas.tipo,this.id).subscribe(
              (data)=>{
                this.mostrarConsultas();
                this.setDatesVenta(this.consultas.medico);
                this._pagoServicios.agregarPedido( this.infoVenta )
              .subscribe( (data) => {
                // console.log( data );
                if(  data['ok'] ){
                  
                  this.generarTicket(data['folio']);
                    // se crea la tabla de las ventas 
                                          Swal.fire({
                        icon: 'success',
                        title: '',
                        text: 'CONSULTA AGREGADA',
                      })
    
                    // ESTA INFORMCAIÓN SE ENVIA A LOS DIFERENTES SERVICIOS 
                    // seteamos las fechas 
                      eliminarStorage();
                      
                      
                      const eliminarPaciente = new PacienteStorage();
                      eliminarPaciente.removePacienteStorage();
                      this.consultas = { tipo: '', consulta: '', fecha: '', medico: '', firma:'' }
                      this._route.navigateByUrl('consulta');  
                }
              });
            })
          }else{
                    Swal.fire({
          icon: 'error',
          title: '',
          text: 'SE TERMINARO LAS CONSULTAS DEL PAQUETE',
        })
            this.consultas = { tipo: '', consulta: '', fecha: '', medico: '', firma:'' }
          }
        }else if(this.consultas.consulta == '2 Estancias cortas de hasta 8 Horas en urgencias'){
          let vali = true;
          vali = this.validacionesVisitas();
          /* console.log(vali); */
          
          if(vali){
            this._paquete.agregarConsulta(this.consultas,this.consultas.tipo,this.id).subscribe(
              (data)=>{
                this.mostrarConsultas();
                this.setDatesVenta(this.consultas.medico);
                this._pagoServicios.agregarPedido( this.infoVenta )
              .subscribe( (data) => {
                // console.log( data );
                if(  data['ok'] ){
                  
                  this.generarTicket(data['folio']);
                    // se crea la tabla de las ventas 
                            Swal.fire({
          icon: 'success',
          title: '',
          text: 'CONSULTA AGREGADA ',
        })
                    // ESTA INFORMCAIÓN SE ENVIA A LOS DIFERENTES SERVICIOS 
                    // seteamos las fechas 
                      eliminarStorage();
                      
                      
                      const eliminarPaciente = new PacienteStorage();
                      eliminarPaciente.removePacienteStorage();
                      this.consultas = { tipo: '', consulta: '', fecha: '', medico: '', firma:'' }
                      this._route.navigateByUrl('consulta');  
                }
              });
            })
          }else{
                    Swal.fire({
          icon: 'error',
          title: '',
          text: 'SE TERMINARON LAS CONSULTAS DE TU PAQUETE',
        })
            this.consultas = { tipo: '', consulta: '', fecha: '', medico: '', firma:'' }
          }
        }else{
          this._paquete.agregarConsulta(this.consultas,this.consultas.tipo,this.id).subscribe(
            (data)=>{
              this.mostrarConsultas();
              this.setDatesVenta(this.consultas.medico);
              this._pagoServicios.agregarPedido( this.infoVenta )
            .subscribe( (data) => {
              // console.log( data );
              if(  data['ok'] ){
                
                this.generarTicket(data['folio']);
                  // se crea la tabla de las ventas 
                          Swal.fire({
          icon: 'success',
          title: '',
          text: 'CONSULTA AGREGADA',
        })
                  // ESTA INFORMCAIÓN SE ENVIA A LOS DIFERENTES SERVICIOS 
                  // seteamos las fechas 
                    eliminarStorage();
                    
                    
                    const eliminarPaciente = new PacienteStorage();
                    eliminarPaciente.removePacienteStorage();
                    this.consultas = { tipo: '', consulta: '', fecha: '', medico: '', firma:'' }
                    this._route.navigateByUrl('consulta');  
              }
            });
          })
        }
        
      }else{
        let val = true;
        val = this.validaciones();
        if(val){
          this._paquete.agregarConsulta(this.consultas,this.consultas.tipo,this.id).subscribe(
            (data)=>{
              this.setDatesVenta(this.consultas.medico);          
              this.pagar_consulta(this.consultas.consulta, this.consultas.medico);
              this.consultas = { tipo: '', consulta: '', fecha: '', medico: '', firma:'' }
            })
        }else{
                  Swal.fire({
          icon: 'error',
          title: '',
          text: 'SE TERINARON LOS LABORATORIOS DEL PAQUETE',
        })
          this.consultas = { tipo: '', consulta: '', fecha: '', medico: '', firma:'' }
        }
      }
    }
  }

  validacionesVisitas(){
    this.contadorcitas = 0
    switch(this.consultas.consulta){      
      case '12 Consultas con Especialista en Pediatría':
        for (const iterator of this.citas) {
          if(iterator.consulta == '12 Consultas con Especialista en Pediatría'){
            this.contadorcitas += 1
          }
        }
        if(this.contadorcitas < 12){
          return true;
        }else{
          return false;
        }
        break;
      case '2 Estancias cortas de hasta 8 Horas en urgencias':
          for (const iterator of this.citas) {
            if(iterator.consulta == '2 Estancias cortas de hasta 8 Horas en urgencias'){
              this.contadorcitas += 1
            }
          }
          if(this.contadorcitas < 2){
            return true;
          }else{
            return false;
          }
        break;
      default:
          return true
        break;
    }
  }

  validaciones(){
    this.contador=0;
    switch(this.consultas.consulta){      
      case '1 Tamiz ampliado (Dentro de los primeros 10 días de nacido)':
        for (const iterator of this.examenes) {
          if(iterator.consulta == '1 Tamiz ampliado (Dentro de los primeros 10 días de nacido)'){
            this.contador+= 1
          }
        }
        if(this.contador < 1){
          return true;
        }else{
          return false;
        }
        break;
      case '1 Grupo Sanguineo y Factor RH':
          for (const iterator of this.examenes) {
            if(iterator.consulta == '1 Grupo Sanguineo y Factor RH'){
              this.contador+= 1
            }
          }
          if(this.contador < 1){
            return true;
          }else{
            return false;
          }
        break;
    }
  }

  //metodo 
  setDatesVenta(medico){
    if(this.consultas.consulta == '12 Consultas con Especialista en Pediatría'){
      const item = {
        name:this.nombrePaquete,
        nombreEstudio: "CONSULTA PEDIATRIA",
        precioCon:0,
        precioSin:0,
        _id:'5fdd2bfba71eca0017a8e939'
      }
      this.carrito.items.push(item);
    }else{
      const item = {
        name:this.nombrePaquete,
        nombreEstudio: "CONSULTA DE MEDICINA GENERAL",
        precioCon:0,
        precioSin:0,
        _id:'5fd3ebca08ccbb0017712f0d'
      }
      this.carrito.items.push(item);
    }
    const dates = new Dates();
    //this.infoVenta.totalCompra = this.carrito.totalSin;
    this.fecha = moment().format('l');    
    this.infoVenta.hora = moment().format('LT');
    this.infoVenta.vendedor = getDataStorage()._id;
    if(this.pacienteInfo.id == undefined){
      this.infoVenta.paciente = this.pacienteInfo._id;
    }else{
      this.infoVenta.paciente = this.pacienteInfo.id;
    }
    this.infoVenta.sede = this.sede;
    this.infoVenta.prioridad = "Programado"
    this.infoVenta.fecha = dates.getDate();
    this.infoVenta.doctorQueSolicito = medico;
    this.infoVenta.estudios= this.carrito.items
    this.infoVenta.totalCompra = 0;    
  }

  setDatesPedidos (){
    // this.pedidosLaboratorios.fecha = moment().format('l');
    const datesPedidoLab = new Dates();
    // configuracion de los pedidos de laboratorio
    this.pedidosLaboratorios.fecha = datesPedidoLab.getDate();
    this.pedidosLaboratorios.hora = moment().format('LT');
    this.pedidosLaboratorios.medicoQueSolicita = this.infoVenta.doctorQueSolicito;
    this.pedidosLaboratorios.sede = this.sede;
    this.pedidosLaboratorios.idPaciente = this.infoVenta.paciente;
  }

  pagar_consulta(nombre,medico){
    const dates = new Dates();
    /* console.log(nombre); */

    switch (nombre) {
      case '1 Grupo Sanguineo y Factor RH':
            this.carrito={
              totalSin: 0,
              totalCon: 0,
              items: []
            };
            this.item.nombreEstudio = "GRUPO Y FACTOR RH";
            this.item._id = '5fd292b31cb0ea0017f57c9e';
            //preparar la venta
            this.fecha = moment().format('l');    
            this.infoVenta.hora = moment().format('LT');
            this.infoVenta.vendedor = getDataStorage()._id;
            if(this.pacienteInfo.id == undefined){
              this.infoVenta.paciente = this.pacienteInfo._id;
            }else{
              this.infoVenta.paciente = this.pacienteInfo.id;
            }
            this.infoVenta.nombrePaciente = this.pacienteInfo.nombrePaciente+ ' ' + this.pacienteInfo.apellidoPaterno+ ' ' + this.pacienteInfo.apellidoMaterno
            this.infoVenta.sede = this.sede;
            this.infoVenta.prioridad = "Programado"
            this.infoVenta.fecha = dates.getDate();
            this.infoVenta.doctorQueSolicito = medico;
            /* this.carrito.items.push(this.item); */
            this.infoVenta.estudios= this.carrito.items
            this.infoVenta.totalCompra = 0;
            const item5 = {
              name:this.nombrePaquete,
              nombreEstudio: "GRUPO Y FACTOR RH",
              precioCon:0,
              precioSin:0,
              idEstudio:'5fd292b31cb0ea0017f57c9e'
            }
            this.carrito.items.push(item5);
            this._pagoServicios.agregarPedido( this.infoVenta )
            .subscribe( (data) => {
              // console.log( data );
              if(  data['ok'] ){
                
                this.generarTicket(data['folio']);
                  // se crea la tabla de las ventas 
                  this.setDatesPedidos();
                  this.pedidosLaboratorios.estudios.push(item5);
                  this._pagoServicios.pedidosLaboratorio( this.pedidosLaboratorios ).subscribe( data => {});
                          Swal.fire({
          icon: 'success',
          title: '',
          text: 'CONSULTA AGREGADA',
        })
                  this.pedidosLaboratorios = { 
                    estudios:[],
                    idPaciente:"",
                    fecha:"",
                    hora:"",
                    medicoQueSolicita:"",
                    sede:"",
                    prioridad:"Programado",
                    estado:""
                  }
                  // ESTA INFORMCAIÓN SE ENVIA A LOS DIFERENTES SERVICIOS 
                  // seteamos las fechas 
                    eliminarStorage();
                    
                    
                    const eliminarPaciente = new PacienteStorage();
                    eliminarPaciente.removePacienteStorage();  
                }
            });
            this.mostrarConsultas();
        break;
      case '1 Tamiz ampliado (Dentro de los primeros 10 días de nacido)':
            this.carrito={
              totalSin: 0,
              totalCon: 0,
              items: []
            };
            this.item.nombreEstudio = "TAMIZ METABOLICO AMPLIADO "
            this.item._id = '5fd2a4631cb0ea0017f57de0'
            //preparar la venta
            this.fecha = moment().format('l');    
            this.infoVenta.hora = moment().format('LT');
            this.infoVenta.vendedor = getDataStorage()._id;
            if(this.pacienteInfo.id == undefined){
              this.infoVenta.paciente = this.pacienteInfo._id;
            }else{
              this.infoVenta.paciente = this.pacienteInfo.id;
            }
            this.infoVenta.nombrePaciente = this.pacienteInfo.nombrePaciente+ ' ' + this.pacienteInfo.apellidoPaterno+ ' ' + this.pacienteInfo.apellidoMaterno
            this.infoVenta.sede = this.sede;
            this.infoVenta.prioridad = "Programado"
            this.infoVenta.fecha = dates.getDate();
            this.infoVenta.doctorQueSolicito = medico;
            /* this.carrito.items.push(this.item); */
            this.infoVenta.estudios= this.carrito.items
            this.infoVenta.totalCompra = 0;
            const item6 = {
              name:this.nombrePaquete,
              nombreEstudio: "TAMIZ METABOLICO AMPLIADO ",
              precioCon:0,
              precioSin:0,
              idEstudio:'5fd2a4631cb0ea0017f57de0'
            }
            this.carrito.items.push(item6);
            this._pagoServicios.agregarPedido( this.infoVenta )
            .subscribe( (data) => {
              // console.log( data );
              if(  data['ok'] ){
                
                this.generarTicket(data['folio']);
                  // se crea la tabla de las ventas 
                  this.setDatesPedidos();
                  this.pedidosLaboratorios.estudios.push(item6);
                  this._pagoServicios.pedidosLaboratorio( this.pedidosLaboratorios ).subscribe( data => {/* console.log( data ) */});
                          Swal.fire({
          icon: 'success',
          title: '',
          text: 'CONSULTA AGREGADA',
        })
                  this.pedidosLaboratorios = { 
                    estudios:[],
                    idPaciente:"",
                    fecha:"",
                    hora:"",
                    medicoQueSolicita:"",
                    sede:"",
                    prioridad:"Programado",
                    estado:""
                  }
                  // ESTA INFORMCAIÓN SE ENVIA A LOS DIFERENTES SERVICIOS 
                  // seteamos las fechas 
                    eliminarStorage();
                    
                    
                    const eliminarPaciente = new PacienteStorage();
                    eliminarPaciente.removePacienteStorage();  
                }
            });
            this.mostrarConsultas();
        break;  
      default:
        break;
    }
  }
  mostrarConsultas(){
    this.obtenerPaquete();
  }
  generarTicket(folio){

    const tickets = new Tickets();
    tickets.printTicketSinMembresia( this.pacienteInfo, this.carrito ,  this.infoVenta, folio );
  
  }
 
  mostrarDatos(consulta, medico, servicio){
    if(servicio == '1'){
        Swal.fire({
          icon: 'success',
          title: '',
          text: `Citas incluidas:\n Tipo de consulta: ${consulta}\nMedico: ${medico}`,
        })
    }
    if(servicio == '2'){
        Swal.fire({
          icon: 'success',
          title: '',
          text: `Examenes de Laboratorio:\n,Tipo de laboratorio: ${consulta}\nMedico: ${medico}`,
        })
    }
  }
}

class consultas  {

  tipo:  string;
  consulta:  string;
  fecha:  string;
  firma:  string;
  hora:  string;
  medico:  string;

}
