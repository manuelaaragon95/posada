import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { PaquetesService } from '../../../../services/paquetes/paquetes.service';
import * as moment from 'moment';
import Swal from 'sweetalert2'
 
import { FichaInfo } from 'src/app/clases/ficha-info-paciente';
import { PagosService } from '../../../../services/pagos/pagos.service';
import Dates from '../../../../clases/dates/dates';
import Tickets from '../../../../clases/tickets/tickets.class';
/* import { CEDE } from 'src/app/classes/cedes/cedes.class'; */
import Carrito from '../../../../clases/carrito/carrito.class';

@Component({
  selector: 'app-prenatal',
  templateUrl: './prenatal.component.html',
  styleUrls: ['./prenatal.component.css']
})
export class PrenatalComponent implements OnInit {

    @Input() id: String;
    consultas:any = { tipo: '', consulta: '', fecha: '', hora:'' , medico: '', firma: '' }
    item:any ={ name:'', nombreEstudio:'', precioCon:0, precioSin:0, _id:''}
    medicos:any[] = []
    paquete:any[] = []
    concepto:any[] = []
    citas:any[] = []
    examenes:any[] = []
    totalpagos:number
    pagos:any = { semanaGesta:'', semanapago: '', pago: ''}
    public adelanto=0
    public deposito =0
    tabularpagos:any = []
    public egoCount=0;
    public BHC = 0;
    public QS = 0;
    public EGO = 0;
    public VDRL = 0;
    public GS = 0;
    public VIH = 0;
    public TC = 0;
    public CTG=0;
    public EV =0;
    public URO =0;
    public contador=0;
    public contadorcitas=0;
    public GINE = 0;
    public parto=0;
    public hospi=0;
    public hospitalizacion = {fecha:"", hora:"" , consulta:"", medico: "", firma:"" };
    public consultaDespuesParto = {fecha:"", hora:"" , consulta:"", medico: "", firma:"" };
    // public paqueteExamenesInto = [];
    public fechaConsulta = moment().format('l');
    public horaIngreso = moment().format('hh:mm');
    public btnAgregaConsultas = false;
    public pacienteFicha: FichaInfo;
    
    public pacienteInfo={

      nombrePaciente: "",
      apellidoPaterno: "",
      apellidoMaterno: "",
      curp: "",
      edad: 0,
      genero: "",
      id:"",
      membresiaActiva:true,
      callePaciente: "",
      fechaNacimientoPaciente:"",
      estadoPaciente: "",
      paisPaciente: "",
      telefono: "",
      _id:""
    
    };



    public valPago=true;
    public sem = [];
    public infoVenta = {  

      paciente:"",
      nombrePaciente:"",
      vendedor:"",
      hora:"",
      estudios:[],
      efectivo:false,
      doctorQueSolicito:"",
      transferencia: false,
      tarjetCredito:false,
      tarjetaDebito:false,
      montoEfectivo:0,
      montoTarjteCredito:0,
      montoTarjetaDebito:0,
      montoTranferencia: 0,
      sede:"",
      totalCompra:0,
      prioridad:"",
      compraConMembresia:true,
      status:'Pagado'

    }
  
    public carrito = {
      totalSin: 0,
      totalCon: 0,
      items: []
    };
    
    fecha: string;
    public semanaGestacion=[];
    public semanapago=[];

    public pedidosLaboratorios = { 
      estudios:[],
      idPaciente:"",
      fecha:"",
      hora:"",
      medicoQueSolicita:"",
      sede:"",
      prioridad:"Programado",
      estado:""
    }

    public pedidosUltrasonido = {
      idPaciente:"", 
      estudios:[],
      fecha:"",
      hora:"",
      medicoQueSolicita:"",
      sede:"",
      prioridad:"Programado"
    }

    public checked = {
      incluye: false,
      noincluye: false
    }

    public OBSTETRI=0;
    public ESTRUC = 0;
    public PEDI= 0;
    public comparacion;

    public pagoFijo ;
    public riesgo={
      valores:""
    }

    public folios =548;
    public evaluacion = false
    public acumuladoPaque = 0;
    public  paqueteAcumulado = {acumulado:0}
    public nombrePaquete='';
    public sede='';

  constructor(public _router: ActivatedRoute, 
            public _paquete:PaquetesService, 
            public _route:Router, 
            private _pagoServicios: PagosService
            ) {
    this.obtenerSede();
    // this.obtenerPaquete();
    // this.folios = JSON.parse(localStorage.getItem('folio'));
  } 
  
  ngOnInit(): void {
    this.obtenerPaquete();
    this.obtenerMedicos();
 /* this.validacionescitas() */
    this.consultas.firma = JSON.parse(localStorage.getItem('usuario')).nombre;
  }
//////////////////// calculo de pagos y semanas de pagos automaticos //////////////////////////////

  obtenerSede(){
    this.sede = localStorage.getItem('cede')
  }

  habilitarRiesgo(){
    if(this.riesgo.valores == "si"){
      this.checked.incluye = true
    }else{
      this.checked.noincluye = true
    }
  }

  obtenerPagos(){
    let semana = (39 - this.sem[0].semanaGesta)
    this.pagoFijo = this.tabularpagos.length
    if(this.pagos.semanapago == semana){
      let ultimoPago=0+2500;
      this.tabularpagos.forEach(element => {
        ultimoPago=element.pago+ultimoPago
      });
      /* console.log(17500-ultimoPago); */
      
      this.pagos.pago = (17500- ultimoPago).toFixed(2);
            
    }else{
      /* console.log('no entro'); */
      if(this.pagoFijo == 0){
        let semanapagos2 = (39 - this.sem[0].semanaGesta)
        if(this.acumuladoPaque == 0 || this.acumuladoPaque == null){
          /* console.log('entro1'); */
          
          this.pagos.pago =(this.deposito + (15000 /semanapagos2)).toFixed(2)
        }else{
          /* console.log('entro2'); */
          
          this.pagos.pago = (parseFloat((this.deposito + (15000 /semanapagos2)).toFixed(2)) - this.acumuladoPaque).toFixed(2)
          this.acumuladoPaque = 0
          this.paqueteAcumulado.acumulado = 0
        }
        this.comparacion = this.pagos.pago
      }else {
        let semanapagos = (39 - this.sem[0].semanaGesta)
        if(this.acumuladoPaque == 0 || this.acumuladoPaque == null){
          /* console.log('entro3'); */
          this.pagos.pago = (15000 /semanapagos).toFixed(2) 
        }else{
          /* console.log('entro4'); */
          this.pagos.pago = (parseFloat((15000 /semanapagos).toFixed(2)) - this.acumuladoPaque).toFixed(2) 
          /* console.log(parseFloat((15000 /semanapagos).toFixed(2)));
          console.log(this.acumuladoPaque.toFixed(2)); */
          
          this.acumuladoPaque = 0
          this.paqueteAcumulado.acumulado = 0
        }
      }
    }
    /* console.log(this.pagos); */
    
    /* this.pagos.semanapago = ''; */
  }

  limpiarradio(){
    this.riesgo.valores = ""
  }

  actualizarSemanasPago(){
    /* console.log(this.checked); */
    let semanapagos2 = (39 - this.sem[0].semanaGesta)
    let pagoinicial = parseFloat((15000 /semanapagos2).toFixed(2)) 
    let valor;
    let semana;
    let totalpago = 17500 - this.totalpagos
    if(this.deposito > totalpago){
      this.limpiarradio();
      this.deposito = 0;
       Swal.fire({
              icon: 'error',
              title: '',
              text: 'NO SE PUEDE AGREGAR EL DEPOSITO MAYOR A LA CANTIDAD RESTANTE',
            })
    }else{
      if(this.pagos.semanaGesta == semanapagos2){
         Swal.fire({
              icon: 'error',
              title: '',
              text: 'YA NO PUEDE AGREGAR DEPOSITOS',
            })
      }else{
        if(this.checked.incluye){
          this.deposito = parseFloat((this.deposito - this.pagos.pago).toFixed(2));
          valor= this.deposito/pagoinicial
          semana = parseInt(this.pagos.semanapago) + parseInt(valor)
          this.pagos.semanapago = semana
          this.paqueteAcumulado.acumulado = this.deposito-(parseInt(valor) * parseFloat(pagoinicial.toFixed(2)))
          /* console.log(this.paqueteAcumulado); */
          
          this.pagos.pago = parseFloat(this.pagos.pago) + this.deposito
        }else{
          valor= this.deposito/pagoinicial
          semana = parseInt(this.pagos.semanapago) + parseInt(valor)
          this.pagos.semanapago = semana
          this.paqueteAcumulado.acumulado = this.deposito - (parseInt(valor) * parseFloat(pagoinicial.toFixed(2)))
          /* console.log(this.paqueteAcumulado); */
          this.pagos.pago = parseFloat(this.pagos.pago) + this.deposito        
        }
      }
    }
  }

  obtenerSemanasPago(){
    if(this.sem.length == 0){
      if(this.pagos.semanaGesta == undefined){
        this.pagos.semanaGesta = '1';
      }
      this.pagos.semanapago = (39 - this.pagos.semanaGesta)
      for (let index = 1 ; index <= this.pagos.semanapago; index++) {
        this.semanapago.push(index)
      }
    }else{
      let val =0;
      if(this.sem[0].semanapago == ""){
        this.pagos.semanapago = 1
        for (let index = 1 ; index <= (39 - this.pagos.semanaGesta); index++) {
          this.semanapago.push(index)
        }
      }else{
        val = parseInt(this.sem[0].semanapago) + 1;
        this.pagos.semanapago = val
        for (let index = val ; index <= (39 - this.pagos.semanaGesta); index++) {
          this.semanapago.push(index)
        }
      }
      this.obtenerPagos();
    }
  }
  ///////////////////////////////////////////////////////////////7
  obtenerPaquete(){
    this.acumuladoPaque = 0;
    this._paquete.obtenerPaquete(this.id)
    .subscribe(  (data:any) =>  {
      localStorage.removeItem('paciente');
      localStorage.setItem('paciente', JSON.stringify(data['paquetes']['paciente'])); 
      this.pacienteInfo = data['paquetes']['paciente'];
      if(data['paquetes'].membresiaActiva == false){
        this.cambiarMembresia();
      }
      this.paquete.push(data['paquetes']);
      this.citas = this.paquete[0].visitas;
      this.examenes = this.paquete[0].examenesLab;
      this.tabularpagos = this.paquete[0].pagos;
      this.acumuladoPaque = this.paquete[0].acumulado;
      this.semanasdegestacion(this.tabularpagos);
      this.totalpagos = (0+2500);
      this.tabularpagos = this.tabularpagos.reverse();
      this.nombrePaquete = this.paquete[0].paquete.nombrePaquete;
      this.tabularpagos.forEach(element => {
        this.totalpagos=element.pago+this.totalpagos
      });
      this.verCosnultashechas();
    });
  }

  cambiarMembresia(){
    let estado = {
      membresiaActiva: false
    }
    this._paquete.actualizarEstadoMembresia( this.pacienteInfo._id, estado )
    .subscribe( (data:any) =>{
      localStorage.removeItem('paciente')
      localStorage.setItem('paciente', JSON.stringify(data.data))
    });
  }

  semanasdegestacion(semanas){
    const semana = semanas.reverse();
    if(semanas.length == 0){
      this.sem.push(this.pagos)
      this.generararreglo(this.sem);
    }else{
      this.sem.push(semana[0]);
      this.pagos.semanaGesta = this.sem[0].semanaGesta
      this.obtenerSemanasPago();
    }
  }
  generararreglo(sem:any){
    this.semanaGestacion=[];
    if(sem[0].semanaGesta == ""){
      for (let index = 1; index < 39; index++) {
        this.semanaGestacion.push(index);
      }
    }else{
      const semana = parseInt(sem[0].semanaGesta)+1;
      for (let index = semana; index < 39; index++) {
        this.semanaGestacion.push(index)
      }
    }
  }

  obtenerMedicos(){
    this._paquete.getMedicos()
    .subscribe( (data) => {
      this.medicos = data['medicos']
    });
  }

  irAUnServicio(  servicio ){
    this.setRouteService( servicio );
  }

  setRouteService(servicio){
    this._route.navigate([ `/serviciosInt/${servicio}`]);
  }

  verCosnultashechas(){
    /* console.log(this.citas); */
    
    this.GINE = 0;
    this.OBSTETRI=0;
    this.ESTRUC = 0;
    this.PEDI= 0;
    this.parto = 0;
    this.hospi=0;
    // esta funcion se encarga de ver cuales de los paquetes ya estan hechas y las colorea
    /* let cardHospitalizacion = document.querySelector('#hospitalizacion');
    let badegHospitalizacion = document.querySelector('#badgeHospitalzacion'); */
    // consultas despues del parto
    /* let cardConsultas = document.querySelector('#consultaDespuesDelParto');
    let badgeConsultas = document.querySelector('#badgeParto'); */
    // incia el codifo que valida las consultas 
    this.citas.forEach(element => {
      // console.log( element );
      if( element.consulta == "Cita abierta a urgencias desde la contratación del servicio hasta la atención del parto o cesárea por medicina general" ){
        // console.log("Se lo lee");
        /* cardHospitalizacion.classList.add('border-primary');
        badegHospitalizacion.innerHTML = "1";
        badegHospitalizacion.classList.add('badge-primary');
        this.hospitalizacion = element ; */
      } 
      if( element.consulta === 'Consulta después del parto o cesárea con medicina general' ){
        this.parto += 1;
        let cardBiometria = document.querySelector('#consultaDespuesDelParto');
        let badgeBiometrie = document.querySelector('#badgeParto');
        this.addClassesCss( cardBiometria, badgeBiometrie);
        this.cambiarMembresiaPaquete();
        /* this.pacienteInfo.membresiaActiva = false; */ 
      }
      if( element.consulta === 'Hospitalización en habitación estándar' ){
        this.hospi += 1;
        let cardBiometria = document.querySelector('#hospitalizacion');
        let badgeBiometrie = document.querySelector('#badgeHospitalzacion');
        this.addClassesCss( cardBiometria, badgeBiometrie);
      }
      if(element.consulta == '7 Consultas con especialista en Ginecología y obstetricia'){
        this.GINE += 1;
        let cardBiometria = document.querySelector('#gineco');
        let badgeBiometrie = document.querySelector('#badegeGineco');
        this.addClassesCss( cardBiometria, badgeBiometrie);
      }
      if(element.consulta == '2 Ultrasonidos obstétricos convencionales'){
        this.OBSTETRI += 1;
        let cardBiometria = document.querySelector('#cardObstetri');
        let badgeBiometrie = document.querySelector('#badgeObstetri');
        this.addClassesCss( cardBiometria, badgeBiometrie);
      }
      if(element.consulta == '1 Ultrasonido estructural'){
        this.ESTRUC += 1;
        let cardBiometria = document.querySelector('#estructuralCard');
        let badgeBiometrie = document.querySelector('#badgeEstructural');
        this.addClassesCss( cardBiometria, badgeBiometrie);
      }
      if(element.consulta == '1 consulta con pediatría antes de su parto o cesárea'){
        this.PEDI += 1;
        let cardBiometria = document.querySelector('#pediatria');
        let badgeBiometrie = document.querySelector('#badegePediatria');
        this.addClassesCss( cardBiometria, badgeBiometrie);
      }
    });
    // termian el codigo que valida las consultas 
    this.verEstudios();
  }

  cambiarMembresiaPaquete(){
    let estado = {
      membresiaActiva: false
    }
    this._paquete.cambiarMembresiaPaque(this.id,estado).subscribe((resp:any)=>{
      /* console.log(resp); */
      
    })
  }

  verEstudios(){
    this.egoCount=0;
    this. BHC = 0;
    this. QS = 0;
    this. EGO = 0;
    this. VDRL = 0;
    this. GS = 0;
    this. VIH = 0;
    this. TC = 0;
    this. CTG=0;
    // badgeQuimica
   //   // quimicaSanguineaCard
   for (const iterator of this.examenes) {
     /* }
    this.examenes.forEach( (examen:consultas) => { */
      if( iterator.consulta === "2 Biometría hemática completa" ){
        this.BHC += 1;
        /* console.log(this.BHC); */
        
        let cardBiometria = document.querySelector('#cardBiometria');
        let badgeBiometrie = document.querySelector('#badgeBiometria');
        this.addClassesCss( cardBiometria, badgeBiometrie);
      }
      if( iterator.consulta === "1 Química sanguínea de 6 elementos (Glucosa, Urea, Creatinina, Ácido Úrico, Colesterol y Triglicéidos )"  ){
        this.QS += 1;
        let cardQuimica = document.querySelector('#quimicaSanguineaCard');
        let badgeQuimica = document.querySelector("#badgeQuimica");
        this.addClassesCss( cardQuimica, badgeQuimica);
      }
      if( iterator.consulta === "2 Exámen General de Orina"  ){
        this.EGO += 1;
        let egoCard = document.querySelector('#egoCard');
        let badgeEgo = document.querySelector("#badegeEgo1");
        this.addClassesCss( egoCard, badgeEgo);

      }
      if(iterator.consulta === "1 V.D.R.L"){
        this.VDRL += 1;
        let cardVrl = document.querySelector('#VrlCard');
        let badgeVrl = document.querySelector('#badgeVrl');
        this.addClassesCss( cardVrl, badgeVrl);
      }
      if( iterator.consulta === "1 Grupo Sanguíneo"  ){
        this.GS += 1;
        let grupoSanguineo = document.querySelector("#grupoSanguineoCard");
        let badgeSanguineo = document.querySelector("#badgeGrupoSanguineo");
        this.addClassesCss(grupoSanguineo, badgeSanguineo)
      }
      if( iterator.consulta === "1 V.I.H." ){
        this.VIH += 1;
        let vihcard = document.querySelector('#vihcard');
        let vihbadge = document.querySelector('#vihbadge');
        this.addClassesCss( vihcard, vihbadge);
      }
      if(  iterator.consulta === "1 Tiempos de Coagulación" ){
        this.TC += 1;
        let tiemposCoagulacionCard = document.querySelector("#tiemposCoagulacionCard");
        let  tiemposCoagulacionBadge = document.querySelector('#tiemposCoagulacionBadge');
        this.addClassesCss( tiemposCoagulacionCard, tiemposCoagulacionBadge);
      }
      if(  iterator.consulta === "1 Curva de tolerancia a la Glucosa"  ){
        this.CTG += 1;
        let curvaDetoleranciaCard = document.querySelector("#curvaDetoleranciaCard");
        let curvaDetoleranciaBadge = document.querySelector("#curvaDetoleranciaBadge");
        this.addClassesCss( curvaDetoleranciaCard, curvaDetoleranciaBadge);
      }
      if(  iterator.consulta === "1 Exudado Vaginal"  ){
        this.EV += 1;
        let curvaDetoleranciaCard = document.querySelector("#ExudadoVagCard");
        let curvaDetoleranciaBadge = document.querySelector("#badegeEV");
        this.addClassesCss( curvaDetoleranciaCard, curvaDetoleranciaBadge);
      }
      if(  iterator.consulta === "1 Urocultivo"  ){
        this.URO += 1;
        let curvaDetoleranciaCard = document.querySelector("#uroCard");
        let curvaDetoleranciaBadge = document.querySelector("#badegeURO");
        this.addClassesCss( curvaDetoleranciaCard, curvaDetoleranciaBadge);
      }
    };
   //   //  termina el codigo que valida los examenes 
  }

  showMessage(examen: string){
    this.examenes.forEach(  (estudio:consultas, index) => {
      // console.log( estudio.consulta  === examen  );
      // console.log( estudio  );
        if( estudio.consulta ===  examen  ){
          // console.log(estudio.consulta, index);
           Swal.fire({
              icon: 'success',
              title: '',
              text: `Tipo:  ${this.examenes[index].consulta}\n  Fecha: ${this.examenes[index].fecha} \n Hora: ${this.examenes[index].hora}\n   Médico ${this.examenes[index].medico}\n  Atendió: ${this.examenes[index].firma}`,
            })
        }
    });
  }

  addClassesCss( card: Element, badge:Element){
    card.classList.add('border-primary');
    if( badge.id == "badegeEgo1"  ){
      badge.innerHTML = this.EGO.toString(); 
    }else if(badge.id == 'badgeQuimica'){
      badge.innerHTML = this.QS.toString();
    }else if(badge.id == 'badgeVrl'){
      badge.innerHTML = this.VDRL.toString();
    }else if(badge.id == 'badgeGrupoSanguineo'){
      badge.innerHTML = this.GS.toString();
    }else if(badge.id == 'vihbadge'){
      badge.innerHTML = this.VIH.toString();
    }else if(badge.id == 'tiemposCoagulacionBadge'){
      badge.innerHTML = this.TC.toString();
    }else if(badge.id == 'curvaDetoleranciaBadge'){
      badge.innerHTML = this.CTG.toString();
    }else if(badge.id == 'curvaDetoleranciaBadge'){
      badge.innerHTML = this.CTG.toString();
    }else if(badge.id == 'badegeEV'){
      badge.innerHTML = this.EV.toString();
    }else if(badge.id == 'badegeURO'){
      badge.innerHTML = this.URO.toString();
    }else if(badge.id == 'badgeObstetri'){
      badge.innerHTML = this.OBSTETRI.toString();
    }else if(badge.id == 'badgeEstructural'){
      badge.innerHTML = this.ESTRUC.toString();
    }else if(badge.id == 'badegePediatria'){
      badge.innerHTML = this.PEDI.toString();
    }else if(badge.id == 'badegeGineco'){
      badge.innerHTML = this.GINE.toString();
    }else if(badge.id == 'badgeBiometria'){
      badge.innerHTML = this.BHC.toString();
    }else if(badge.id == 'badgeParto'){
      badge.innerHTML = this.parto.toString();
    }else if(badge.id == 'badgeHospitalzacion'){
      badge.innerHTML = this.hospi.toString();
    }
    badge.classList.add('badge-primary');
  } 

  mostrarHospitalizacion(){
    /* console.log( this.hospitalizacion ); */
     Swal.fire({
              icon: 'success',
              title: '',
              text: `Tipo:  ${this.hospitalizacion.consulta}\n  Fecha: ${this.hospitalizacion.fecha} \n Hora: ${this.hospitalizacion.hora}\n   Médico ${this.hospitalizacion.medico}\n  Atendió: ${this.hospitalizacion.firma}`,
            })
  }

  cambiarVisita( selectCon ){
    let nombreConsultaSelect = selectCon.value ;
    if(this.consultas.tipo  === 'examenesLab' ){
      // let log = console.log;
      // checar si nos se han consumido los laboratorios
      this.examenes.forEach((examen:consultas) =>  {
          if( nombreConsultaSelect == examen.consulta && examen.consulta == nombreConsultaSelect ){
             Swal.fire({
              icon: 'error',
              title: '',
              text: 'ESTE ELEMENTO HA YA SIDO UTILIZADO',
            }) 
            // console.log("Biometria hematica");
            this.btnAgregaConsultas = true;
          }
      });
    }
    
    this.citas.forEach( element => {
      if( nombreConsultaSelect ===  element.consulta && element.consulta === "Hospitalicación en habitación estándar"  ){
                     Swal.fire({
              icon: 'error',
              title: '',
              text: 'ESTE ELEMENTO HA YA SIDO UTILIZADO',
            })
        this.btnAgregaConsultas = true;
      }else if(nombreConsultaSelect ===  element.consulta && element.consulta === "Consulta despues del parto o Cesárea con Medicina General"){
        this.btnAgregaConsultas = true;
                   Swal.fire({
              icon: 'error',
              title: '',
              text: 'ESTE ELEMENTO HA YA SIDO UTILIZADO',
            }) 
      }
      this.btnAgregaConsultas = false;
      if(  nombreConsultaSelect  != "Hospitalicación en habitación estándar" && nombreConsultaSelect  != "Consulta despues del parto o Cesárea con Medicina General" && nombreConsultaSelect != "1 Biometría hemática completa" && nombreConsultaSelect != "Quimica sanguinea de 6 elementos (Glucosa, Urea, Creatinina, Ácido Úrico, Colesterol y Trigliceridos )"  && nombreConsultaSelect != "1 V.D.R.L" && nombreConsultaSelect != "1 V.I.H." && nombreConsultaSelect != "1 Grupo Sanguineo" && nombreConsultaSelect != "1 Tiempos de Coagulación"  ){
        this.btnAgregaConsultas = false;        
      }
      if( this.egoCount == 2  ){
        this.btnAgregaConsultas = false;
      }
    }) 
  }

  //insercion
  seleccion($event, value){
    switch (value) {
      case 'visitas':
        this.concepto=[]
        for(let item of this.paquete ){
          for(let item2 of item.paquete.CitasIncluidas){
            this.concepto.push(item2)
          }
          /* console.log(this.concepto) */
        }  
        this.consultas.consulta = ''
      break;

      case 'examenesLab':
        this.concepto=[]
        for(let item of this.paquete ){
          for(let item2 of item.paquete.examenesLaboratorio){
            this.concepto.push(item2)
          }
          /* console.log(this.concepto) */
        }  
        this.consultas.consulta = ''
      break;

      default:
        break;
    }
  }

  agregarConsulta(){

    let consulta =  { tipo: '', consulta: '', fecha: '', hora:'' , medico: '', firma: '' };

    this.consultas.fecha = this.fechaConsulta;
    this.consultas.hora = this.horaIngreso;
    this.consultas.firma = JSON.parse(localStorage.getItem('usuario')).nombre;
    //  console.log(this.consultas); 
    
     
     if(this.consultas.tipo == '' || this.consultas.consulta == '' || this.consultas.medico == '' || this.consultas.firma == ''){
       Swal.fire({
         icon: 'error',
         title: '',
         text: 'INGRESA LOS DATOS REQUERIDOS',
        }) 
        
      }else{

        if(this.consultas.tipo == 'visitas'){
          
          if(this.consultas.consulta == '7 Consultas con especialista en Ginecología y obstetricia'){
            let valu = true;
            valu = this.validacionescitas();
            
            if(valu){
              
              consulta = this.consultas;
              this.infoVenta.estudios.push( this.consultas );
              console.log(this.infoVenta.estudios);
              
              this.setDatesVenta(this.consultas.medico);

              /* this._paquete.agregarConsulta(this.consultas,this.consultas.tipo,this.id).subscribe(
              (data)=>{
                // console.log(data);
                //swal('Consulta Agregada', 'Puede ver las visitas en la tabla', 'success')
                // console.log(data)
                this.mostrarConsultas();
                this._pagoServicios.agregarPedido( this.infoVenta )
                .subscribe( (data) => {
                  // console.log( data );
                  if(  data['ok'] ){
                    
                    this.generarTicket(data['folio']);
                      // se crea la tabla de las ventas 
                      this.obtenerPaquete();
                       Swal.fire({
                        icon: 'success',
                        title: '',
                        text: 'LA CONSULTA SE AGREGO CORRECTAMENTE',
                      })
                      // seteamos las fechas 
                        eliminarStorage();
                        
                        
                        const eliminarPaciente = new PacienteStorage();
                        eliminarPaciente.removePacienteStorage(); 
                        this.consultas = { tipo: '', consulta: '', fecha: '', medico: '', firma:'' } 
                        this._route.navigateByUrl('consulta'); 
                    }
                });
              }) */
              this.consultas = { tipo: '', consulta: '', fecha: '', medico: '', firma:'' }
          }else{
             Swal.fire({
              icon: 'error',
              title: '',
              text: 'SE TERMINARON LAS CONSULTAS DEL PAQUETE',
            })
            this.consultas = { tipo: '', consulta: '', fecha: '', medico: '', firma:'' }
          }
        }else if(this.consultas.consulta == '1 consulta con pediatría antes de su parto o cesárea'){
          let valu = true;
          valu = this.validacionescitas();
          if(valu){

            consulta = this.consultas;
            // console.log( consulta )
            // this.carritoItems.push( consulta );
            /* this.setDatesVenta(this.consultas.medico); */
            this.infoVenta.estudios.push(consulta);
            // this._paquete.agregarConsulta(this.consultas,this.consultas.tipo,this.id).subscribe(
            //   (data)=>{
            //     //swal('Consulta Agregada', 'Puede ver las visitas en la tabla', 'success')
            //     this.mostrarConsultas();
            //     this._pagoServicios.agregarPedido( this.infoVenta )
            //     .subscribe( (data) => {
            //       // console.log( data );
            //       if(  data['ok'] ){
                    
            //         this.generarTicket(data['folio']);
            //           // se crea la tabla de las ventas 
            //           this.obtenerPaquete();
            //            Swal.fire({
            //             icon: 'success',
            //             title: '',
            //             text: 'LA CONSULTA SE AGREGO CORRECTAMENTE',
            //           })
            //           // ESTA INFORMCAIÓN SE ENVIA A LOS DIFERENTES SERVICIOS 
            //           // seteamos las fechas 
            //             eliminarStorage();
                        
                        
            //             const eliminarPaciente = new PacienteStorage();
            //             eliminarPaciente.removePacienteStorage(); 
            //             this.consultas = { tipo: '', consulta: '', fecha: '', medico: '', firma:'' } 
            //             this._route.navigateByUrl('consulta'); 
            //         }
            //     });
            //   });
            this.consultas = { tipo: '', consulta: '', fecha: '', medico: '', firma:'' }
          }else{
             Swal.fire({
              icon: 'error',
              title: '',
              text: 'SE TERMINARON LAS CONSULTAS DEL PAQUETE',
            })
            this.consultas = { tipo: '', consulta: '', fecha: '', medico: '', firma:'' }
          }
          
        }else if(this.consultas.consulta == '2 Ultrasonidos obstétricos convencionales'){
          let val = true;
          val = this.validacionescitas();
          /* console.log(val); */
        
          if(val){
            
            consulta = this.consultas;
            // console.log( consulta )
            // this.carritoItems.push( consulta );
            this.infoVenta.estudios.push( consulta );
            // this._paquete.agregarConsulta(this.consultas,this.consultas.tipo,this.id).subscribe(
            //   (data)=>{
            //     this.setDatesVenta(this.consultas.medico);          
            //     this.pagar_consulta(this.consultas.consulta, this.consultas.medico);
            //     this.consultas = { tipo: '', consulta: '', fecha: '', medico: '', firma:'' }
            //   })
            this.consultas = { tipo: '', consulta: '', fecha: '', medico: '', firma:'' }
          }else{
             Swal.fire({
              icon: 'error',
              title: '',
              text: 'SE TERMINARON LOS ULTRASONIDOS DEL PAQUETE',
            })
            this.consultas = { tipo: '', consulta: '', fecha: '', medico: '', firma:'' }
          }
          
        }else if(this.consultas.consulta == '1 Ultrasonido estructural'){
          let val = true;
          val = this.validacionescitas();
          /* console.log(val); */
        
        
          if(val){
            
            consulta = this.consultas;

            this.infoVenta.estudios.push(consulta);
            
            /* this._paquete.agregarConsulta(this.consultas,this.consultas.tipo,this.id).subscribe(
              (data)=>{
                this.setDatesVenta(this.consultas.medico);          
                this.pagar_consulta(this.consultas.consulta, this.consultas.medico);
                this.consultas = { tipo: '', consulta: '', fecha: '', medico: '', firma:'' }
              }) */
              this.consultas = { tipo: '', consulta: '', fecha: '', medico: '', firma:'' }
          }else{
             Swal.fire({
              icon: 'error',
              title: '',
              text: 'SE TERMINARON OS ULTRASONIDOS DEL PAQUETE',
            })
            this.consultas = { tipo: '', consulta: '', fecha: '', medico: '', firma:'' }
          }
          
        }else if(this.consultas.consulta == 'Cita abierta a urgencias desde la contratación del servicio hasta la atención del parto o cesárea por medicina general'){
         
          consulta = this.consultas;
          // console.log( consulta )
          // this.carritoItems.push( consulta );
          this.infoVenta.estudios.push( consulta );
          // console.log(  this.carritoItems );
          // this._paquete.agregarConsulta(this.consultas,this.consultas.tipo,this.id).subscribe(
          //   (data)=>{
          //     //swal('Consulta Agregada', 'Puede ver las visitas en la tabla', 'success')
          //     this.mostrarConsultas();
          //     this.setDatesVenta(this.consultas.medico);
          //     this._pagoServicios.agregarPedido( this.infoVenta )
          //     .subscribe( (data) => {
          //       // console.log( data );
          //       if(  data['ok'] ){
                  
          //         this.generarTicket(data['folio']);
          //           // se crea la tabla de las ventas 
          //           this.obtenerPaquete();
          //            Swal.fire({
          //             icon: 'success',
          //             title: '',
          //             text: 'LA CONSULTA SE AGREGO CORRECTAMENTE',
          //           })
          //           // ESTA INFORMCAIÓN SE ENVIA A LOS DIFERENTES SERVICIOS 
          //           // seteamos las fechas 
          //             eliminarStorage();
                      
                      
          //             const eliminarPaciente = new PacienteStorage();
          //             eliminarPaciente.removePacienteStorage(); 
          //             this.consultas = { tipo: '', consulta: '', fecha: '', medico: '', firma:'' } 
          //             this._route.navigateByUrl('consulta'); 
          //         }
          //     });
          //   })
          this.consultas = { tipo: '', consulta: '', fecha: '', medico: '', firma:'' }
        }else if(this.consultas.consulta == 'Consulta después del parto o cesárea con medicina general'){

          // this.carritoItems.push( this.consultas );
          consulta.tipo = this.consultas;
          // console.log( consulta )
          // this.carritoItems.push( consulta );
          this.infoVenta.estudios.push( consulta )
            this.setDatesVenta(this.consultas.medico);
          // this._paquete.agregarConsulta(this.consultas,this.consultas.tipo,this.id).subscribe(
          //   (data)=>{
          //     //swal('Consulta Agregada', 'Puede ver las visitas en la tabla', 'success')
          //     this.mostrarConsultas();
          //     this._pagoServicios.agregarPedido( this.infoVenta )
          //     .subscribe( (data) => {
          //       // console.log( data );
          //       if(  data['ok'] ){
                  
          //         this.generarTicket(data['folio']);
          //           // se crea la tabla de las ventas
          //           if(this.paquete.length == 0){

          //             this.actualizarEstadoMembresia()
          //           } 
          //           this.obtenerPaquete();
          //                                Swal.fire({
          //             icon: 'success',
          //             title: '',
          //             text: 'LA CONSULTA SE AGREGO CORRECTAMENTE',
          //           })
          //           // ESTA INFORMCAIÓN SE ENVIA A LOS DIFERENTES SERVICIOS 
          //           // seteamos las fechas 
          //             eliminarStorage();
                      
                      
          //             const eliminarPaciente = new PacienteStorage();
          //             eliminarPaciente.removePacienteStorage(); 
          //             this.consultas = { tipo: '', consulta: '', fecha: '', medico: '', firma:'' } 
          //             this._route.navigateByUrl('consulta'); 
          //         }
          //     });
          //   })

        }
        this.consultas = { tipo: '', consulta: '', fecha: '', medico: '', firma:'' }
        
      }else{
        let val = true;
        val = this.validaciones();
        /* console.log(val); */
        
        if(val){
          consulta = this.consultas;

          // this.carritoItems.push( consulta );
          this.infoVenta.estudios.push(consulta);
          this._paquete.agregarConsulta(this.consultas,this.consultas.tipo,this.id).subscribe(
            (data)=>{
              this.setDatesVenta(this.consultas.medico);          
              this.pagar_consulta(this.consultas.consulta, this.consultas.medico);
              this.consultas = { tipo: '', consulta: '', fecha: '', medico: '', firma:'' }
            })
        }else{
                     Swal.fire({
                      icon: 'error',
                      title: '',
                      text: 'SE TERMINARON LOS LABORATORIOS DEL PAQUETE',
                    })
            this.consultas = { tipo: '', consulta: '', fecha: '', medico: '', firma:'' }
        }
      }

        //citas   
    }

  }

  actualizarEstadoMembresia(){
    const body = {
      membresiaActiva: false
    }
    this._paquete.actualizarEstadoMembresia( this.infoVenta.paciente, body  )
    .subscribe( data => {/* console.log(data) */}  );
  }

  validacionescitas(){
    this.contadorcitas=0;
    switch(this.consultas.consulta){
      case '7 Consultas con especialista en Ginecología y obstetricia':
        for (const iterator of this.citas) {
            if(iterator.consulta == '7 Consultas con especialista en Ginecología y obstetricia'){
              this.contadorcitas += 1
            }
          }  
          if(this.contadorcitas < 7){
            return true;
          }else{
            return false;
          }
        break;
      case '1 consulta con pediatría antes de su parto o cesárea':
        for (const iterator of this.citas) {
          if(iterator.consulta == '1 consulta con pediatría antes de su parto o cesárea'){
            this.contadorcitas += 1
          }
          /* console.log(this.contadorcitas); */
          }  
          if(this.contadorcitas < 1){
            return true;
          }else{
            return false;
          }
        break;
      case '2 Ultrasonidos obstétricos convencionales':
          for (const iterator of this.citas) {
            if(iterator.consulta == '2 Ultrasonidos obstétricos convencionales'){
              this.contadorcitas += 1
            }
            /* console.log(this.contadorcitas); */
            }  
            if(this.contadorcitas < 2){
              return true;
            }else{
              return false;
            }
          break;
      case '1 Ultrasonido estructural':
        for (const iterator of this.citas) {
          if(iterator.consulta == '1 Ultrasonido estructural'){
            this.contadorcitas += 1
          }
          /* console.log(this.contadorcitas); */
          }  
          if(this.contadorcitas < 1){
            return true;
          }else{
            return false;
          }
        break;
      case 'Hospitalización en habitación estándar':
        for (const iterator of this.citas) {
          if(iterator.consulta == 'Hospitalización en habitación estándar'){
            this.contadorcitas += 1
          }
          }  
          if(this.contadorcitas < 1){
            return true;
          }else{
            return false;
          }
        break;
    }
  }

  validaciones(){
    this.contador=0
    /* console.log(this.examenes); */
    switch(this.consultas.consulta){      
      case '2 Biometría hemática completa':
        for (const iterator of this.examenes) {
          if(iterator.consulta == '2 Biometría hemática completa'){
            this.contador= this.contador + 1
          }
        }
        if(this.contador < 2){
          return true;
        }else{
          return false;
        }
        break;
      case '1 Química sanguínea de 6 elementos (Glucosa, Urea, Creatinina, Ácido Úrico, Colesterol y Triglicéidos )':
          for (const iterator of this.examenes) {
            if(iterator.consulta == '1 Química sanguínea de 6 elementos (Glucosa, Urea, Creatinina, Ácido Úrico, Colesterol y Triglicéidos )'){
              this.contador= this.contador + 1
            }
          }
          /* console.log('asghasgdh'); */
          
          if(this.contador < 1){
            return true;
          }else{
            return false;
          }
        break;
      case '2 Exámen General de Orina':
          for (const iterator of this.examenes) {
            if(iterator.consulta == '2 Exámen General de Orina'){
              this.contador= this.contador + 1
            }
          }
          /* console.log(this.contador); */
          
          if(this.contador < 2){
            return true;
          }else{
            return false;
          }
        break;
      case '1 V.D.R.L':
          for (const iterator of this.examenes) {
            if(iterator.consulta == '1 V.D.R.L'){
              this.contador= this.contador + 1
            }
          }
          if(this.contador < 1){
            return true;
          }else{
            return false;
          }
        break;
      case '1 V.I.H.':
          for (const iterator of this.examenes) {
            if(iterator.consulta == '1 V.I.H.'){
              this.contador= this.contador + 1
            }
          }
          if(this.contador < 1){
            return true;
          }else{
            return false;
          }
        break;
      case '1 Grupo Sanguíneo':
          for (const iterator of this.examenes) {
            if(iterator.consulta == '1 Grupo Sanguíneo'){
              this.contador= this.contador + 1
            }
          }
          if(this.contador < 1){
            return true;
          }else{
            return false;
          }
        break;
      case '1 Tiempos de Coagulación':
          for (const iterator of this.examenes) {
            if(iterator.consulta == '1 Tiempos de Coagulación'){
              this.contador= this.contador + 1
            }
          }
          if(this.contador < 1){
            return true;
          }else{
            return false;
          }
        break;
        case '1 Curva de tolerancia a la Glucosa':
          for (const iterator of this.examenes) {
            if(iterator.consulta == '1 Curva de tolerancia a la Glucosa'){
              this.contador= this.contador + 1
            }
          }
          if(this.contador < 1){
            return true;
          }else{
            return false;
          }
        break;
        case '1 Exudado Vaginal':
          for (const iterator of this.examenes) {
            if(iterator.consulta == '1 Exudado Vaginal'){
              this.contador= this.contador + 1
            }
          }
          if(this.contador < 1){
            return true;
          }else{
            return false;
          }
          
        break;
        case '1 Urocultivo':
          for (const iterator of this.examenes) {
            if(iterator.consulta == '1 Urocultivo'){
              this.contador= this.contador + 1
            }
          }
          if(this.contador < 1){
            return true;
          }else{
            return false;
          }
        break;
    }
  }

  //metodo 
  setDatesVenta(medico){

    if(this.consultas.consulta == '7 Consultas con especialista en Ginecología y obstetricia'){
      const item = {
        name:this.nombrePaquete,
        nombreEstudio: "CONSULTA GINECOLOGIA",
        precioCon:0,
        precioSin:0,
        _id:'5fdd2b41a71eca0017a8e936'
      }
      this.carrito.items.push(item);
      
      const carrito= new Carrito();
      carrito.carritoPaquete( item );

    }else if( this.consultas.consulta == '1 consulta con pediatría antes de su parto o cesárea'){
      const item = {
        name:this.nombrePaquete,
        nombreEstudio: "CONSULTA PEDIATRIA",
        precioCon:0,
        precioSin:0,
        _id:'5fdd2bfba71eca0017a8e939'
      }
      this.carrito.items.push(item);
      const carrito= new Carrito();
      carrito.carritoPaquete( item );
    }
    /* else if( this.consultas.consulta == 'Hospitalización en habitación estándar'){
      const item = {
        nombreEstudio: "Hospitalización en habitación estándar",
        precioCon:0,
        precioSin:0,
        _id:'5fdd2bfba71eca0017a8e939'
      }
      const item2 = {
        nombreEstudio: "Medicamento y material de curación durante la hospitalización",
        precioCon:0,
        precioSin:0,
        _id:'5fdd2bfba71eca0017a8e939'
      }
      const item3 = {
        nombreEstudio: "Atención del parto o cesárea por especialista en ginecología y obstetricia, anestesia y pediatría",
        precioCon:0,
        precioSin:0,
        _id:'5fdd2bfba71eca0017a8e939'
      }
      const item4 = {
        nombreEstudio: "Atención del recién nacido durante el parto o cesárea por el especialista en pediatría",
        precioCon:0,
        precioSin:0,
        _id:'5fdd2bfba71eca0017a8e939'
      }
      const item5 = {
        nombreEstudio: "Estancia del recién nacido en la habitación de la madre",
        precioCon:0,
        precioSin:0,
        _id:'5fdd2bfba71eca0017a8e939'
      }
      this.carrito.items.push(item);
    } */
    // else if ( this.consultas.consulta === "" ){
    //   const item = {
    //     name:this.nombrePaquete,
    //     nombreEstudio: "CONSULTA DE MEDICO GENERAL",
    //     precioCon:0,
    //     precioSin:0,
    //     _id:'5fd3ebca08ccbb0017712f0d'
    //   }
    //   this.carrito.items.push(item);
    //   const carrito= new Carrito();
    //   carrito.carritoPaquete( item );
    // }
    
    /* const dates = new Dates();
    //this.infoVenta.totalCompra = this.carrito.totalSin;
    this.fecha = moment().format('l');    
    this.infoVenta.hora = moment().format('LT');
    this.infoVenta.vendedor = getDataStorage()._id;
    if(this.pacienteInfo.id == undefined){
      this.infoVenta.paciente = this.pacienteInfo._id;
    }else{
      this.infoVenta.paciente = this.pacienteInfo.id;
    }
    this.infoVenta.sede = CEDE;
    this.infoVenta.prioridad = "Programado"
    // this.infoVenta.fecha = dates.getDate();
    this.infoVenta.doctorQueSolicito = medico;
    this.infoVenta.estudios= this.carrito.items
    this.infoVenta.totalCompra = 0;  */   
  }

  validarPago(semana, semana2, pago, pagototal){
    /* console.log(pago);
    console.log(pagototal); */
    
    if(semana == semana2){
      if(pago < pagototal.toFixed(2)){
        this.valPago = false;
        return this.valPago
      }else{
        this.valPago = true;
        return this.valPago
      }
    }else{
      this.valPago = true;
      return this.valPago
    }
  }

  agregarPago(){
    if(this.pagos.tipo == '' || this.pagos.pago == '' || this.pagos.semanaGesta == '' || this.pagos.semanapago == ''){
                          Swal.fire({
                      icon: 'error',
                      title: '',
                      text: 'INGRESA LOS DATOS REQUERIDOS',
                    })
     }else{
      let semanapagos2 = (39 - this.sem[0].semanaGesta)
      if(this.pagos.semanapago > semanapagos2 ){
                 Swal.fire({
                      icon: 'success',
                      title: '',
                      text: 'YA NO SE PUEDE REALIZAR MAS PAGOS',
                    })
      }else{
        let vali;
        this.pagos.pago = parseFloat(this.pagos.pago);
        let totalpago = 17500 - this.totalpagos  
        vali = this.validarPago(this.pagos.semanapago, semanapagos2, this.pagos.pago, totalpago);        
        if(this.pagos.pago > parseFloat(totalpago.toFixed(2)) || vali == false){
         Swal.fire({
                      icon: 'success',
                      title: '',
                      text: 'EL PAGO NO DEBE SER DIFERENTE AL RESTANTE',
                    })
          this.pagos = { semanaGesta:'', semanapago: '', pago: ''}
            this.carrito={
              totalSin: 0,
              totalCon: 0,
              items: []
            };
            this.checked = {
              incluye: false,
              noincluye: false
            }
            this.sem = [];
            this.semanapago=[];
            this.acumuladoPaque = 0
            this.obtenerPaquete();
        }else{
          /* console.log(this.pagos);
          console.log(this.id); */
          
          this._paquete.agregarConsulta(this.pagos,'pagos',this.id).subscribe((data)=>{
            this.carrito={
              totalSin: parseFloat(this.pagos.pago),
              totalCon: parseFloat(this.pagos.pago),
              items: []
            };
            
            const item8 = {
              name:this.nombrePaquete,
              nombreEstudio: "Pago semana"+this.pagos.semanapago,
              precioCon:parseFloat(this.pagos.pago),
              precioSin:parseFloat(this.pagos.pago),
            }
            if(this.acumuladoPaque == 0){
              if(this.paqueteAcumulado.acumulado == 0){
                this.paqueteAcumulado.acumulado = 0
                this._paquete.actualizarAdelanto(this.id,this.paqueteAcumulado).subscribe((resp:any)=> {
                })
              }else{
                this.paqueteAcumulado.acumulado = parseFloat(this.paqueteAcumulado.acumulado.toFixed(2))
                this._paquete.actualizarAdelanto(this.id,this.paqueteAcumulado).subscribe((resp:any)=> {
                })
              } 
              
            } else{ 
              this.paqueteAcumulado.acumulado = parseFloat(this.paqueteAcumulado.acumulado.toFixed(2))
              this.paqueteAcumulado.acumulado= parseFloat(this.paqueteAcumulado.acumulado.toFixed(2))             
              this._paquete.actualizarAdelanto(this.id,this.paqueteAcumulado).subscribe((resp:any)=> {
              })
            }
            this.carrito.items.push(item8);
            this.infoVenta = {  
              paciente:this.pacienteInfo._id,
              nombrePaciente:this.pacienteInfo.nombrePaciente,
              vendedor:JSON.parse(localStorage.getItem('usuario'))._id,
              hora:moment().format('LT'),
              estudios:this.carrito.items,
              efectivo:true,
              doctorQueSolicito:"",
              transferencia: false,
              tarjetCredito:false,
              tarjetaDebito:false,
              montoEfectivo:this.pagos.pago,
              montoTarjteCredito:0,
              montoTarjetaDebito:0,
              montoTranferencia: 0,
              sede:this.sede,
              totalCompra:this.pagos.pago,
              prioridad:"",
              status:'Pagado',
              compraConMembresia:this.pacienteInfo.membresiaActiva
            }
            
            this._pagoServicios.agregarPedido(this.infoVenta).subscribe((resp:any)=>{
              if(resp.ok){
                this.generarTicket(resp['folio']);
                this.carrito={
                  totalSin: 0,
                  totalCon: 0,
                  items: []
                };
                this.paquete=[];
                this.pagos = { semanaGesta:'', semanapago: '', pago: ''}
                /* this.carrito={
                  totalSin: 0,
                  totalCon: 0,
                  items: []
                }; */
                this.checked = {
                  incluye: false,
                  noincluye: false
                }
                this.sem = [];
                this.semanapago=[];
                this.acumuladoPaque = 0
                this.obtenerPaquete();
                /* swal('Pago Agregada', 'se agrego el pago', 'success'); */
                    Swal.fire({
                      icon: 'success',
                      title: '',
                      text: 'EL PAGO SE REALIZO CORRECTAMENTE',
                    })
               }
            })
            /* this.folios += 1
            this.generarTicket(this.folios); */
          })
          // localStorage.removeItem('folio');
        }
      }
      this.checked = {
        incluye: false,
        noincluye: false
      }
      this.sem = [];
      this.semanapago=[];
      this.acumuladoPaque = 0
      this.limpiarradio();
      this.deposito=0;
      this.sem = [];
      }
      // localStorage.setItem('folio',JSON.stringify(this.folios));
  }

  setDatesPedidos (){
    // this.pedidosLaboratorios.fecha = moment().format('l');
    const datesPedidoLab = new Dates();
    // configuracion de los pedidos de laboratorio
    this.pedidosLaboratorios.fecha = datesPedidoLab.getDate();
    this.pedidosLaboratorios.hora = moment().format('LT');
    this.pedidosLaboratorios.medicoQueSolicita = this.infoVenta.doctorQueSolicito;
    this.pedidosLaboratorios.sede = this.sede;
    this.pedidosLaboratorios.idPaciente = this.infoVenta.paciente;

    // configuracion de los pedidos de ultrasonido
    this.pedidosUltrasonido.idPaciente = this.infoVenta.paciente;
    this.pedidosUltrasonido.fecha = datesPedidoLab.getDate();
    this.pedidosUltrasonido.sede = this.sede;
  }

  pagar_consulta(nombre, medico){
    /* localStorage.removeItem('carrito') */
    const dates = new Dates();
    /* console.log(nombre); */
    
    const carrito= new Carrito();
    switch (nombre) {
      
      case '2 Ultrasonidos obstétricos convencionales':
           
            const ite = {
              name:this.nombrePaquete,
              nombreEstudio: "ULTRASONIDO OBSTETRICO CONVENCIONAL",
              precioCon:0,
              precioSin:0,
              idEstudio:'5fd280031cb0ea0017f57b87'
            }
           
            this.carrito.items.push(ite); 
            carrito.carritoPaquete( ite );


            /* console.log(this.infoVenta); */
            

            // this._pagoServicios.agregarPedido( this.infoVenta )
            // .subscribe( (data) => {
            //   // console.log( data );
            //   if(  data['ok'] ){
                
            //     this.generarTicket(data['folio']);
            //       // se crea la tabla de las ventas 
            //       this.setDatesPedidos();
            //       this.pedidosUltrasonido.estudios.push(ite)

            //       this._pagoServicios.postPedidosUltra( this.pedidosUltrasonido ).subscribe( data => {/* console.log( data ) */});
            //       Swal.fire({
            //           icon: 'success',
            //           title: '',
            //           text: 'LA CONSULTA SE AGREGO CORRECTAMENTE',
            //         })
            //       this.pedidosUltrasonido = {
            //         idPaciente:"", 
            //         estudios:[],
            //         fecha:"",
            //         hora:"",
            //         medicoQueSolicita:"",
            //         sede:"",
            //         prioridad:"Programado"
            //       }
            //       // ESTA INFORMCAIÓN SE ENVIA A LOS DIFERENTES SERVICIOS 
            //       // seteamos las fechas 
            //         eliminarStorage();
            //         /* const eliminarPaciente = new PacienteStorage();
            //         eliminarPaciente.removePacienteStorage();*/ 
            //     }
            // });

            this.mostrarConsultas();
      break;
      case '1 Ultrasonido estructural':
    
        
        const ite2 = {
          name:this.nombrePaquete,
          nombreEstudio: "ULTRASONIDO OBSTETRICO ESTRUCTURAL DEL SEGUNDO TRIMESTRE ",
          precioCon:0,
          precioSin:0,
          idEstudio:'5fd280c41cb0ea0017f57b91'
        }

        this.carrito.items.push(ite2);
        carrito.carritoPaquete( ite2 );
        
        /* console.log(this.infoVenta); */
            
            // this._pagoServicios.agregarPedido( this.infoVenta )
            // .subscribe( (data) => {
            //   // console.log( data );
            //   if(  data['ok'] ){
                
            //     this.generarTicket(data['folio']);
            //       // se crea la tabla de las ventas 
            //       this.setDatesPedidos();
            //       this.pedidosUltrasonido.estudios.push(ite2)
            //       this._pagoServicios.postPedidosUltra( this.pedidosUltrasonido ).subscribe( data => {/* console.log( data ) */});
            //                            Swal.fire({
            //           icon: 'success',
            //           title: '',
            //           text: 'LA CONSULTA SE AGREGO CORRECTAMENTE',
            //         })
            //       this.pedidosUltrasonido = {
            //         idPaciente:"", 
            //         estudios:[],
            //         fecha:"",
            //         hora:"",
            //         medicoQueSolicita:"",
            //         sede:"",
            //         prioridad:"Programado"
            //       }
            //       // ESTA INFORMCAIÓN SE ENVIA A LOS DIFERENTES SERVICIOS 
            //       // seteamos las fechas 
            //         eliminarStorage();
            //         /* const eliminarPaciente = new PacienteStorage();
            //         eliminarPaciente.removePacienteStorage();*/ 
            //     }
            // });

            this.mostrarConsultas();
      break;
      case '2 Biometría hemática completa':
            const item = {
              name:this.nombrePaquete,
              nombreEstudio: "BIOMETRIA HEMATICA COMPLETA",
              precioCon:0,
              precioSin:0,
              idEstudio:'5fd284b11cb0ea0017f57bd8'
            }
            this.carrito.items.push(item);
            carrito.carritoPaquete( item );
            /* console.log(this.infoVenta); */
            
            // this._pagoServicios.agregarPedido( this.infoVenta )
            // .subscribe( (data) => {
            //   // console.log( data );
            //   if(  data['ok'] ){
                
            //     this.generarTicket(data['folio']);
            //       // se crea la tabla de las ventas 
            //       this.setDatesPedidos();
            //       this.pedidosLaboratorios.estudios.push(item);
            //       /* console.log(this.pedidosLaboratorios); */
                  
            //       this._pagoServicios.pedidosLaboratorio( this.pedidosLaboratorios ).subscribe( data => {/* console.log( data ) */});
            //                            Swal.fire({
            //           icon: 'success',
            //           title: '',
            //           text: 'LA CONSULTA SE AGREGO CORRECTAMENTE',
            //         })// ESTA INFORMCAIÓN SE ENVIA A LOS DIFERENTES SERVICIOS 
            //       // seteamos las fechas 
            //         this.pedidosLaboratorios = { 
            //           estudios:[],
            //           idPaciente:"",
            //           fecha:"",
            //           hora:"",
            //           medicoQueSolicita:"",
            //           sede:"",
            //           prioridad:"Programado",
            //           estado:""
            //         }
            //         eliminarStorage();
            //         /* const eliminarPaciente = new PacienteStorage();
            //         eliminarPaciente.removePacienteStorage();*/ 
            //     }
            // });

            this.mostrarConsultas();
      break;
      case '1 Química sanguínea de 6 elementos (Glucosa, Urea, Creatinina, Ácido Úrico, Colesterol y Triglicéidos )':
        const item2 = {
          name:this.nombrePaquete,
          nombreEstudio: "QUIMICA SANGUINEA 6",
          precioCon:0,
          precioSin:0,
          idEstudio:'5fd2a4171cb0ea0017f57ddb'
        }
        this.carrito.items.push(item2);

        carrito.carritoPaquete( item2 );
        // this._pagoServicios.agregarPedido( this.infoVenta )
        // .subscribe( (data) => {
        //   // console.log( data );
        //   if(  data['ok'] ){
            
        //     this.generarTicket(data['folio']);
        //       // se crea la tabla de las ventas
        //       this.setDatesPedidos();
        //       this.pedidosLaboratorios.estudios.push(item2);
        //       this._pagoServicios.pedidosLaboratorio( this.pedidosLaboratorios ).subscribe( data => {/* console.log( data ) */}); 
        //                            Swal.fire({
        //               icon: 'success',
        //               title: '',
        //               text: 'LA CONSULTA SE AGREGO CORRECTAMENTE',
        //             })
        //         this.pedidosLaboratorios = { 
        //         estudios:[],
        //         idPaciente:"",
        //         fecha:"",
        //         hora:"",
        //         medicoQueSolicita:"",
        //         sede:"",
        //         prioridad:"Programado",
        //         estado:""
        //       }
        //       // ESTA INFORMCAIÓN SE ENVIA A LOS DIFERENTES SERVICIOS 
        //       // seteamos las fechas 
        //         eliminarStorage();
        //         /* const eliminarPaciente = new PacienteStorage();
        //         eliminarPaciente.removePacienteStorage();  */ 
        //   }
        // });

        this.mostrarConsultas(); 

      break;
      case '2 Exámen General de Orina':
       
      
        const item3 = {
          name:this.nombrePaquete,
          nombreEstudio: "EXAMEN GENERAL DE ORINA ",
          precioCon:0,
          precioSin:0,
          idEstudio:'5fd28c2f1cb0ea0017f57c58'
        }
        this.carrito.items.push(item3);
        carrito.carritoPaquete( item3 );
        // this._pagoServicios.agregarPedido( this.infoVenta )
        // .subscribe( (data) => {
        //   // console.log( data );
        //   if(  data['ok'] ){
            
        //     this.generarTicket(data['folio']);
        //       // se crea la tabla de las ventas 
        //       this.setDatesPedidos();
        //       this.pedidosLaboratorios.estudios.push(item3);
        //       this._pagoServicios.pedidosLaboratorio( this.pedidosLaboratorios ).subscribe( data => {/* console.log( data ) */});
        //                            Swal.fire({
        //               icon: 'success',
        //               title: '',
        //               text: 'LA CONSULTA SE AGREGO CORRECTAMENTE',
        //             })
        //       this.pedidosLaboratorios = { 
        //         estudios:[],
        //         idPaciente:"",
        //         fecha:"",
        //         hora:"",
        //         medicoQueSolicita:"",
        //         sede:"",
        //         prioridad:"Programado",
        //         estado:""
        //       }
        //       // ESTA INFORMCAIÓN SE ENVIA A LOS DIFERENTES SERVICIOS 
        //       // seteamos las fechas 
        //         eliminarStorage();
        //         /* const eliminarPaciente = new PacienteStorage();
        //         eliminarPaciente.removePacienteStorage();  */ 
        //   }
        // });

        this.mostrarConsultas();  
      break;
      case '1 V.D.R.L':

        
        const item4 = {
          name:this.nombrePaquete,
          nombreEstudio: "V.D.R.L.",
          precioCon:0,
          precioSin:0,
          idEstudio:'5fd29b611cb0ea0017f57d51'
        }

        carrito.carritoPaquete( item4 );
        this.carrito.items.push(item4);

        // this._pagoServicios.agregarPedido( this.infoVenta )
        // .subscribe( (data) => {
        //   // console.log( data );
        //   if(  data['ok'] ){
            
        //     this.generarTicket(data['folio']);
        //       // se crea la tabla de las ventas 
        //       this.setDatesPedidos();
        //       this.pedidosLaboratorios.estudios.push(item4);
        //       this._pagoServicios.pedidosLaboratorio( this.pedidosLaboratorios ).subscribe( data => {/* console.log( data ) */});
        //                            Swal.fire({
        //               icon: 'success',
        //               title: '',
        //               text: 'LA CONSULTA SE AGREGO CORRECTAMENTE',
        //             })
        //       this.pedidosLaboratorios = { 
        //         estudios:[],
        //         idPaciente:"",
        //         fecha:"",
        //         hora:"",
        //         medicoQueSolicita:"",
        //         sede:"",
        //         prioridad:"Programado",
        //         estado:""
        //       }
        //       // ESTA INFORMCAIÓN SE ENVIA A LOS DIFERENTES SERVICIOS 
        //       // seteamos las fechas 
        //         eliminarStorage();
        //         /* const eliminarPaciente = new PacienteStorage();
        //         eliminarPaciente.removePacienteStorage();   */
        //     }
        // });
        this.mostrarConsultas();
      break;
      case '1 Grupo Sanguíneo':
        
        const item5 = {
          name:this.nombrePaquete,
          nombreEstudio: "GRUPO Y FACTOR RH",
          precioCon:0,
          precioSin:0,
          idEstudio:'5fd292b31cb0ea0017f57c9e'
        }
        carrito.carritoPaquete( item5 );
        this.carrito.items.push(item5);
        
        // this._pagoServicios.agregarPedido( this.infoVenta )
        // .subscribe( (data) => {
        //   // console.log( data );
        //   if(  data['ok'] ){
            
        //     this.generarTicket(data['folio']);
        //       // se crea la tabla de las ventas
        //       this.setDatesPedidos();
        //       this.pedidosLaboratorios.estudios.push(item5);
        //       this._pagoServicios.pedidosLaboratorio( this.pedidosLaboratorios ).subscribe( data => {/* console.log( data ) */}); 
        //                            Swal.fire({
        //               icon: 'success',
        //               title: '',
        //               text: 'LA CONSULTA SE AGREGO CORRECTAMENTE',
        //             })
        //       this.pedidosLaboratorios = { 
        //         estudios:[],
        //         idPaciente:"",
        //         fecha:"",
        //         hora:"",
        //         medicoQueSolicita:"",
        //         sede:"",
        //         prioridad:"Programado",
        //         estado:""
        //       }
        //       // ESTA INFORMCAIÓN SE ENVIA A LOS DIFERENTES SERVICIOS 
        //       // seteamos las fechas 
        //         eliminarStorage();
                
                
        //         /* const eliminarPaciente = new PacienteStorage();
        //         eliminarPaciente.removePacienteStorage();   */
        //     }
        // });

        this.mostrarConsultas();
      break;
      case '1 V.I.H.':

        
        const item6 = {
          name:this.nombrePaquete,
          nombreEstudio: "H.I.V. (PRUEBA PRESUNTIVA)",
          precioCon:0,
          precioSin:0,
          idEstudio:'5fd293501cb0ea0017f57caa'
        }

        this.carrito.items.push(item6);
        carrito.carritoPaquete( item6 );
        // this._pagoServicios.agregarPedido( this.infoVenta )
        // .subscribe( (data) => {
        //   // console.log( data );
        //   if(  data['ok'] ){
            
        //     this.generarTicket(data['folio']);
        //       // se crea la tabla de las ventas
        //       this.setDatesPedidos();
        //       this.pedidosLaboratorios.estudios.push(item6);
        //       this._pagoServicios.pedidosLaboratorio( this.pedidosLaboratorios ).subscribe( data => {/* console.log( data ) */}); 
        //                            Swal.fire({
        //               icon: 'success',
        //               title: '',
        //               text: 'LA CONSULTA SE AGREGO CORRECTAMENTE',
        //             })
        //       this.pedidosLaboratorios = { 
        //         estudios:[],
        //         idPaciente:"",
        //         fecha:"",
        //         hora:"",
        //         medicoQueSolicita:"",
        //         sede:"",
        //         prioridad:"Programado",
        //         estado:""
        //       }
        //       // ESTA INFORMCAIÓN SE ENVIA A LOS DIFERENTES SERVICIOS 
        //       // seteamos las fechas 
        //         eliminarStorage();
                
                
        //         /* const eliminarPaciente = new PacienteStorage();
        //         eliminarPaciente.removePacienteStorage(); */  
        //     }
        // });

        this.mostrarConsultas();
      break;
      case '1 Tiempos de Coagulación':
        
        const item7 = {
          name:this.nombrePaquete,
          nombreEstudio: "TIEMPO DE COAGULACION  ",
          precioCon:0,
          precioSin:0,
          idEstudio:'5fd29a991cb0ea0017f57d41'
        }
        this.carrito.items.push(item7);
        carrito.carritoPaquete( item7 );

        // this._pagoServicios.agregarPedido( this.infoVenta )
        // .subscribe( (data) => {
        //   // console.log( data );
        //   if(  data['ok'] ){
            
        //     this.generarTicket(data['folio']);
        //       // se crea la tabla de las ventas 
        //       this.setDatesPedidos();
        //       this.pedidosLaboratorios.estudios.push(item7);
        //       this._pagoServicios.pedidosLaboratorio( this.pedidosLaboratorios ).subscribe( data => {/* console.log( data ) */});
        //                            Swal.fire({
        //               icon: 'success',
        //               title: '',
        //               text: 'LA CONSULTA SE AGREGO CORRECTAMENTE',
        //             })
        //       this.pedidosLaboratorios = { 
        //         estudios:[],
        //         idPaciente:"",
        //         fecha:"",
        //         hora:"",
        //         medicoQueSolicita:"",
        //         sede:"",
        //         prioridad:"Programado",
        //         estado:""
        //       }
        //       // ESTA INFORMCAIÓN SE ENVIA A LOS DIFERENTES SERVICIOS 
        //       // seteamos las fechas 
        //         eliminarStorage();
                
                
        //         /* const eliminarPaciente = new PacienteStorage();
        //         eliminarPaciente.removePacienteStorage();   */
        //     }
        // });

        this.mostrarConsultas();
      break;
      case '1 Curva de tolerancia a la Glucosa':
      
        const item8 = {
          name:this.nombrePaquete,
          nombreEstudio: "CURVA  DE   TOLERANCIA  A  LA  GLUCOSA  2 HRS",
          precioCon:0,
          precioSin:0,
            idEstudio:'5fd2a0051cb0ea0017f57d7a'
        }
        
        this.carrito.items.push(item8);
        carrito.carritoPaquete( item8 );
        // this._pagoServicios.agregarPedido( this.infoVenta )
        // .subscribe( (data) => {
        //   // console.log( data );
        //   if(  data['ok'] ){
            
        //     this.generarTicket(data['folio']);
        //       // se crea la tabla de las ventas 
        //       this.setDatesPedidos();
        //       this.pedidosLaboratorios.estudios.push(item8);
        //       this._pagoServicios.pedidosLaboratorio( this.pedidosLaboratorios ).subscribe( data => {/* console.log( data ) */});
        //                            Swal.fire({
        //               icon: 'success',
        //               title: '',
        //               text: 'LA CONSULTA SE AGREGO CORRECTAMENTE',
        //             })
        //       this.pedidosLaboratorios = { 
        //         estudios:[],
        //         idPaciente:"",
        //         fecha:"",
        //         hora:"",
        //         medicoQueSolicita:"",
        //         sede:"",
        //         prioridad:"Programado",
        //         estado:""
        //       }
        //       // ESTA INFORMCAIÓN SE ENVIA A LOS DIFERENTES SERVICIOS 
        //       // seteamos las fechas 
        //         eliminarStorage();
                
                
        //         /* const eliminarPaciente = new PacienteStorage();
        //         eliminarPaciente.removePacienteStorage();   */
        //     }
        // });
        this.mostrarConsultas();
      break;
      case '1 Exudado Vaginal':
        
      const item9 = {
          name:this.nombrePaquete,
          nombreEstudio: "CULTIVO CERVICO VAGINAL",
          precioCon:0,
          precioSin:0,
          idEstudio:'5fd289731cb0ea0017f57c24'
        }
        this.carrito.items.push(item9);
        carrito.carritoPaquete( item9 );
        // this._pagoServicios.agregarPedido( this.infoVenta )
        // .subscribe( (data) => {
        //   // console.log( data );
        //   if(  data['ok'] ){
            
        //     this.generarTicket(data['folio']);
        //       // se crea la tabla de las ventas 
        //       this.setDatesPedidos();
        //       this.pedidosLaboratorios.estudios.push(item9);
        //       this._pagoServicios.pedidosLaboratorio( this.pedidosLaboratorios ).subscribe( data => {/* console.log( data ) */});
        //                            Swal.fire({
        //               icon: 'success',
        //               title: '',
        //               text: 'LA CONSULTA SE AGREGO CORRECTAMENTE',
        //             })
        //       this.pedidosLaboratorios = { 
        //         estudios:[],
        //         idPaciente:"",
        //         fecha:"",
        //         hora:"",
        //         medicoQueSolicita:"",
        //         sede:"",
        //         prioridad:"Programado",
        //         estado:""
        //       }
        //       // ESTA INFORMCAIÓN SE ENVIA A LOS DIFERENTES SERVICIOS 
        //       // seteamos las fechas 
        //         eliminarStorage();
                
                
        //         /* const eliminarPaciente = new PacienteStorage();
        //         eliminarPaciente.removePacienteStorage();   */
        //     }
        // });
        this.mostrarConsultas();
      break;
      case '1 Urocultivo':

        
        const item10 = {
          name:this.nombrePaquete,
          nombreEstudio: "CULTIVO DE ORINA (UROCULTIVO)",
          precioCon:0,
          precioSin:0,
          idEstudio:'5fd28a2b1cb0ea0017f57c32'
        }
        
        this.carrito.items.push(item10);
        carrito.carritoPaquete( item10 );
        // this._pagoServicios.agregarPedido( this.infoVenta )
        // .subscribe( (data) => {
        //   // console.log( data );
        //   if(  data['ok'] ){
            
        //     this.generarTicket(data['folio']);
        //       // se crea la tabla de las ventas 
        //       this.setDatesPedidos();
        //       this.pedidosLaboratorios.estudios.push(item10);
        //       this._pagoServicios.pedidosLaboratorio( this.pedidosLaboratorios ).subscribe( data => {/* console.log( data ) */});
        //                            Swal.fire({
        //               icon: 'success',
        //               title: '',
        //               text: 'LA CONSULTA SE AGREGO CORRECTAMENTE',
        //             })
        //       this.pedidosLaboratorios = { 
        //         estudios:[],
        //         idPaciente:"",
        //         fecha:"",
        //         hora:"",
        //         medicoQueSolicita:"",
        //         sede:"",
        //         prioridad:"Programado",
        //         estado:""
        //       }
        //       // ESTA INFORMCAIÓN SE ENVIA A LOS DIFERENTES SERVICIOS 
        //       // seteamos las fechas 
        //         eliminarStorage();
                
                
        //         /* const eliminarPaciente = new PacienteStorage();
        //         eliminarPaciente.removePacienteStorage();   */
        //     }
        // });

        this.mostrarConsultas();
      break;
      default:
        break;
    }
  }

  mostrarConsultas(){
    this.paquete = [];
    this.obtenerPaquete();
  }
  
  generarTicket(folio){
    const tickets = new Tickets();
    /* console.log(this.carrito); */
    
    tickets.printTicketSinMembresia( this.pacienteInfo, this.carrito ,  this.infoVenta, folio );
  
  }
  mostrarConsultasDespuesParto(){
                         Swal.fire({
                      icon: 'success',
                      title: '',
                      text: `Tipo:  ${this.consultaDespuesParto.consulta}\n  Fecha: ${this.consultaDespuesParto.fecha} \n Hora: ${this.consultaDespuesParto.hora}\n   Médico ${this.consultaDespuesParto.medico}\n  Atendió: ${this.consultaDespuesParto.firma}`,
                    })
  }

  mostrarDatos(consulta, medico, servicio){
    if(servicio == '1'){
      Swal.fire({
              icon: 'success',
              title: '',
              text: `Citas incluidas\nTipo de consulta:${consulta}\nMedico: ${medico}`,
            })
    }
    if(servicio == '2'){
       Swal.fire({
              icon: 'success',
              title: '',
              text: `Examenes de Laboratorio\nTipo de laboratorio:${consulta}\nMedico:${medico}`,
            })
    }
  }

  printComponent(cmpName) { 
    let printContents = document.getElementById(cmpName).innerHTML;
    let originalContents = document.body.innerHTML;
    document.body.innerHTML = printContents;
    window.print();
    document.body.innerHTML = originalContents;
    window.location.reload();
}

}


class consultas  {

  tipo:  string;
  consulta:  string;
  fecha:  string;
  firma:  string;
  hora:  string;
  medico:  string;

}

  
class paqueteDB{ 
CitasIncluidas:  []
consultasGinecologia: Number
contenidoPaquete: []
costoTotal: 14500
examenesLaboratorio:  []
tabuladorPagos: []
icon: String;
nombrePaquete: String;
nomenclatura: String;
nomenclaturaPaciente: []
_id: String;
}