import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { PaquetesService } from '../../../../services/paquetes/paquetes.service';
import * as moment from 'moment';
import Swal from 'sweetalert2'
import { PagosService } from '../../../../services/pagos/pagos.service';
import Dates from '../../../../clases/dates/dates';
import { getDataStorage } from 'src/app/functions/storage/storage.functions';
/* import { CEDE } from 'src/app/classes/cedes/cedes.class'; */
import Tickets from '../../../../clases/tickets/tickets.class';
import { eliminarStorage } from 'src/app/functions/storage/pacienteIntegrados';
import PacienteStorage from 'src/app/clases/pacientes/pacientes.class';

@Component({
  selector: 'app-pediatrico',
  templateUrl: './pediatrico.component.html',
  styleUrls: ['./pediatrico.component.css']
})
export class PediatricoComponent implements OnInit {

  @Input() id: String;
  consultas:any = { tipo: '', consulta: '', fecha: '', medico: '', firma: '' }
  item:any ={ nombreEstudio:'', precioCon:0, precioSin:0, _id:''}
  medicos:any[] = []
  paquete:any[] = []
  concepto:any[] = []
  citas:any[] = []
  examenes:any[] = []
  laboratorio:any[] = []
  public egoCount = 0;
  public ego2Count=0;
  public ego3Count = 0;
  public ego4Count=0;
  public certificado = 0
  public contador=0;
  public contadorcitas =0;
  public urgencias = {fecha:"", hora:"" , consulta:"", medico: "", firma:"" };
  public estancias = {fecha:"", hora:"" , consulta:"", medico: "", firma:"" };
   // public paqueteExamenesInto = [];
  public fechaConsulta = moment().format('l');
  public horaIngreso = moment().format('hh:mm');
  public btnAgregaConsultas = false;

  public pacienteInfo={
    nombrePaciente: "",
    apellidoPaterno: "",
    apellidoMaterno: "",
    curp: "",
    edad: 0,
    genero: "",
    id:"",
    callePaciente: "",
    fechaNacimientoPaciente:"",
    estadoPaciente: "",
    paisPaciente: "",
    telefono: "",
    _id:""
  };
  public infoVenta = {  

    paciente:"",
    nombrePaciente:"",
    vendedor:"",
    fecha:"",
    hora:"",
    estudios:[],
    efectivo:false,
    doctorQueSolicito:"",
    transferencia: false,
    tarjetCredito:false,
    tarjetaDebito:false,
    montoEfectivo:0,
    montoTarjteCredito:0,
    montoTarjetaDebito:0,
    montoTranferencia: 0,
    sede:"",
    totalCompra:0,
    prioridad:"",
    compraConMembresia:true,
    status:'Pagado'
  }

  public pedidosLaboratorios = { 
    estudios:[],
    idPaciente:"",
    fecha:"",
    hora:"",
    medicoQueSolicita:"",
    sede:"",
    prioridad:"Programado",
    estado:""
  }

  public carrito = {
    totalSin: 0,
    totalCon: 0,
    items: []
  };
  
  fecha: string;

  public diasRestantes:Number;
  public fechaRegistro: string;
  public nombrePaquete='';
  public sede='';

  constructor(public _router: ActivatedRoute, 
              public _paquete:PaquetesService, 
              public _route:Router, 
              private _pagoServicios: PagosService) {
                this.obtenerSede();
              }

  ngOnInit(): void {
    this.obtenerPaquete();
    this.obtenerMedicos();
    this.consultas.firma = JSON.parse(localStorage.getItem('usuario')).nombre;
  }

  obtenerSede(){
    this.sede = localStorage.getItem('cede')
  }

  obtenerPaquete(){
    this._paquete.obtenerPaquete(this.id)
    .subscribe(  (data:any) =>  {
      this.pacienteInfo = data['paquetes'][0]['paciente'];
      this.paquete.push(data['paquetes']);
      this.citas = this.paquete[0].visitas
      this.examenes = this.paquete[0].examenesLab;
      this.fechaRegistro = this.paquete[0].fecha;
      this.nombrePaquete = this.paquete[0].paquete.nombrePaquete;
      this.verCosnultashechas();
      this.getDiasRestantes();
    });
  }

  getDiasRestantes(){
    const dias = new Dates()
    this.diasRestantes  =  dias.getDateRest( this.fechaRegistro ); 
    if( this.diasRestantes <= 0 || this.diasRestantes == NaN ){
      let estado = {
        membresiaActiva: false
      }
      this._paquete.actualizarEstadoMembresia( this.pacienteInfo._id, estado )
      .subscribe( data => {/* console.log(data) */});
    }
  }

  obtenerMedicos(){
    this._paquete.getMedicos()
    .subscribe( (data) => {
      this.medicos = data['medicos']
    });
  }

  irAUnServicio(  servicio ){
    this.setRouteService( servicio );
  }

  setRouteService(servicio){
    this._route.navigate([ `/serviciosInt/${servicio}`]);
  }

  verCosnultashechas(){
    this.certificado = 0
    // esta funcion se encarga de ver cuales de los paquetes ya estan hechas y las colorea
    let cardUrgencias = document.querySelector('#urgencias');
    let badegUrgencias = document.querySelector('#badgeUrgencias');
    // consultas despues del parto
    let cardEstancias = document.querySelector('#estanciasCortas');
    let badgeEstancias = document.querySelector('#badgeEstancias');
    // incia el codifo que valida las consultas 
    this.citas.forEach(element => {
      // console.log( element );
      if( element.consulta == "Cita abierta a Urgencias con Médico General" ){
        // console.log("Se lo lee");
        cardUrgencias.classList.add('border-primary');
        badegUrgencias.innerHTML = "1";
        badegUrgencias.classList.add('badge-primary');
        this.urgencias = element ;
      } 
      if( element.consulta === 'Estancias cortas de hasta 8 Horas en Urgencias'){
        cardEstancias.classList.add('border-primary');
        badgeEstancias.innerHTML = '1';
        badgeEstancias.classList.add('badge-primary');
        this.estancias = element;
      }
      if( element.consulta === '1 Certificado Médico Escolar'){
        this.certificado += 1;
        let cardSanguineo = document.querySelector('#certificadoCard');
        let badgeSanguineo = document.querySelector("#badgeCertificado");
        this.addClassesCss( cardSanguineo, badgeSanguineo );
      }
    });
    // termian el codigo que valida las consultas 
    this.verEstudios();
  }

  verEstudios(){
    this.egoCount = 0;
    this.ego2Count=0;
    this.ego3Count = 0;
    this.ego4Count=0;
    // badgeQuimica
   //   // quimicaSanguineaCard
   /* console.log(this.examenes); */
   
   for (const iterator of this.examenes) {
      if( iterator.consulta == "1 Grupo Sanguineo y Factor RH"  ){
        this.egoCount += 1;
        /* console.log( this.egoCount); */
        
        let cardSanguineo = document.querySelector('#SanguineoCard');
        let badgeSanguineo = document.querySelector("#badgeSanguineo");
        this.addClassesCss( cardSanguineo, badgeSanguineo );
      }
      if( iterator.consulta == "1 Biometria Hematica Completa"  ){
        this.ego2Count += 1;
        let cardHematica = document.querySelector('#biometriaHematica');
        let badgeHematica = document.querySelector("#badgeHematica");
        this.addClassesCss( cardHematica, badgeHematica );
      }
      if(iterator.consulta == "2 Examen General de Orina"){
        this.ego3Count += 1;
        let cardGO = document.querySelector('#GOCard');
        let badgeGO = document.querySelector('#badgeGO');
        this.addClassesCss( cardGO, badgeGO );
      }
      if( iterator.consulta == "1 Examen coprológico"){
        this.ego4Count += 1;
        let grupoCoprologico = document.querySelector("#coprologicoCard");
        let badgeCoprologico = document.querySelector("#badgeCopologico");
        this.addClassesCss(grupoCoprologico, badgeCoprologico)
      }
    };
   //   //  termina el codigo que valida los examenes 
  }

  showMessage(examen: string){
    this.examenes.forEach(  (estudio:consultas, index) => {
      // console.log( estudio.consulta  === examen  );
      // console.log( estudio  );
        if( estudio.consulta ===  examen  ){
          // console.log(estudio.consulta, index);
        Swal.fire({
          icon: 'success',
          title: '',
          text: `Tipo:  ${this.examenes[index].consulta}\n  Fecha: ${this.examenes[index].fecha} \n Hora: ${this.examenes[index].hora}\n   Médico ${this.examenes[index].medico}\n  Atendió: ${this.examenes[index].firma}`,
        })
        }
    });
  }

  addClassesCss( card: Element, badge:Element ){
    card.classList.add('border-primary');
    if( badge.id == "badgeSanguineo"  ){
      badge.innerHTML = this.egoCount.toString(); 
    } else if(badge.id == 'badgeHematica'){
      badge.innerHTML = this.ego2Count.toString();
    }else if(badge.id == 'badgeGO'){
      badge.innerHTML = this.ego3Count.toString();
    }else if(badge.id == 'badgeCopologico'){
      badge.innerHTML = this.ego4Count.toString();
    }else if(badge.id == 'badgeCertificado'){
      badge.innerHTML = this.certificado.toString();
    }
    badge.classList.add('badge-primary');
  }

  mostrarUrgencias(){
    /* console.log( this.urgencias ); */
     Swal.fire({
              icon: 'success',
              title: '',
              text: `Tipo:  ${this.urgencias.consulta}\n  Fecha: ${this.urgencias.fecha} \n Hora: ${this.urgencias.hora}\n   Médico ${this.urgencias.medico}\n  Atendió: ${this.urgencias.firma}`,
            })
  }

  mostrarEstancias(){
     Swal.fire({
              icon: 'success',
              title: '',
              text: `Tipo:  ${this.estancias.consulta}\n  Fecha: ${this.estancias.fecha} \n Hora: ${this.estancias.hora}\n   Médico ${this.estancias.medico}\n  Atendió: ${this.estancias.firma}`,
            })
  }

  cambiarVisita( selectCon ){
    let nombreConsultaSelect = selectCon.value ;
    if(this.consultas.tipo  === 'examenesLab' ){
      // let log = console.log;
      // checar si nos se han consumido los laboratorios
      this.examenes.forEach((examen:consultas) =>  {
          if( nombreConsultaSelect == examen.consulta && examen.consulta == nombreConsultaSelect ){
             Swal.fire({
              icon: 'error',
              title: '',
              text: 'ESTE ELEMENTO YA SE UTILIZO',
            })
            this.btnAgregaConsultas = true;
          }
      });
    }
    
    this.citas.forEach( element => {
      if( nombreConsultaSelect ===  element.consulta && element.consulta === "Cita abierta a Urgencias con Médico General"  ){
                     Swal.fire({
              icon: 'error',
              title: '',
              text: 'ESTE ELEMENTO YA SE UTILIZO',
            })
        /* alert('Este elemento ya ha sido usado'); */
        this.btnAgregaConsultas = true;
      }else if(nombreConsultaSelect ===  element.consulta && element.consulta === "Estancias cortas de hasta 8 Horas en Urgencias"){
        this.btnAgregaConsultas = true;
                     Swal.fire({
              icon: 'error',
              title: '',
              text: 'ESTE ELEMENTO YA SE UTILIZO',
            })
      }
      this.btnAgregaConsultas = false;
      if(  nombreConsultaSelect  != "Cita abierta a Urgencias con Médico General" && nombreConsultaSelect  != "Estancias cortas de hasta 8 Horas en Urgencias" && nombreConsultaSelect != "4 Estudios de laboratorio" && nombreConsultaSelect != "1 Grupo Sanguineo y Facto RH"  && nombreConsultaSelect != "1 Biometria Hematica Completa" && nombreConsultaSelect != "2 Examen General de Orina" && nombreConsultaSelect != "1 Examen coprológico"){
        this.btnAgregaConsultas = false;        
      }
      if( this.egoCount == 4  ){
        this.btnAgregaConsultas = false;
      }
      if( this.ego2Count == 2  ){
        this.btnAgregaConsultas = false;
      }
    }) 
  }

  //insercion
  seleccion($event, value){
    switch (value) {
      case 'visitas':
        this.concepto=[]
        for(let item of this.paquete ){
          for(let item2 of item.paquete.CitasIncluidas){
            this.concepto.push(item2)
          }
          /* console.log(this.concepto) */
        }  
        this.consultas.consulta = ''
      break;

      case 'examenesLab':
        this.concepto=[]
        for(let item of this.paquete ){
          for(let item2 of item.paquete.examenesLaboratorio){
            this.concepto.push(item2)
          }
          /* console.log(this.concepto) */
        }  
        this.consultas.consulta = ''
      break;

      default:
        break;
    }
  } 

  agregarConsulta(){
    this.consultas.fecha = new Date()
    this.consultas.hora = this.horaIngreso;
    this.consultas.firma = JSON.parse(localStorage.getItem('usuario')).nombre;

    if(this.consultas.tipo == '' || this.consultas.consulta == '' || this.consultas.medico == '' || this.consultas.firma == ''){
                   Swal.fire({
              icon: 'error',
              title: '',
              text: 'INGRESE LOS DATOS REQUERIDOS',
            })
    }else{
      if(this.consultas.tipo == 'visitas'){
        if(this.consultas.consulta == '1 Certificado Médico Escolar'){
          let val = true;
          val = this.validacionescitas();
          if(val){
            this._paquete.agregarConsulta(this.consultas,this.consultas.tipo,this.id).subscribe(
              (data)=>{
                this.mostrarConsultas();
                this.setDatesVenta(this.consultas.medico);
                this._pagoServicios.agregarPedido( this.infoVenta )
                .subscribe( (data) => {
                  // console.log( data );
                    if(  data['ok'] ){
                    
                      this.generarTicket(data['folio']);
                      // se crea la tabla de las ventas 
                        Swal.fire({
                        icon: 'success',
                        title: '',
                        text: 'LA CONSULTA SE AGREGO EXITOSAMENTE',
                      })
                      // ESTA INFORMCAIÓN SE ENVIA A LOS DIFERENTES SERVICIOS 
                      // seteamos las fechas 
                      eliminarStorage();
                        
                        
                      const eliminarPaciente = new PacienteStorage();
                      eliminarPaciente.removePacienteStorage();
                      this.consultas = { tipo: '', consulta: '', fecha: '', medico: '', firma:'' } 
                      this._route.navigateByUrl('consulta');   
                    }
                });
              })
          }else{
                         Swal.fire({
              icon: 'error',
              title: '',
              text: 'TU PAQUETE SE TERMINO',
            })
            this.consultas = { tipo: '', consulta: '', fecha: '', medico: '', firma:'' }
          }
        }else{
          this._paquete.agregarConsulta(this.consultas,this.consultas.tipo,this.id).subscribe(
            (data)=>{
              this.mostrarConsultas();
              this.setDatesVenta(this.consultas.medico);
              this._pagoServicios.agregarPedido( this.infoVenta )
              .subscribe( (data) => {
                // console.log( data );
                  if(  data['ok'] ){
                  
                    this.generarTicket(data['folio']);
                    // se crea la tabla de las ventas 
                    Swal.fire({
                      icon: 'success',
                      title: '',
                      text: 'la CONSULTA SE AGREGO EXITOSAMENTE',
                    })
                    // ESTA INFORMCAIÓN SE ENVIA A LOS DIFERENTES SERVICIOS 
                    // seteamos las fechas 
                    eliminarStorage();
                      
                      
                    const eliminarPaciente = new PacienteStorage();
                    eliminarPaciente.removePacienteStorage();
                    this.consultas = { tipo: '', consulta: '', fecha: '', medico: '', firma:'' } 
                    this._route.navigateByUrl('consulta');   
                  }
              });
            })
        }          
      }else{
        let val = true;
        val = this.validaciones();
        /* console.log(val); */
        
        if(val){
          this._paquete.agregarConsulta(this.consultas,this.consultas.tipo,this.id).subscribe(
            (data)=>{
              this.setDatesVenta(this.consultas.medico);          
              this.pagar_consulta(this.consultas.consulta, this.consultas.medico);
              this.consultas = { tipo: '', consulta: '', fecha: '', medico: '', firma:'' }
            })
        }else{
                       Swal.fire({
              icon: 'error',
              title: '',
              text: 'SE TERMINARON LOS LABORATORIOS DEL PAQUETE',
            })
          this.consultas = { tipo: '', consulta: '', fecha: '', medico: '', firma:'' }
        }
      }
    }
  }

  validacionescitas(){
    this.contadorcitas=0;
    switch(this.consultas.consulta){
      case '1 Certificado Médico Escolar':
        for (const iterator of this.citas) {
            if(iterator.consulta == '1 Certificado Médico Escolar'){
              this.contadorcitas += 1
            }
          }  
          if(this.contadorcitas < 1){
            return true;
          }else{
            return false;
          }
        break;
    }
  }

  validaciones(){
    this.contador = 0;
    /* console.log(this.examenes); */
    
    switch(this.consultas.consulta){      
      case '1 Grupo Sanguineo y Factor RH':
        for (const iterator of this.examenes) {
          if(iterator.consulta == '1 Grupo Sanguineo y Factor RH'){
            this.contador+= 1
          }
        }
        if(this.contador < 1){
          return true;
        }else{
          return false;
        }
        break;
      case '1 Biometria Hematica Completa':
          for (const iterator of this.examenes) {
            if(iterator.consulta == '1 Biometria Hematica Completa'){
              this.contador+= 1
            }
          }
          /* console.log('asghasgdh'); */
          
          if(this.contador < 1){
            return true;
          }else{
            return false;
          }
        break;
      case '2 Examen General de Orina':
          for (const iterator of this.examenes) {
            if(iterator.consulta == '2 Examen General de Orina'){
              this.contador+= 1
            }
          }
          /* console.log(this.contador); */
          
          if(this.contador < 2){
            return true;
          }else{
            return false;
          }
        break;
      case '1 Examen coprológico':
          for (const iterator of this.examenes) {
            if(iterator.consulta == '1 Examen coprológico'){
              this.contador+= 1
            }
          }
          if(this.contador < 1){
            return true;
          }else{
            return false;
          }
        break;
    }
  }

  //metodo 
  setDatesVenta(medico){
    if(this.consultas.consulta == '1 Certificado Médico Escolar'){
      const item = {
        name:this.nombrePaquete,
        nombreEstudio: "CERTIFICADO MEDICO ESCOLAR",
        precioCon:0,
        precioSin:0,
        _id:'5fd3efec08ccbb0017712f11'
      }
      this.carrito.items.push(item);
    }else{
      const item = {
        name:this.nombrePaquete,
        nombreEstudio: "CONSULTA DE MEDICO GENERAL",
        precioCon:0,
        precioSin:0,
        _id:'5fd3ebca08ccbb0017712f0d'
      }
      this.carrito.items.push(item);
    }
    
    const dates = new Dates();
    //this.infoVenta.totalCompra = this.carrito.totalSin;
    this.fecha = moment().format('l');    
    this.infoVenta.hora = moment().format('LT');
    this.infoVenta.vendedor = getDataStorage()._id;
    if(this.pacienteInfo.id == undefined){
      this.infoVenta.paciente = this.pacienteInfo._id;
    }else{
      this.infoVenta.paciente = this.pacienteInfo.id;
    }
    this.infoVenta.sede = this.sede;
    this.infoVenta.prioridad = "Programado"
    this.infoVenta.fecha = dates.getDate();
    this.infoVenta.doctorQueSolicito = medico;
    
    this.infoVenta.estudios= this.carrito.items
    this.infoVenta.totalCompra = 0;    
  }

  pagar_consulta(nombre,medico){
    const dates = new Dates();
    switch (nombre) {
        case "1 Grupo Sanguineo y Factor RH":
          this.carrito={
            totalSin: 0,
            totalCon: 0,
            items: []
          };
          this.item.nombreEstudio = "GRUPO Y FACTOR RH";
          this.item._id = '5fd292b31cb0ea0017f57c9e';
          this.fecha = moment().format('l');    
          this.infoVenta.hora = moment().format('LT');
          this.infoVenta.vendedor = getDataStorage()._id;
          if(this.pacienteInfo.id == undefined){
            this.infoVenta.paciente = this.pacienteInfo._id;
          }else{
            this.infoVenta.paciente = this.pacienteInfo.id;
          }
          this.infoVenta.nombrePaciente = this.pacienteInfo.nombrePaciente+ ' ' + this.pacienteInfo.apellidoPaterno+ ' ' + this.pacienteInfo.apellidoMaterno
          this.infoVenta.sede = this.sede;
          this.infoVenta.prioridad = "Programado"
          this.infoVenta.fecha = dates.getDate();
          this.infoVenta.doctorQueSolicito = medico;
          /* this.carrito.items.push(this.item); */
          this.infoVenta.estudios= this.carrito.items
          this.infoVenta.totalCompra = 0;
          const item = {
            name:this.nombrePaquete,
            nombreEstudio: "GRUPO Y FACTOR RH",
            precioCon:0,
            precioSin:0,
            idEstudio:'5fd292b31cb0ea0017f57c9e'
          }
          this.carrito.items.push(item);
          this._pagoServicios.agregarPedido( this.infoVenta )
          .subscribe( (data) => {
            // console.log( data );
            if(  data['ok'] ){
              
              this.generarTicket(data['folio']);
                  this.setDatesPedidos();
                  this.pedidosLaboratorios.estudios.push(item);
                  /* console.log(this.pedidosLaboratorios); */
                  
                  this._pagoServicios.pedidosLaboratorio( this.pedidosLaboratorios ).subscribe( data => {/* console.log( data ) */});
                // se crea la tabla de las ventas 
                             Swal.fire({
                  icon: 'success',
                  title: '',
                  text: 'LA CONSULTA SE AGREGO EXITOSAMENTE',
                })
                this.pedidosLaboratorios = { 
                  estudios:[],
                  idPaciente:"",
                  fecha:"",
                  hora:"",
                  medicoQueSolicita:"",
                  sede:"",
                  prioridad:"Programado",
                  estado:""
                }
                // ESTA INFORMCAIÓN SE ENVIA A LOS DIFERENTES SERVICIOS 
                // seteamos las fechas 
                  eliminarStorage();
                  
                  
                  const eliminarPaciente = new PacienteStorage();
                  eliminarPaciente.removePacienteStorage();  
              }
          });
          this.mostrarConsultas();
          break;
        case "1 Biometria Hematica Completa":
            this.carrito={
              totalSin: 0,
              totalCon: 0,
              items: []
            };
            this.item.nombreEstudio = "BIOMETRIA HEMATICA COMPLETA";
            this.item._id = '5fd284b11cb0ea0017f57bd8';
            this.fecha = moment().format('l');    
            this.infoVenta.hora = moment().format('LT');
            this.infoVenta.vendedor = getDataStorage()._id;
            if(this.pacienteInfo.id == undefined){
              this.infoVenta.paciente = this.pacienteInfo._id;
            }else{
              this.infoVenta.paciente = this.pacienteInfo.id;
            }
            this.infoVenta.nombrePaciente = this.pacienteInfo.nombrePaciente+ ' ' + this.pacienteInfo.apellidoPaterno+ ' ' + this.pacienteInfo.apellidoMaterno
            this.infoVenta.sede = this.sede;
            this.infoVenta.prioridad = "Programado"
            this.infoVenta.fecha = dates.getDate();
            this.infoVenta.doctorQueSolicito = medico;
            /* this.carrito.items.push(this.item); */
            this.infoVenta.estudios= this.carrito.items
            this.infoVenta.totalCompra = 0;
            const item2 = {
              name:this.nombrePaquete,
              nombreEstudio: "BIOMETRIA HEMATICA COMPLETA",
              precioCon:0,
              precioSin:0,
              idEstudio:'5fd284b11cb0ea0017f57bd8'
            }
            this.carrito.items.push(item2);
            this._pagoServicios.agregarPedido( this.infoVenta )
            .subscribe( (data) => {
              // console.log( data );
              if(  data['ok'] ){
                
                this.generarTicket(data['folio']);
                  // se crea la tabla de las ventas 
                  this.setDatesPedidos();
                  this.pedidosLaboratorios.estudios.push(item2);
                  console.log(this.pedidosLaboratorios);
                  
                  this._pagoServicios.pedidosLaboratorio( this.pedidosLaboratorios ).subscribe( data => {
                    /* console.log( data ) */});
                  Swal.fire({
                    icon: 'success',
                    title: '',
                    text: 'LA CONSULTA SE AGREGO EXITOSAMENTE',
                  })
                  this.pedidosLaboratorios = { 
                    estudios:[],
                    idPaciente:"",
                    fecha:"",
                    hora:"",
                    medicoQueSolicita:"",
                    sede:"",
                    prioridad:"Programado",
                    estado:""
                  }
                  // ESTA INFORMCAIÓN SE ENVIA A LOS DIFERENTES SERVICIOS 
                  // seteamos las fechas 
                    eliminarStorage();
                    
                    
                    const eliminarPaciente = new PacienteStorage();
                    eliminarPaciente.removePacienteStorage();  
                }
            });
            this.mostrarConsultas();
        break;
        case "2 Examen General de Orina":
                this.carrito={
                  totalSin: 0,
                  totalCon: 0,
                  items: []
                };
                this.item.nombreEstudio = "EXAMEN GENERAL DE ORINA ";
                this.item._id = '5fd28c2f1cb0ea0017f57c58';
                this.fecha = moment().format('l');    
                this.infoVenta.hora = moment().format('LT');
                this.infoVenta.vendedor = getDataStorage()._id;
                if(this.pacienteInfo.id == undefined){
                  this.infoVenta.paciente = this.pacienteInfo._id;
                }else{
                  this.infoVenta.paciente = this.pacienteInfo.id;
                }
                this.infoVenta.nombrePaciente = this.pacienteInfo.nombrePaciente+ ' ' + this.pacienteInfo.apellidoPaterno+ ' ' + this.pacienteInfo.apellidoMaterno
                this.infoVenta.sede = this.sede;
                this.infoVenta.prioridad = "Programado"
                this.infoVenta.fecha = dates.getDate();
                this.infoVenta.doctorQueSolicito = medico;
                /* this.carrito.items.push(this.item); */
                this.infoVenta.estudios= this.carrito.items
                this.infoVenta.totalCompra = 0;
                const item3 = {
                  name:this.nombrePaquete,
                  nombreEstudio: "EXAMEN GENERAL DE ORINA ",
                  precioCon:0,
                  precioSin:0,
                  idEstudio:'5fd28c2f1cb0ea0017f57c58'
                }
                this.carrito.items.push(item3);
                this._pagoServicios.agregarPedido( this.infoVenta )
                .subscribe( (data) => {
                  // console.log( data );
                  if(  data['ok'] ){
                    
                    this.generarTicket(data['folio']);
                      // se crea la tabla de las ventas 
                      this.setDatesPedidos();
                      this.pedidosLaboratorios.estudios.push(item3);
                      /* console.log(this.pedidosLaboratorios); */
                      
                      this._pagoServicios.pedidosLaboratorio( this.pedidosLaboratorios ).subscribe( data => {/* console.log( data ) */});
                      Swal.fire({
                    icon: 'success',
                    title: '',
                    text: 'LA CONSULTA SE AGREGO EXITOSAMENTE',
                  })
                      this.pedidosLaboratorios = { 
                        estudios:[],
                        idPaciente:"",
                        fecha:"",
                        hora:"",
                        medicoQueSolicita:"",
                        sede:"",
                        prioridad:"Programado",
                        estado:""
                      }
                      // ESTA INFORMCAIÓN SE ENVIA A LOS DIFERENTES SERVICIOS 
                      // seteamos las fechas 
                        eliminarStorage();
                        
                        
                        const eliminarPaciente = new PacienteStorage();
                        eliminarPaciente.removePacienteStorage();  
                    }
                });
                this.mostrarConsultas();
        break;
        case "1 Examen coprológico":
                this.carrito={
                  totalSin: 0,
                  totalCon: 0,
                  items: []
                };
                this.item.nombreEstudio = "COPROLOGICO (EXAMEN GENERAL DE HECES)";
                this.item._id = '5fd287ed1cb0ea0017f57c0d';
                this.fecha = moment().format('l');    
                this.infoVenta.hora = moment().format('LT');
                this.infoVenta.vendedor = getDataStorage()._id;
                if(this.pacienteInfo.id == undefined){
                  this.infoVenta.paciente = this.pacienteInfo._id;
                }else{
                  this.infoVenta.paciente = this.pacienteInfo.id;
                }
                this.infoVenta.nombrePaciente = this.pacienteInfo.nombrePaciente+ ' ' + this.pacienteInfo.apellidoPaterno+ ' ' + this.pacienteInfo.apellidoMaterno
                this.infoVenta.sede = this.sede;
                this.infoVenta.prioridad = "Programado"
                this.infoVenta.fecha = dates.getDate();
                this.infoVenta.doctorQueSolicito = medico;
                /* this.carrito.items.push(this.item); */
                this.infoVenta.estudios= this.carrito.items
                this.infoVenta.totalCompra = 0;
                const item4 = {
                  name:this.nombrePaquete,
                  nombreEstudio: "COPROLOGICO (EXAMEN GENERAL DE HECES)",
                  precioCon:0,
                  precioSin:0,
                  idEstudio:'5fd287ed1cb0ea0017f57c0d'
                }
                this.carrito.items.push(item4);
                this._pagoServicios.agregarPedido( this.infoVenta )
                .subscribe( (data) => {
                  // console.log( data );
                  if(  data['ok'] ){
                    
                    this.generarTicket(data['folio']);
                      // se crea la tabla de las ventas 
                      this.setDatesPedidos();
                      this.pedidosLaboratorios.estudios.push(item4);
                      /* console.log(this.pedidosLaboratorios); */
                      
                      this._pagoServicios.pedidosLaboratorio( this.pedidosLaboratorios ).subscribe( data => {/* console.log( data ) */});
                      Swal.fire({
                        icon: 'success',
                        title: '',
                        text: 'LA CONSULTA SE AGREGO EXITOSAMENTE',
                      })
                      this.pedidosLaboratorios = { 
                        estudios:[],
                        idPaciente:"",
                        fecha:"",
                        hora:"",
                        medicoQueSolicita:"",
                        sede:"",
                        prioridad:"Programado",
                        estado:""
                      }
                      // ESTA INFORMCAIÓN SE ENVIA A LOS DIFERENTES SERVICIOS 
                      // seteamos las fechas 
                        eliminarStorage();
                        
                        
                        const eliminarPaciente = new PacienteStorage();
                        eliminarPaciente.removePacienteStorage();  
                    }
                });
                this.mostrarConsultas();
        break;
      default:
        break;
    }
  }

  setDatesPedidos (){
    // this.pedidosLaboratorios.fecha = moment().format('l');
    const datesPedidoLab = new Dates();
    // configuracion de los pedidos de laboratorio
    this.pedidosLaboratorios.fecha = datesPedidoLab.getDate();
    this.pedidosLaboratorios.hora = moment().format('LT');
    this.pedidosLaboratorios.medicoQueSolicita = this.infoVenta.doctorQueSolicito;
    this.pedidosLaboratorios.sede = this.sede;
    this.pedidosLaboratorios.idPaciente = this.infoVenta.paciente;
  }

  mostrarConsultas(){
    this.obtenerPaquete();
  }
  generarTicket(folio){

    const tickets = new Tickets();
    tickets.printTicketSinMembresia( this.pacienteInfo, this.carrito ,  this.infoVenta, folio );
  
  }
  mostrarDatos(consulta, medico, servicio){
    if(servicio == '1'){
      Swal.fire({
                    icon: 'success',
                    title: '',
                    text: `Citas incluidas\nTipo de consulta:${consulta}\nMedico: ${medico}`,
                  })
    }
    if(servicio == '2'){
      Swal.fire({
                    icon: 'success',
                    title: '',
                    text: `Examenes de Laboratorio\nTipo de laboratorio: ${consulta}\nMedico: ${medico}`,
                  })
    }
  }
}

class consultas  {

  tipo:  string;
  consulta:  string;
  fecha:  string;
  firma:  string;
  hora:  string;
  medico:  string;

}
