import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PagosPaquetesComponent } from './pagos-paquetes.component';

describe('PagosPaquetesComponent', () => {
  let component: PagosPaquetesComponent;
  let fixture: ComponentFixture<PagosPaquetesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PagosPaquetesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PagosPaquetesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
