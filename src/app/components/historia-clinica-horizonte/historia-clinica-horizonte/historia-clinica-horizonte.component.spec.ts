import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HistoriaClinicaHorizonteComponent } from './historia-clinica-horizonte.component';

describe('HistoriaClinicaHorizonteComponent', () => {
  let component: HistoriaClinicaHorizonteComponent;
  let fixture: ComponentFixture<HistoriaClinicaHorizonteComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HistoriaClinicaHorizonteComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HistoriaClinicaHorizonteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
