import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AntecedentesPNPComponent } from './antecedentes-pnp.component';

describe('AntecedentesPNPComponent', () => {
  let component: AntecedentesPNPComponent;
  let fixture: ComponentFixture<AntecedentesPNPComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AntecedentesPNPComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AntecedentesPNPComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
