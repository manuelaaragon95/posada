import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AntecedentesPpComponent } from './antecedentes-pp.component';

describe('AntecedentesPpComponent', () => {
  let component: AntecedentesPpComponent;
  let fixture: ComponentFixture<AntecedentesPpComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AntecedentesPpComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AntecedentesPpComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
