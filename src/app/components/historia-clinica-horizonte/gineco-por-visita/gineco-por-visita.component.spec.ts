import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GinecoPorVisitaComponent } from './gineco-por-visita.component';

describe('GinecoPorVisitaComponent', () => {
  let component: GinecoPorVisitaComponent;
  let fixture: ComponentFixture<GinecoPorVisitaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GinecoPorVisitaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GinecoPorVisitaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
