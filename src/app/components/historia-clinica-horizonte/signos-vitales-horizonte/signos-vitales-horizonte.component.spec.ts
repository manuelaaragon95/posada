import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SignosVitalesHorizonteComponent } from './signos-vitales-horizonte.component';

describe('SignosVitalesHorizonteComponent', () => {
  let component: SignosVitalesHorizonteComponent;
  let fixture: ComponentFixture<SignosVitalesHorizonteComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SignosVitalesHorizonteComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SignosVitalesHorizonteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
