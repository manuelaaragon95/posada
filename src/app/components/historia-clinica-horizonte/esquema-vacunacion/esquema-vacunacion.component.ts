import { Component, Input, OnInit} from '@angular/core';
import { NgForm } from '@angular/forms';
import { HistoriaClinicaService } from 'src/app/services/historiaClinica/historia-clinica.service';
import { EsquemaVacun } from '../../../interfaces/historia-clinica';
import Swal from 'sweetalert2'
import { NgxSpinnerService } from "ngx-spinner";

@Component({
  selector: 'app-esquema-vacunacion',
  templateUrl: './esquema-vacunacion.component.html',
  styleUrls: ['./esquema-vacunacion.component.css']
})
export class EsquemaVacunacionComponent implements OnInit {

  @Input() _id='';
  @Input() esquemaVacuncaion:EsquemaVacun = {} as EsquemaVacun;
  
  constructor(private _HistoriaClinicaService: HistoriaClinicaService,
              private spinner: NgxSpinnerService) { }

  ngOnInit(): void {
  }

  agregarVacunacionNinos(form: NgForm) {
    this.spinner.show();
    this.esquemaVacuncaion = form.value
    this.esquemaVacuncaion.idPaciente = this._id;
    this._HistoriaClinicaService.agregarEsquemaVacunacion( this.esquemaVacuncaion )
    .subscribe((data:any) => {
      if(data['ok']) {
        this.spinner.hide();
        Swal.fire({
          icon: 'success',
          title: '',
          text: 'SE GUARDARDO EL ESQUEMA DE VACUNACION',
        })
      }
    });
  }

  actualizarEsquemaVacunacion(form: NgForm){
    this.spinner.show();
    let id = this.esquemaVacuncaion._id
    this.esquemaVacuncaion = form.value
    this.esquemaVacuncaion._id = id;
    this.esquemaVacuncaion.idPaciente = this._id;
    this._HistoriaClinicaService.actualizarEV(this.esquemaVacuncaion._id, this.esquemaVacuncaion)
    .subscribe((data:any) => {
      this.spinner.hide();
      if(data['ok']){
        Swal.fire({
          icon: 'success',
          title: '',
          text: 'SE ACTUALIZO EL ESQUEMA DE VACUNACION',
        })
      }
    })
  }

}
