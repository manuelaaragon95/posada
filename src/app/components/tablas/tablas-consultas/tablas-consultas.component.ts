import { Component, OnInit,Input} from '@angular/core';
import { NgxSpinnerService } from 'ngx-spinner';

import { ConsultaService } from 'src/app/services/consultas/consulta.service';
import { RecetasService } from 'src/app/services/recetas/recetas.service';



@Component({
  selector: 'app-tablas-consultas',
  templateUrl: './tablas-consultas.component.html',
  styleUrls: ['./tablas-consultas.component.css']
})
export class TablasConsultasComponent implements OnInit {

  @Input() estudiosPendientes: string;

  public consultas:[]=[];
  public recetasConEstudios:[] =[];
  public pagina = 0;
  public total: string;

  constructor(public _consultaService: ConsultaService,
              private _recetaService: RecetasService,
              private spinner: NgxSpinnerService) { }

  ngOnInit(): void {
    this.spinner.show();
    if( this.estudiosPendientes == 'consultas'){
      this.obtenerCosnultas();
    }else if( this.estudiosPendientes == 'estudios'  ){
      this.obtenerRecetas();
    }
  }
  obtenerCosnultas(){
    this._consultaService.verConsultasRecepcion()
    .subscribe( (data) =>   {
      this.consultas = data['data'].reverse()
      this.total=data['data'].results;
      this.spinner.hide();
    });
  }


  obtenerRecetas(){
    this._recetaService.verRecetasEmitidas(  )
    .subscribe(  (data) => {
      this.recetasConEstudios = data['data'].reverse();
      this.spinner.hide();
    });
  }

}
