import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TablasConsultasComponent } from './tablas-consultas.component';

describe('TablasConsultasComponent', () => {
  let component: TablasConsultasComponent;
  let fixture: ComponentFixture<TablasConsultasComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TablasConsultasComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TablasConsultasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
