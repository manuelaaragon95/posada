import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RegistroPosadaComponent } from './registro-posada.component';

describe('RegistroPosadaComponent', () => {
  let component: RegistroPosadaComponent;
  let fixture: ComponentFixture<RegistroPosadaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RegistroPosadaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RegistroPosadaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
