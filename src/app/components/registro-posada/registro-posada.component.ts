import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { NgxSpinnerService } from "ngx-spinner";
import { PosadaService } from './../../services/posada/posada.service';

@Component({
  selector: 'app-registro-posada',
  templateUrl: './registro-posada.component.html',
  styleUrls: ['./registro-posada.component.css']
})
export class RegistroPosadaComponent implements OnInit {

  public validateBtn = false;
  public edadPaciente: any;
  public btnActivar = false;

  constructor(private spinner: NgxSpinnerService,
              private _posada: PosadaService) { }

  ngOnInit(): void {
  }

  validarFecha(fechaForm: HTMLInputElement){
    let fechaElegida = fechaForm.value;
    let fechaAhora = new Date();
    if(Date.parse(fechaElegida.toString()) > Date.parse(fechaAhora.toString())){
      fechaForm.value = fechaAhora.toString();
    }
  }

  generarEdad(edadForm: HTMLInputElement ) {
    this.edadPaciente = document.querySelector('#edad');
    let fecha = edadForm.value;
    let splitFecha = fecha.split('-');
    var fechaActual = new Date();
    var anio = fechaActual.getFullYear();
    let edadNormal = anio - parseFloat( splitFecha[0]  );
    let edades = edadNormal.toString();
    this.edadPaciente.value = edades;
    this.validarFecha(edadForm);
  }

  enviar(form: NgForm){
    this.spinner.show();
    if (form.form.status != 'INVALID') {
      console.log(form.form.value);
      this._posada.registrarHuesped(form.form.value).subscribe((resp:any)=>{
        console.log(resp);
      })
    }
  }

}
