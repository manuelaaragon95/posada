export interface fichaHuesped {
    apellidoMaterno: string
    apellidoPaterno: string
    claveIne: string
    correoHuesped: string
    domicilio: string
    edad: number
    genero: string
    nombre: string
    status: string
    telefono: number
}